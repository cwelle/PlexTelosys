# PlexTelosys [![Build Status](https://travis-ci.org/welle/PlexTelosys.svg?branch=master)](https://travis-ci.org/welle/PlexTelosys) [![Quality Gate](https://sonarcloud.io/api/badges/gate?key=aka.plextelosys:PlexTelosys)](https://sonarcloud.io/dashboard?id=aka.plextelosys:PlexTelosys) #

## Quick summary ##
PlexTelosys is Java library providing query search for Plex database.

## How to use it ##
See Unit tests to see how it works.
You can fork and implementing other searches.

### Version

Go to [my maven repository](https://github.com/welle/maven-repository) to get the latest version.

## Notes
Need the eclipse-external-annotations-m2e-plugin: 

p2 update site to install this from: http://www.lastnpe.org/eclipse-external-annotations-m2e-plugin-p2-site/ (The 404 is normal, just because there is no index.html; it will work in Eclipse.)