package org.hibernate.dialect.identity;

/**
 * SQLiteDialectIdentityColumnSupport.
 *
 * @author charlottew
 */
public class SQLiteDialectIdentityColumnSupport extends IdentityColumnSupportImpl {
    @Override
    public boolean supportsIdentityColumns() {
        return true;
    }

    @Override
    public boolean hasDataTypeInIdentityColumn() {
        return false;
    }

    @Override
    public String getIdentitySelectString(final String table, final String column, final int type) {
        return "select last_insert_rowid()";
    }

    @Override
    public String getIdentityColumnString(final int type) {
        // return "integer primary key autoincrement";
        // FIXME "autoincrement"
        return "integer";
    }
}
