package aka.plexdb.bean;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;

/**
 * Persistent class for entity stored in table "media_items".
 *
 * This file can be filled with association or specific method.
 * REGENERATION WILL ERASED YOUR CHANGES.
 */
@Entity
@Table(name = "\"media_items\"")
public class MediaItemsEntity extends AbstractMediaItemsEntity {

    private static final long serialVersionUID = 1L;

    // bi-directional one-to-mane association to MediaPartsEntity
    @OneToMany(mappedBy = "mediaItem")
    @NonNull
    private List<@NonNull MediaPartsEntity> mediaParts = new ArrayList<>();

    // bi-directional many-to-one association to MetadataItemsEntity
    @ManyToOne(targetEntity = MetadataItemsEntity.class, optional = true)
    @JoinColumn(name = "\"metadata_item_id\"", referencedColumnName = "\"id\"")
    @Nullable
    private MetadataItemsEntity metadataItem;

    // bi-directional many-to-one association to SectionLocationsEntity
    @ManyToOne(targetEntity = SectionLocationsEntity.class, optional = true)
    @JoinColumn(name = "\"section_location_id\"", referencedColumnName = "\"id\"")
    @Nullable
    private SectionLocationsEntity sectionLocation;

    // bi-directional one-to-mane association to MediaStreamsEntity
    @OneToMany(mappedBy = "mediaItem")
    @NonNull
    private List<MediaStreamsEntity> mediaStreams = new ArrayList<>();

    // bi-directional many-to-one association to LibrarySectionsEntity
    @ManyToOne(targetEntity = LibrarySectionsEntity.class, optional = true)
    @JoinColumn(name = "\"library_section_id\"", referencedColumnName = "\"id\"")
    @Nullable
    private LibrarySectionsEntity librarySection;

    /**
     * Get list of linked MediaStreamsEntity.
     *
     * @return list of linked MediaStreamsEntity
     * @see MediaStreamsEntity
     */
    public List<MediaStreamsEntity> getMediaStreams() {
        return this.mediaStreams;
    }

    /**
     * Set list of MediaStreamsEntity.
     *
     * @param mediaStreams list of MediaStreamsEntity
     * @see MediaStreamsEntity
     */
    public void setMediaStreams(@NonNull final List<@NonNull MediaStreamsEntity> mediaStreams) {
        this.mediaStreams = mediaStreams;
    }

    /**
     * Add MediaStreamsEntity.
     *
     * @param mediaStreamsEntity
     * @return added media stream with media item set.
     * @see MediaStreamsEntity
     */
    @NonNull
    public MediaStreamsEntity addMediaStream(@NonNull final MediaStreamsEntity mediaStreamsEntity) {
        getMediaStreams().add(mediaStreamsEntity);
        mediaStreamsEntity.setMediaItem(this);

        return mediaStreamsEntity;
    }

    /**
     * Remove MediaStreamsEntity.
     *
     * @param mediaStreamsEntity
     * @return removed meta data item with media item unset.
     * @see MediaStreamsEntity
     */
    @NonNull
    public MediaStreamsEntity removeMediaStream(@NonNull final MediaStreamsEntity mediaStreamsEntity) {
        getMediaStreams().remove(mediaStreamsEntity);
        mediaStreamsEntity.setMediaItem(null);

        return mediaStreamsEntity;
    }

    /**
     * Get list of linked MediaPartsEntity.
     *
     * @return list of linked MediaPartsEntity
     * @see MediaPartsEntity
     */
    @NonNull
    public List<@NonNull MediaPartsEntity> getMediaParts() {
        return this.mediaParts;
    }

    /**
     * Set list of MediaPartsEntity.
     *
     * @param mediaParts list of MediaPartsEntity
     */
    public void setMediaParts(@NonNull final List<@NonNull MediaPartsEntity> mediaParts) {
        this.mediaParts = mediaParts;
    }

    /**
     * Add MediaPartsEntity.
     *
     * @param mediaPart
     * @return added media part with media item set.
     * @see MediaPartsEntity
     */
    @NonNull
    public MediaPartsEntity addMediaPart(@NonNull final MediaPartsEntity mediaPart) {
        getMediaParts().add(mediaPart);
        mediaPart.setMediaItem(this);

        return mediaPart;
    }

    /**
     * Remove MediaPartsEntity.
     *
     * @param mediaPart
     * @return removed media part with media item unset.
     * @see MediaPartsEntity
     */
    @NonNull
    public MediaPartsEntity removeMediaPart(@NonNull final MediaPartsEntity mediaPart) {
        getMediaParts().remove(mediaPart);
        mediaPart.setMediaItem(null);

        return mediaPart;
    }

    /**
     * Set linked MetadataItemsEntity.
     *
     * @return linked MetadataItemsEntity
     */
    @Nullable
    public MetadataItemsEntity getMetadataItem() {
        return this.metadataItem;
    }

    /**
     * Set linked MetadataItemsEntity.
     *
     * @param metadataItem
     */
    public void setMetadataItem(@Nullable final MetadataItemsEntity metadataItem) {
        this.metadataItem = metadataItem;
    }

    /**
     * Get linked SectionLocationsEntity
     *
     * @return linked SectionLocationsEntity
     * @see SectionLocationsEntity
     */
    @Nullable
    public SectionLocationsEntity getSectionLocation() {
        return this.sectionLocation;
    }

    /**
     * Set linked SectionLocationsEntity.
     *
     * @param sectionLocation
     * @see SectionLocationsEntity
     */
    public void setSectionLocation(@Nullable final SectionLocationsEntity sectionLocation) {
        this.sectionLocation = sectionLocation;
    }

    /**
     * Get linked LibrarySectionsEntity.
     *
     * @return linked LibrarySectionsEntity
     * @see LibrarySectionsEntity
     */
    @Nullable
    public LibrarySectionsEntity getLibrarySection() {
        return this.librarySection;
    }

    /**
     * Set linked LibrarySectionsEntity.
     *
     * @param librarySection
     * @see LibrarySectionsEntity
     */
    public void setLibrarySection(@Nullable final LibrarySectionsEntity librarySection) {
        this.librarySection = librarySection;
    }

}
