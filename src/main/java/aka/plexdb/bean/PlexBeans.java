package aka.plexdb.bean;

import java.util.ArrayList;
import java.util.List;

public class PlexBeans {

    public static List<Class<? extends Object>> PLEX_BEAN_LIST;

    static {
        PLEX_BEAN_LIST = new ArrayList<>();
        PLEX_BEAN_LIST.add(DirectoriesEntity.class);
        PLEX_BEAN_LIST.add(LibrarySectionsEntity.class);
        PLEX_BEAN_LIST.add(MediaItemsEntity.class);
        PLEX_BEAN_LIST.add(MediaPartsEntity.class);
        PLEX_BEAN_LIST.add(MediaStreamsEntity.class);
        PLEX_BEAN_LIST.add(MetadataItemsEntity.class);
        PLEX_BEAN_LIST.add(MetadataItemSettingsEntity.class);
        PLEX_BEAN_LIST.add(MetadataItemViewsEntity.class);
        PLEX_BEAN_LIST.add(SectionLocationsEntity.class);
        PLEX_BEAN_LIST.add(StreamTypesEntity.class);
        PLEX_BEAN_LIST.add(TaggingsEntity.class);
        PLEX_BEAN_LIST.add(TagsEntity.class);
    }
}
