package aka.plexdb.bean;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.eclipse.jdt.annotation.NonNull;

import aka.plexdb.bean.guid.GuID;

/**
 * Persistent class for entity stored in table "metadata_item_settings".
 *
 * This file can be filled with association or specific method.
 * REGENERATION WILL ERASED YOUR CHANGES.
 */
@Entity
@Table(name = "\"metadata_item_settings\"")
public class MetadataItemSettingsEntity extends AbstractMetadataItemSettingsEntity {

    private static final long serialVersionUID = 1L;

    /**
     * Get GuId.
     *
     * @return GuId
     * @see GuID
     */
    @NonNull
    public GuID getGuId() {
        GuID result = new GuID("");

        final String guidValue = getGuid();
        if (guidValue != null) {
            result = new GuID(guidValue);
        }

        return result;
    }
}
