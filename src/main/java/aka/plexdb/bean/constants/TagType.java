package aka.plexdb.bean.constants;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;

/**
 * @author Charlotte
 */
public enum TagType {

    /**
     * Constant for the <code>GENRE</code>.
     */
    GENRE("Genre", 1),

    /**
     * Constant for the <code>COLLECTION</code>.
     */
    COLLECTION("Collection", 2),

    /**
     * Constant for the <code>DIRECTOR</code>.
     */
    DIRECTOR("Director", 4),

    /**
     * Constant for the <code>WRITER</code>.
     */
    WRITER("Writer", 5),

    /**
     * Constant for the <code>Actor</code>.
     */
    ACTOR("Actor", 6),

    /**
     * Constant for the <code>COUNTRY</code>.
     */
    COUNTRY("Country", 8);

    @NonNull
    private final String name;
    @NonNull
    private final Integer value;

    TagType(@NonNull final String name, final int value) {
        this.name = name;
        this.value = new Integer(value);
    }

    /**
     * Get the name.
     *
     * @return value.
     */
    @NonNull
    public String getName() {
        return this.name;
    }

    /**
     * Get the value to be store in the database.
     *
     * @return value.
     */
    @NonNull
    public Integer getValue() {
        return this.value;
    }

    /**
     * Get GuIDType enumeration value corresponding to the given Integer tagType.
     *
     * @param tagType tag type
     * @return TagType
     */
    @Nullable
    public static TagType getTagType(@Nullable final Integer tagType) {
        if (tagType == null) {
            return null;
        } else {
            for (final TagType e : TagType.values()) {
                if (tagType.equals(e.getValue())) {
                    return e;
                }
            }
        }

        return null;
    }
}
