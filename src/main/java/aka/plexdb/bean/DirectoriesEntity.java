package aka.plexdb.bean;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;

/**
 * Persistent class for entity stored in table "directories".
 *
 * This file can be filled with association or specific method.
 * REGENERATION WILL ERASED YOUR CHANGES.
 */
@Entity
@Table(name = "\"directories\"")
public class DirectoriesEntity extends AbstractDirectoriesEntity {

    private static final long serialVersionUID = 1L;

    // bi-directional many-to-one association to MediaPartsEntity
    @OneToMany(mappedBy = "directories")
    @NonNull
    private List<@NonNull MediaPartsEntity> mediaPartsEntities = new ArrayList<>();

    // bi-directional many-to-one association to DirectoriesEntity
    @OneToMany(mappedBy = "parentDirectory")
    @NonNull
    private List<@NonNull DirectoriesEntity> childDirectories = new ArrayList<>();

    // bi-directional many-to-one association to LibrarySectionsEntity
    @ManyToOne(targetEntity = LibrarySectionsEntity.class, optional = true)
    @JoinColumn(name = "\"library_section_id\"", referencedColumnName = "\"id\"")
    @Nullable
    private LibrarySectionsEntity librarySection;

    // bi-directional many-to-one association to DirectoriesEntity
    @ManyToOne(targetEntity = DirectoriesEntity.class, optional = true)
    @JoinColumn(name = "\"parent_directory_id\"", referencedColumnName = "\"id\"")
    @Nullable
    private DirectoriesEntity parentDirectory;

    /**
     * Set parent directory.
     *
     * @param parentDirectory
     */
    public void setParentDirectory(@Nullable final DirectoriesEntity parentDirectory) {
        this.parentDirectory = parentDirectory;
    }

    /**
     * Get parent directory.
     *
     * @return parent directory
     */
    @Nullable
    public DirectoriesEntity getParentDirectory() {
        return this.parentDirectory;
    }

    /**
     * Get list of children directories.
     *
     * @return list of children directories
     */
    @NonNull
    public List<@NonNull DirectoriesEntity> getChildDirectories() {
        return this.childDirectories;
    }

    /**
     * Set list of children directories.
     *
     * @param childDirectories
     */
    public void setChildDirectories(@NonNull final List<@NonNull DirectoriesEntity> childDirectories) {
        this.childDirectories = childDirectories;
    }

    /**
     * Add child directory.
     *
     * @param mediaPartsEntity
     * @return added child directory with parent directory set
     */
    @NonNull
    public DirectoriesEntity addChildDirectory(@NonNull final DirectoriesEntity mediaPartsEntity) {
        getChildDirectories().add(mediaPartsEntity);
        mediaPartsEntity.setParentDirectory(this);

        return mediaPartsEntity;
    }

    /**
     * Remove child directory.
     *
     * @param mediaPartsEntity
     * @return removed child directory with parent directory unset
     */
    @NonNull
    public DirectoriesEntity removeChildDirectory(@NonNull final DirectoriesEntity mediaPartsEntity) {
        getChildDirectories().remove(mediaPartsEntity);
        mediaPartsEntity.setParentDirectory(null);

        return mediaPartsEntity;
    }

    /**
     * Get list of linked MediaPartsEntity.
     *
     * @return list of linked MediaPartsEntity
     * @see MediaPartsEntity
     */
    @NonNull
    public List<@NonNull MediaPartsEntity> getMediaParts() {
        return this.mediaPartsEntities;
    }

    /**
     * Set list of MediaPartsEntity.
     *
     * @param mediaPartsEntities list of MediaPartsEntity
     * @see MediaPartsEntity
     */
    public void setMediaParts(@NonNull final List<@NonNull MediaPartsEntity> mediaPartsEntities) {
        this.mediaPartsEntities = mediaPartsEntities;
    }

    /**
     * Add media part.
     *
     * @param mediaPartsEntity
     * @return added media part with directory set
     * @see MediaPartsEntity
     */
    @NonNull
    public MediaPartsEntity addMediaPart(@NonNull final MediaPartsEntity mediaPartsEntity) {
        getMediaParts().add(mediaPartsEntity);
        mediaPartsEntity.setDirectory(this);

        return mediaPartsEntity;
    }

    /**
     * Remove media part.
     *
     * @param mediaPartsEntity
     * @return removed child directory with parent directory unset
     * @see MediaPartsEntity
     */
    @NonNull
    public MediaPartsEntity removeMediaPart(@NonNull final MediaPartsEntity mediaPartsEntity) {
        getMediaParts().remove(mediaPartsEntity);
        mediaPartsEntity.setDirectory(null);

        return mediaPartsEntity;
    }

    /**
     * Get library section.
     *
     * @return library section
     * @see LibrarySectionsEntity
     */
    @Nullable
    public LibrarySectionsEntity getLibrarySection() {
        return this.librarySection;
    }

    /**
     * Set library section.
     *
     * @param librarySection
     * @see LibrarySectionsEntity
     */
    public void setLibrarySection(@Nullable final LibrarySectionsEntity librarySection) {
        this.librarySection = librarySection;
    }
}
