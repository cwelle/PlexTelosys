package aka.plexdb.bean;

import java.io.Serializable;

import java.util.Date;
import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import org.eclipse.jdt.annotation.Nullable;

/**
 * Persistent class for entity stored in table "directories".
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class AbstractDirectoriesEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Nullable
    @Column(name = "\"id\"")
    private Integer id;

    @Nullable
    @Column(name = "\"path\"")
    private String path;

    @Nullable
    @Column(name = "\"created_at\"")
    private Date createdAt;

    @Nullable
    @Column(name = "\"updated_at\"")
    private Date updatedAt;

    @Nullable
    @Column(name = "\"deleted_at\"")
    private Date deletedAt;

    /**
     * Set id.
     * 
     * @param id
     */  
    public void setId(@Nullable Integer id) {
        this.id = id;
    }

    /**
     * Get id.
     * 
     * @return id
     */
    @Nullable
    public Integer getId() {
        return this.id;
    }



    // DATABASE MAPPING : path ( VARCHAR(255) ) 
    /**
     * Set path.
     * 
     * @param path
     */    
    public void setPath(@Nullable String path) {
        this.path = path;
    }

    /**
     * Get path.
     * 
     * @return path
     */
    @Nullable
    public String getPath() {
        return this.path;
    }

    // DATABASE MAPPING : created_at ( DATETIME ) 
    /**
     * Set createdAt.
     * 
     * @param createdAt
     */    
    public void setCreatedAt(@Nullable Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * Get createdAt.
     * 
     * @return createdAt
     */
    @Nullable
    public Date getCreatedAt() {
        return this.createdAt;
    }

    // DATABASE MAPPING : updated_at ( DATETIME ) 
    /**
     * Set updatedAt.
     * 
     * @param updatedAt
     */    
    public void setUpdatedAt(@Nullable Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * Get updatedAt.
     * 
     * @return updatedAt
     */
    @Nullable
    public Date getUpdatedAt() {
        return this.updatedAt;
    }

    // DATABASE MAPPING : deleted_at ( DATETIME ) 
    /**
     * Set deletedAt.
     * 
     * @param deletedAt
     */    
    public void setDeletedAt(@Nullable Date deletedAt) {
        this.deletedAt = deletedAt;
    }

    /**
     * Get deletedAt.
     * 
     * @return deletedAt
     */
    @Nullable
    public Date getDeletedAt() {
        return this.deletedAt;
    }

}
