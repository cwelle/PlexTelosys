package aka.plexdb.bean;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.eclipse.jdt.annotation.NonNull;

/**
 * Persistent class for entity stored in table "library_sections".
 *
 * This file can be filled with association or specific method.
 * REGENERATION WILL ERASED YOUR CHANGES.
 */
@Entity
@Table(name = "\"library_sections\"")
public class LibrarySectionsEntity extends AbstractLibrarySectionsEntity {

    private static final long serialVersionUID = 1L;

    // bi-directional many-to-one association to MetadataItemsEntity
    @OneToMany(mappedBy = "librarySection")
    @NonNull
    private List<@NonNull MetadataItemsEntity> metadataItemsEntities = new ArrayList<>();

    // bi-directional many-to-one association to DirectoriesEntity
    @OneToMany(mappedBy = "librarySection")
    @NonNull
    private List<@NonNull DirectoriesEntity> directoriesEntities = new ArrayList<>();

    // bi-directional many-to-one association to MediaItemsEntity
    @OneToMany(mappedBy = "librarySection")
    @NonNull
    private List<@NonNull MediaItemsEntity> mediaItems = new ArrayList<>();

    // bi-directional many-to-one association to MetadataItemViewsEntity
    @OneToMany(mappedBy = "librarySection")
    @NonNull
    private List<@NonNull MetadataItemViewsEntity> metadataItemSettingsEntities = new ArrayList<>();

    // bi-directional many-to-one association to SectionLocationsEntity
    @OneToMany(mappedBy = "librarySection")
    @NonNull
    private List<@NonNull SectionLocationsEntity> sectionLocationsEntities = new ArrayList<>();

    /**
     * Get list of linked MetadataItemsEntity.
     *
     * @return list of MetadataItemsEntity
     * @see MetadataItemsEntity
     */
    @NonNull
    public List<@NonNull MetadataItemsEntity> getMetadataItems() {
        return this.metadataItemsEntities;
    }

    /**
     * Set list of MetadataItemsEntity.
     *
     * @param mediaPartsEntities
     * @see MetadataItemsEntity
     */
    public void setMetadataItems(@NonNull final List<@NonNull MetadataItemsEntity> mediaPartsEntities) {
        this.metadataItemsEntities = mediaPartsEntities;
    }

    /**
     * Add MetadataItemsEntity.
     *
     * @param metadataItemsEntity
     * @return added meta data item with library section set.
     * @see MetadataItemsEntity
     */
    @NonNull
    public MetadataItemsEntity addMetadataItems(@NonNull final MetadataItemsEntity metadataItemsEntity) {
        getMetadataItems().add(metadataItemsEntity);
        metadataItemsEntity.setLibrarySection(this);

        return metadataItemsEntity;
    }

    /**
     * Remove MetadataItemsEntity.
     *
     * @param metadataItemsEntity
     * @return removed meta data item with library section unset.
     * @see MetadataItemsEntity
     */
    @NonNull
    public MetadataItemsEntity removeMetadataItems(@NonNull final MetadataItemsEntity metadataItemsEntity) {
        getMetadataItems().remove(metadataItemsEntity);
        metadataItemsEntity.setLibrarySection(null);

        return metadataItemsEntity;
    }

    /**
     * Get list of linked MediaItemsEntity.
     *
     * @return list of linked MediaItemsEntity
     * @see MediaItemsEntity
     */
    @NonNull
    public List<@NonNull MediaItemsEntity> getMediaItems() {
        return this.mediaItems;
    }

    /**
     * Set list of MediaItemsEntity.
     *
     * @param mediaItems list of media item
     * @see MediaItemsEntity
     */
    public void setMediaItems(@NonNull final List<@NonNull MediaItemsEntity> mediaItems) {
        this.mediaItems = mediaItems;
    }

    /**
     * Add MediaItemsEntity.
     *
     * @param mediaItem
     * @return added media item entity with library section set.
     * @see MediaItemsEntity
     */
    @NonNull
    public MediaItemsEntity addMediaItems(@NonNull final MediaItemsEntity mediaItem) {
        getMediaItems().add(mediaItem);
        mediaItem.setLibrarySection(this);

        return mediaItem;
    }

    /**
     * Remove MediaItemsEntity.
     *
     * @param mediaItem
     * @return removed media item entity with library section unset.
     * @see MediaItemsEntity
     */
    @NonNull
    public MediaItemsEntity removeMediaItems(@NonNull final MediaItemsEntity mediaItem) {
        getMediaItems().remove(mediaItem);
        mediaItem.setLibrarySection(null);

        return mediaItem;
    }

    /**
     * Get list of linked MetadataItemViewsEntity.
     *
     * @return list of linked MetadataItemViewsEntity
     * @see MetadataItemViewsEntity
     */
    @NonNull
    public List<@NonNull MetadataItemViewsEntity> getMetadataItemViews() {
        return this.metadataItemSettingsEntities;
    }

    /**
     * Set list of MetadataItemViewsEntity.
     *
     * @param metadataItemViewsEntities list of MetadataItemViewsEntity
     * @see MetadataItemViewsEntity
     */
    public void setMetadataItemViews(@NonNull final List<@NonNull MetadataItemViewsEntity> metadataItemViewsEntities) {
        this.metadataItemSettingsEntities = metadataItemViewsEntities;
    }

    /**
     * Add MetadataItemViewsEntity.
     *
     * @param metadataItemViewsEntity
     * @return added meta data item view with library section set.
     * @see MetadataItemViewsEntity
     */
    @NonNull
    public MetadataItemViewsEntity addMetadataItemViews(@NonNull final MetadataItemViewsEntity metadataItemViewsEntity) {
        getMetadataItemViews().add(metadataItemViewsEntity);
        metadataItemViewsEntity.setLibrarySection(this);

        return metadataItemViewsEntity;
    }

    /**
     * Remove MetadataItemViewsEntity.
     *
     * @param metadataItemViewsEntity
     * @return removed meta data item view with library section unset.
     * @see MetadataItemViewsEntity
     */
    @NonNull
    public MetadataItemViewsEntity removeMetadataItemViews(@NonNull final MetadataItemViewsEntity metadataItemViewsEntity) {
        getMetadataItemViews().remove(metadataItemViewsEntity);
        metadataItemViewsEntity.setLibrarySection(null);

        return metadataItemViewsEntity;
    }

    /**
     * Get list of linked SectionLocationsEntity
     *
     * @return list of linked SectionLocationsEntity
     * @see SectionLocationsEntity
     */
    @NonNull
    public List<@NonNull SectionLocationsEntity> getSectionLocations() {
        return this.sectionLocationsEntities;
    }

    /**
     * Set list of SectionLocationsEntity.
     *
     * @param sectionLocationsEntities
     * @see SectionLocationsEntity
     */
    public void setSectionLocations(@NonNull final List<@NonNull SectionLocationsEntity> sectionLocationsEntities) {
        this.sectionLocationsEntities = sectionLocationsEntities;
    }

    /**
     * Add SectionLocationsEntity.
     *
     * @param sectionLocationsEntity
     * @return added section location with library section set.
     * @see SectionLocationsEntity
     */
    public SectionLocationsEntity addSectionLocations(@NonNull final SectionLocationsEntity sectionLocationsEntity) {
        getSectionLocations().add(sectionLocationsEntity);
        sectionLocationsEntity.setLibrarySection(this);

        return sectionLocationsEntity;
    }

    /**
     * Remove SectionLocationsEntity.
     *
     * @param sectionLocationsEntity
     * @return remove section location with library section unset.
     * @see SectionLocationsEntity
     */
    public SectionLocationsEntity removeSectionLocations(@NonNull final SectionLocationsEntity sectionLocationsEntity) {
        getSectionLocations().remove(sectionLocationsEntity);
        sectionLocationsEntity.setLibrarySection(null);

        return sectionLocationsEntity;
    }

    /**
     * Get list of linked DirectoriesEntity.
     *
     * @return list of linked DirectoriesEntity
     * @see DirectoriesEntity
     */
    @NonNull
    public List<@NonNull DirectoriesEntity> getDirectories() {
        return this.directoriesEntities;
    }

    /**
     * Set list of DirectoriesEntity.
     *
     * @param directoriesEntities list of DirectoriesEntity
     * @see DirectoriesEntity
     */
    public void setDirectory(@NonNull final List<@NonNull DirectoriesEntity> directoriesEntities) {
        this.directoriesEntities = directoriesEntities;
    }

    /**
     * Add DirectoriesEntity.
     *
     * @param directory
     * @return remove directory with library section set.
     * @see DirectoriesEntity
     */
    @NonNull
    public DirectoriesEntity addDirectory(@NonNull final DirectoriesEntity directory) {
        getDirectories().add(directory);
        directory.setLibrarySection(this);

        return directory;
    }

    /**
     * Remove DirectoriesEntity.
     *
     * @param directory
     * @return remove directory with library section unset.
     * @see DirectoriesEntity
     */
    @NonNull
    public DirectoriesEntity removeDirectory(@NonNull final DirectoriesEntity directory) {
        getDirectories().remove(directory);
        directory.setLibrarySection(null);

        return directory;
    }
}
