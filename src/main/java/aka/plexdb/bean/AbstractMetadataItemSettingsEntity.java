package aka.plexdb.bean;

import java.io.Serializable;

import java.util.Date;
import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import org.eclipse.jdt.annotation.Nullable;

/**
 * Persistent class for entity stored in table "metadata_item_settings".
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class AbstractMetadataItemSettingsEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Nullable
    @Column(name = "\"id\"")
    private Integer id;

    @Nullable
    @Column(name = "\"guid\"")
    private String guid;

    @Nullable
    @Column(name = "\"rating\"")
    private Float rating;

    @Nullable
    @Column(name = "\"view_offset\"")
    private Integer viewOffset;

    @Nullable
    @Column(name = "\"view_count\"")
    private Integer viewCount;

    @Nullable
    @Column(name = "\"last_viewed_at\"")
    private Date lastViewedAt;

    @Nullable
    @Column(name = "\"created_at\"")
    private Date createdAt;

    @Nullable
    @Column(name = "\"updated_at\"")
    private Date updatedAt;

    @Nullable
    @Column(name = "\"skip_count\"")
    private Integer skipCount;

    @Nullable
    @Column(name = "\"last_skipped_at\"")
    private Date lastSkippedAt;

    @Nullable
    @Column(name = "\"changed_at\"")
    private Integer changedAt;

    /**
     * Set id.
     * 
     * @param id
     */  
    public void setId(@Nullable Integer id) {
        this.id = id;
    }

    /**
     * Get id.
     * 
     * @return id
     */
    @Nullable
    public Integer getId() {
        return this.id;
    }


    // DATABASE MAPPING : guid ( VARCHAR(255) ) 
    /**
     * Set guid.
     * 
     * @param guid
     */    
    public void setGuid(@Nullable String guid) {
        this.guid = guid;
    }

    /**
     * Get guid.
     * 
     * @return guid
     */
    @Nullable
    public String getGuid() {
        return this.guid;
    }

    // DATABASE MAPPING : rating ( FLOAT ) 
    /**
     * Set rating.
     * 
     * @param rating
     */    
    public void setRating(@Nullable Float rating) {
        this.rating = rating;
    }

    /**
     * Get rating.
     * 
     * @return rating
     */
    @Nullable
    public Float getRating() {
        return this.rating;
    }

    // DATABASE MAPPING : view_offset ( INTEGER ) 
    /**
     * Set viewOffset.
     * 
     * @param viewOffset
     */    
    public void setViewOffset(@Nullable Integer viewOffset) {
        this.viewOffset = viewOffset;
    }

    /**
     * Get viewOffset.
     * 
     * @return viewOffset
     */
    @Nullable
    public Integer getViewOffset() {
        return this.viewOffset;
    }

    // DATABASE MAPPING : view_count ( INTEGER ) 
    /**
     * Set viewCount.
     * 
     * @param viewCount
     */    
    public void setViewCount(@Nullable Integer viewCount) {
        this.viewCount = viewCount;
    }

    /**
     * Get viewCount.
     * 
     * @return viewCount
     */
    @Nullable
    public Integer getViewCount() {
        return this.viewCount;
    }

    // DATABASE MAPPING : last_viewed_at ( DATETIME ) 
    /**
     * Set lastViewedAt.
     * 
     * @param lastViewedAt
     */    
    public void setLastViewedAt(@Nullable Date lastViewedAt) {
        this.lastViewedAt = lastViewedAt;
    }

    /**
     * Get lastViewedAt.
     * 
     * @return lastViewedAt
     */
    @Nullable
    public Date getLastViewedAt() {
        return this.lastViewedAt;
    }

    // DATABASE MAPPING : created_at ( DATETIME ) 
    /**
     * Set createdAt.
     * 
     * @param createdAt
     */    
    public void setCreatedAt(@Nullable Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * Get createdAt.
     * 
     * @return createdAt
     */
    @Nullable
    public Date getCreatedAt() {
        return this.createdAt;
    }

    // DATABASE MAPPING : updated_at ( DATETIME ) 
    /**
     * Set updatedAt.
     * 
     * @param updatedAt
     */    
    public void setUpdatedAt(@Nullable Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * Get updatedAt.
     * 
     * @return updatedAt
     */
    @Nullable
    public Date getUpdatedAt() {
        return this.updatedAt;
    }

    // DATABASE MAPPING : skip_count ( INTEGER ) 
    /**
     * Set skipCount.
     * 
     * @param skipCount
     */    
    public void setSkipCount(@Nullable Integer skipCount) {
        this.skipCount = skipCount;
    }

    /**
     * Get skipCount.
     * 
     * @return skipCount
     */
    @Nullable
    public Integer getSkipCount() {
        return this.skipCount;
    }

    // DATABASE MAPPING : last_skipped_at ( DATETIME ) 
    /**
     * Set lastSkippedAt.
     * 
     * @param lastSkippedAt
     */    
    public void setLastSkippedAt(@Nullable Date lastSkippedAt) {
        this.lastSkippedAt = lastSkippedAt;
    }

    /**
     * Get lastSkippedAt.
     * 
     * @return lastSkippedAt
     */
    @Nullable
    public Date getLastSkippedAt() {
        return this.lastSkippedAt;
    }

    // DATABASE MAPPING : changed_at ( INTEGER(8) ) 
    /**
     * Set changedAt.
     * 
     * @param changedAt
     */    
    public void setChangedAt(@Nullable Integer changedAt) {
        this.changedAt = changedAt;
    }

    /**
     * Get changedAt.
     * 
     * @return changedAt
     */
    @Nullable
    public Integer getChangedAt() {
        return this.changedAt;
    }

}
