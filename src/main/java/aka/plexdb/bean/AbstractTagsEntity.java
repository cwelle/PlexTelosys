package aka.plexdb.bean;

import java.io.Serializable;

import java.util.Date;
import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import org.eclipse.jdt.annotation.Nullable;

/**
 * Persistent class for entity stored in table "tags".
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class AbstractTagsEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Nullable
    @Column(name = "\"id\"")
    private Integer id;

    @Nullable
    @Column(name = "\"tag\"")
    private String tag;

    @Nullable
    @Column(name = "\"tag_type\"")
    private Integer tagType;

    @Nullable
    @Column(name = "\"user_thumb_url\"")
    private String userThumbUrl;

    @Nullable
    @Column(name = "\"user_art_url\"")
    private String userArtUrl;

    @Nullable
    @Column(name = "\"user_music_url\"")
    private String userMusicUrl;

    @Nullable
    @Column(name = "\"created_at\"")
    private Date createdAt;

    @Nullable
    @Column(name = "\"updated_at\"")
    private Date updatedAt;

    @Nullable
    @Column(name = "\"tag_value\"")
    private Integer tagValue;

    @Nullable
    @Column(name = "\"extra_data\"")
    private String extraData;

    /**
     * Set id.
     * 
     * @param id
     */  
    public void setId(@Nullable Integer id) {
        this.id = id;
    }

    /**
     * Get id.
     * 
     * @return id
     */
    @Nullable
    public Integer getId() {
        return this.id;
    }


    // DATABASE MAPPING : tag ( VARCHAR(255) ) 
    /**
     * Set tag.
     * 
     * @param tag
     */    
    public void setTag(@Nullable String tag) {
        this.tag = tag;
    }

    /**
     * Get tag.
     * 
     * @return tag
     */
    @Nullable
    public String getTag() {
        return this.tag;
    }

    // DATABASE MAPPING : tag_type ( INTEGER ) 
    /**
     * Set tagType.
     * 
     * @param tagType
     */    
    public void setTagType(@Nullable Integer tagType) {
        this.tagType = tagType;
    }

    /**
     * Get tagType.
     * 
     * @return tagType
     */
    @Nullable
    public Integer getTagType() {
        return this.tagType;
    }

    // DATABASE MAPPING : user_thumb_url ( VARCHAR(255) ) 
    /**
     * Set userThumbUrl.
     * 
     * @param userThumbUrl
     */    
    public void setUserThumbUrl(@Nullable String userThumbUrl) {
        this.userThumbUrl = userThumbUrl;
    }

    /**
     * Get userThumbUrl.
     * 
     * @return userThumbUrl
     */
    @Nullable
    public String getUserThumbUrl() {
        return this.userThumbUrl;
    }

    // DATABASE MAPPING : user_art_url ( VARCHAR(255) ) 
    /**
     * Set userArtUrl.
     * 
     * @param userArtUrl
     */    
    public void setUserArtUrl(@Nullable String userArtUrl) {
        this.userArtUrl = userArtUrl;
    }

    /**
     * Get userArtUrl.
     * 
     * @return userArtUrl
     */
    @Nullable
    public String getUserArtUrl() {
        return this.userArtUrl;
    }

    // DATABASE MAPPING : user_music_url ( VARCHAR(255) ) 
    /**
     * Set userMusicUrl.
     * 
     * @param userMusicUrl
     */    
    public void setUserMusicUrl(@Nullable String userMusicUrl) {
        this.userMusicUrl = userMusicUrl;
    }

    /**
     * Get userMusicUrl.
     * 
     * @return userMusicUrl
     */
    @Nullable
    public String getUserMusicUrl() {
        return this.userMusicUrl;
    }

    // DATABASE MAPPING : created_at ( DATETIME ) 
    /**
     * Set createdAt.
     * 
     * @param createdAt
     */    
    public void setCreatedAt(@Nullable Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * Get createdAt.
     * 
     * @return createdAt
     */
    @Nullable
    public Date getCreatedAt() {
        return this.createdAt;
    }

    // DATABASE MAPPING : updated_at ( DATETIME ) 
    /**
     * Set updatedAt.
     * 
     * @param updatedAt
     */    
    public void setUpdatedAt(@Nullable Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * Get updatedAt.
     * 
     * @return updatedAt
     */
    @Nullable
    public Date getUpdatedAt() {
        return this.updatedAt;
    }

    // DATABASE MAPPING : tag_value ( INTEGER ) 
    /**
     * Set tagValue.
     * 
     * @param tagValue
     */    
    public void setTagValue(@Nullable Integer tagValue) {
        this.tagValue = tagValue;
    }

    /**
     * Get tagValue.
     * 
     * @return tagValue
     */
    @Nullable
    public Integer getTagValue() {
        return this.tagValue;
    }

    // DATABASE MAPPING : extra_data ( VARCHAR(255) ) 
    /**
     * Set extraData.
     * 
     * @param extraData
     */    
    public void setExtraData(@Nullable String extraData) {
        this.extraData = extraData;
    }

    /**
     * Get extraData.
     * 
     * @return extraData
     */
    @Nullable
    public String getExtraData() {
        return this.extraData;
    }

}
