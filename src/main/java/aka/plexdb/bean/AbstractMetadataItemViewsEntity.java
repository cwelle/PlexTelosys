package aka.plexdb.bean;

import java.io.Serializable;

import java.util.Date;
import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import org.eclipse.jdt.annotation.Nullable;

/**
 * Persistent class for entity stored in table "metadata_item_views".
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class AbstractMetadataItemViewsEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Nullable
    @Column(name = "\"id\"")
    private Integer id;

    @Nullable
    @Column(name = "\"guid\"")
    private String guid;

    @Nullable
    @Column(name = "\"metadata_type\"")
    private Integer metadataType;

    @Nullable
    @Column(name = "\"grandparent_title\"")
    private String grandparentTitle;

    @Nullable
    @Column(name = "\"parent_index\"")
    private Integer parentIndex;

    @Nullable
    @Column(name = "\"parent_title\"")
    private String parentTitle;

    @Nullable
    @Column(name = "\"title\"")
    private String title;

    @Nullable
    @Column(name = "\"thumb_url\"")
    private String thumbUrl;

    @Nullable
    @Column(name = "\"viewed_at\"")
    private Date viewedAt;

    @Nullable
    @Column(name = "\"grandparent_guid\"")
    private String grandparentGuid;

    @Nullable
    @Column(name = "\"originally_available_at\"")
    private Date originallyAvailableAt;

    /**
     * Set id.
     * 
     * @param id
     */  
    public void setId(@Nullable Integer id) {
        this.id = id;
    }

    /**
     * Get id.
     * 
     * @return id
     */
    @Nullable
    public Integer getId() {
        return this.id;
    }


    // DATABASE MAPPING : guid ( VARCHAR(255) ) 
    /**
     * Set guid.
     * 
     * @param guid
     */    
    public void setGuid(@Nullable String guid) {
        this.guid = guid;
    }

    /**
     * Get guid.
     * 
     * @return guid
     */
    @Nullable
    public String getGuid() {
        return this.guid;
    }

    // DATABASE MAPPING : metadata_type ( INTEGER ) 
    /**
     * Set metadataType.
     * 
     * @param metadataType
     */    
    public void setMetadataType(@Nullable Integer metadataType) {
        this.metadataType = metadataType;
    }

    /**
     * Get metadataType.
     * 
     * @return metadataType
     */
    @Nullable
    public Integer getMetadataType() {
        return this.metadataType;
    }


    // DATABASE MAPPING : grandparent_title ( VARCHAR(255) ) 
    /**
     * Set grandparentTitle.
     * 
     * @param grandparentTitle
     */    
    public void setGrandparentTitle(@Nullable String grandparentTitle) {
        this.grandparentTitle = grandparentTitle;
    }

    /**
     * Get grandparentTitle.
     * 
     * @return grandparentTitle
     */
    @Nullable
    public String getGrandparentTitle() {
        return this.grandparentTitle;
    }

    // DATABASE MAPPING : parent_index ( INTEGER ) 
    /**
     * Set parentIndex.
     * 
     * @param parentIndex
     */    
    public void setParentIndex(@Nullable Integer parentIndex) {
        this.parentIndex = parentIndex;
    }

    /**
     * Get parentIndex.
     * 
     * @return parentIndex
     */
    @Nullable
    public Integer getParentIndex() {
        return this.parentIndex;
    }

    // DATABASE MAPPING : parent_title ( VARCHAR(255) ) 
    /**
     * Set parentTitle.
     * 
     * @param parentTitle
     */    
    public void setParentTitle(@Nullable String parentTitle) {
        this.parentTitle = parentTitle;
    }

    /**
     * Get parentTitle.
     * 
     * @return parentTitle
     */
    @Nullable
    public String getParentTitle() {
        return this.parentTitle;
    }


    // DATABASE MAPPING : title ( VARCHAR(255) ) 
    /**
     * Set title.
     * 
     * @param title
     */    
    public void setTitle(@Nullable String title) {
        this.title = title;
    }

    /**
     * Get title.
     * 
     * @return title
     */
    @Nullable
    public String getTitle() {
        return this.title;
    }

    // DATABASE MAPPING : thumb_url ( VARCHAR(255) ) 
    /**
     * Set thumbUrl.
     * 
     * @param thumbUrl
     */    
    public void setThumbUrl(@Nullable String thumbUrl) {
        this.thumbUrl = thumbUrl;
    }

    /**
     * Get thumbUrl.
     * 
     * @return thumbUrl
     */
    @Nullable
    public String getThumbUrl() {
        return this.thumbUrl;
    }

    // DATABASE MAPPING : viewed_at ( DATETIME ) 
    /**
     * Set viewedAt.
     * 
     * @param viewedAt
     */    
    public void setViewedAt(@Nullable Date viewedAt) {
        this.viewedAt = viewedAt;
    }

    /**
     * Get viewedAt.
     * 
     * @return viewedAt
     */
    @Nullable
    public Date getViewedAt() {
        return this.viewedAt;
    }

    // DATABASE MAPPING : grandparent_guid ( VARCHAR(255) ) 
    /**
     * Set grandparentGuid.
     * 
     * @param grandparentGuid
     */    
    public void setGrandparentGuid(@Nullable String grandparentGuid) {
        this.grandparentGuid = grandparentGuid;
    }

    /**
     * Get grandparentGuid.
     * 
     * @return grandparentGuid
     */
    @Nullable
    public String getGrandparentGuid() {
        return this.grandparentGuid;
    }

    // DATABASE MAPPING : originally_available_at ( DATETIME ) 
    /**
     * Set originallyAvailableAt.
     * 
     * @param originallyAvailableAt
     */    
    public void setOriginallyAvailableAt(@Nullable Date originallyAvailableAt) {
        this.originallyAvailableAt = originallyAvailableAt;
    }

    /**
     * Get originallyAvailableAt.
     * 
     * @return originallyAvailableAt
     */
    @Nullable
    public Date getOriginallyAvailableAt() {
        return this.originallyAvailableAt;
    }

}
