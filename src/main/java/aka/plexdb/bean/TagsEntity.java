package aka.plexdb.bean;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;

import aka.plexdb.bean.constants.TagType;

/**
 * Persistent class for entity stored in table "tags".
 *
 * This file can be filled with association or specific method.
 * REGENERATION WILL ERASED YOUR CHANGES.
 */
@Entity
@Table(name = "\"tags\"")
public class TagsEntity extends AbstractTagsEntity {

    private static final long serialVersionUID = 1L;

    // bi-directional many-to-one association to MetadataItemsEntity
    @ManyToOne(targetEntity = MetadataItemsEntity.class, optional = false)
    @JoinColumn(name = "\"metadata_item_id\"", referencedColumnName = "\"id\"")
    @Nullable
    private MetadataItemsEntity metadataItem;

    // bi-directional one-to-many association to TaggingsEntity
    @OneToMany(mappedBy = "tags")
    @NonNull
    private List<@NonNull TaggingsEntity> taggingsEntities = new ArrayList<>();

    /**
     * Get linked MetadataItemsEntity.
     *
     * @return linked MetadataItemsEntity
     * @see MetadataItemsEntity
     */
    @Nullable
    public MetadataItemsEntity getMetadataItem() {
        return this.metadataItem;
    }

    /**
     * Set linked MetadataItemsEntity.
     *
     * @param metadataItems
     */
    public void setMetadataItem(@Nullable final MetadataItemsEntity metadataItems) {
        this.metadataItem = metadataItems;
    }

    /**
     * Get TagType.
     *
     * @return TagType enum
     * @see TagType
     */
    @Nullable
    public TagType getTagTypeEnum() {
        return TagType.getTagType(getTagType());
    }

    /**
     * Set TagType.
     *
     * @param tagType
     * @see TagType
     */
    public void setTagTypeEnum(@Nullable final TagType tagType) {
        if (tagType == null) {
            setTagType(null);
        } else {
            setTagType(tagType.getValue());
        }
    }

    /**
     * Get list of linked TaggingsEntity.
     *
     * @return list of linked TaggingsEntity
     * @see TaggingsEntity
     */
    @NonNull
    public List<@NonNull TaggingsEntity> getTaggings() {
        return this.taggingsEntities;
    }

    /**
     * Set list of linked TaggingsEntity.
     *
     * @param taggingsEntities list of linked TaggingsEntity
     * @see TaggingsEntity
     */
    public void setTaggings(@NonNull final List<@NonNull TaggingsEntity> taggingsEntities) {
        this.taggingsEntities = taggingsEntities;
    }

    /**
     * Add TaggingsEntity.
     *
     * @param tagging
     * @return added tagging with tag set.
     * @see TaggingsEntity
     */
    @NonNull
    public TaggingsEntity addTagging(@NonNull final TaggingsEntity tagging) {
        getTaggings().add(tagging);
        tagging.setTag(this);

        return tagging;
    }

    /**
     * Remove TaggingsEntity.
     *
     * @param tagging
     * @return removed tagging with tag unset.
     * @see TaggingsEntity
     */
    @NonNull
    public TaggingsEntity removeTagging(@NonNull final TaggingsEntity tagging) {
        getTaggings().remove(tagging);
        tagging.setTag(null);

        return tagging;
    }
}
