package aka.plexdb.bean.guid;

import org.eclipse.jdt.annotation.NonNull;

/**
 * GuId mapper.
 *
 * @author Cha
 */
public class GuID {

    @NonNull
    private GuIDType guIDType = GuIDType.UNKNOWN;
    @NonNull
    private String value = "";

    /**
     * Constructor.
     *
     * @param guid
     */
    public GuID(@NonNull final String guid) {
        final int index = guid.indexOf("://");
        if (index > 0) {
            final int size = guid.length();
            final String type = guid.substring(0, index);
            this.guIDType = GuIDType.getGuIDType(type);

            final String urlValue = guid.substring(index + 3, size);
            if (urlValue == null) {
                this.value = "";
            } else {
                this.value = urlValue;
            }
        }
    }

    /**
     * Get the GuIDType
     *
     * @return the guIDType
     * @see GuIDType
     */
    @NonNull
    public final GuIDType getGuIDType() {
        return this.guIDType;
    }

    /**
     * Get the value.
     *
     * @return the value
     */
    @NonNull
    public final String getValue() {
        return this.value;
    }
}
