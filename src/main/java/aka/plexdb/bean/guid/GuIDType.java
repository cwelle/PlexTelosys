package aka.plexdb.bean.guid;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;

/**
 * @author Charlotte
 */
public enum GuIDType {

    /**
     * Constant for the <code>UNKNOWN</code>.
     */
    UNKNOWN("Unknown", ""),

    /**
     * Constant for the <code>THE_MOVIE_DB</code>.
     */
    THE_MOVIE_DB("The Movie DB", "com.plexapp.agents.themoviedb"),

    /**
     * Constant for the <code>FAN_ART_TV</code>.
     */
    FAN_ART_TV("Fan art TV", "com.plexapp.agents.fanarttv"),

    /**
     * Constant for the <code>NONE</code>.
     */
    NONE("None", "com.plexapp.agents.none"),

    /**
     * Constant for the <code>IMDB</code>.
     */
    IMDB("IMDB", "com.plexapp.agents.imdb"),

    /**
     * Constant for the <code>THE_TV_DB</code>.
     */
    THE_TV_DB("The TV DB", "com.plexapp.agents.thetvdb"),

    /**
     * Constant for the <code>MOVIE_POSTER_DB</code>.
     */
    MOVIE_POSTER_DB("Country", "com.plexapp.agents.movieposterdb");

    @NonNull
    private final String name;
    @NonNull
    private final String value;

    GuIDType(@NonNull final String name, @NonNull final String value) {
        this.name = name;
        this.value = value;
    }

    /**
     * Get the name.
     *
     * @return value.
     */
    @NonNull
    public String getName() {
        return this.name;
    }

    /**
     * Get the value to be store in the database.
     *
     * @return value.
     */
    @NonNull
    public String getValue() {
        return this.value;
    }

    /**
     * Get GuIDType enumeration value corresponding to the given String tagType.
     *
     * @param guid guid
     * @return GuIDType
     */
    @NonNull
    public static GuIDType getGuIDType(@Nullable final String guid) {
        if (guid == null) {
            return UNKNOWN;
        } else {
            for (final GuIDType e : GuIDType.values()) {
                if (guid.equals(e.getValue())) {
                    return e;
                }
            }
        }

        return UNKNOWN;
    }
}
