package aka.plexdb.bean;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.eclipse.jdt.annotation.Nullable;

/**
 * Persistent class for entity stored in table "media_streams".
 *
 * This file can be filled with association or specific method.
 * REGENERATION WILL ERASED YOUR CHANGES.
 */
@Entity
@Table(name = "\"media_streams\"")
public class MediaStreamsEntity extends AbstractMediaStreamsEntity {

    private static final long serialVersionUID = 1L;

    // bi-directional many-to-one association to MediaPartsEntity
    @ManyToOne(targetEntity = MediaPartsEntity.class, optional = true)
    @JoinColumn(name = "\"media_part_id\"", referencedColumnName = "\"id\"")
    @Nullable
    private MediaPartsEntity mediaPart;

    // bi-directional many-to-one association to StreamTypesEntity
    @ManyToOne(targetEntity = StreamTypesEntity.class, optional = true)
    @JoinColumn(name = "\"stream_type_id\"", referencedColumnName = "\"id\"")
    @Nullable
    private StreamTypesEntity streamTypesEntity;

    // bi-directional many-to-one association to MediaItemsEntity
    @ManyToOne(targetEntity = MediaItemsEntity.class, optional = true)
    @JoinColumn(name = "\"media_item_id\"", referencedColumnName = "\"id\"")
    @Nullable
    private MediaItemsEntity mediaItem;

    /**
     * Get linked MediaPartsEntity.
     *
     * @return linked MediaPartsEntity
     * @see MediaPartsEntity
     */
    @Nullable
    public MediaPartsEntity getMediaPart() {
        return this.mediaPart;
    }

    /**
     * Set linked MediaPartsEntity.
     *
     * @param mediaPart
     * @see MediaPartsEntity
     */
    public void setMediaPart(@Nullable final MediaPartsEntity mediaPart) {
        this.mediaPart = mediaPart;
    }

    /**
     * Get linked StreamTypesEntity.
     *
     * @return linked StreamTypesEntity
     * @see StreamTypesEntity
     */
    @Nullable
    public StreamTypesEntity getStreamType() {
        return this.streamTypesEntity;
    }

    /**
     * Set linked StreamTypesEntity.
     *
     * @param streamTypesEntity
     * @see StreamTypesEntity
     */
    public void setStreamType(@Nullable final StreamTypesEntity streamTypesEntity) {
        this.streamTypesEntity = streamTypesEntity;
    }

    /**
     * Get linked MediaItemsEntity.
     *
     * @return linked MediaItemsEntity
     * @see MediaItemsEntity
     */
    @Nullable
    public MediaItemsEntity getMediaItem() {
        return this.mediaItem;
    }

    /**
     * Set linked MediaItemsEntity.
     *
     * @param mediaItemsEntity
     * @see MediaItemsEntity
     */
    public void setMediaItem(@Nullable final MediaItemsEntity mediaItemsEntity) {
        this.mediaItem = mediaItemsEntity;
    }
}
