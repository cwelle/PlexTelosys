package aka.plexdb.bean;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.eclipse.jdt.annotation.Nullable;

/**
 * Persistent class for entity stored in table "taggings".
 *
 * This file can be filled with association or specific method.
 * REGENERATION WILL ERASED YOUR CHANGES.
 */
@Entity
@Table(name = "\"taggings\"")
public class TaggingsEntity extends AbstractTaggingsEntity {

    private static final long serialVersionUID = 1L;

    // bi-directional one-to-many association to MetadataItemsEntity
    @ManyToOne(targetEntity = MetadataItemsEntity.class, optional = true)
    @JoinColumn(name = "\"metadata_item_id\"", referencedColumnName = "\"id\"")
    @Nullable
    private MetadataItemsEntity metadataItem;

    // bi-directional many-to-one association to TagsEntity
    @ManyToOne(targetEntity = TagsEntity.class, optional = true)
    @JoinColumn(name = "\"tag_id\"", referencedColumnName = "\"id\"")
    @Nullable
    private TagsEntity tags;

    /**
     * Get linked MetadataItemsEntity.
     *
     * @return linked MetadataItemsEntity
     * @see MetadataItemsEntity
     */
    @Nullable
    public MetadataItemsEntity getMetadataItem() {
        return this.metadataItem;
    }

    /**
     * Set linked MetadataItemsEntity.
     *
     * @param metadataItemsEntity
     * @see MetadataItemsEntity
     */
    public void setMetadataItem(@Nullable final MetadataItemsEntity metadataItemsEntity) {
        this.metadataItem = metadataItemsEntity;
    }

    /**
     * Get linked TagsEntity.
     *
     * @return linked TagsEntity
     * @see TagsEntity
     */
    @Nullable
    public TagsEntity getTag() {
        return this.tags;
    }

    /**
     * Set linked TagsEntity
     *
     * @param tag
     * @see TagsEntity
     */
    public void setTag(@Nullable final TagsEntity tag) {
        this.tags = tag;
    }

}
