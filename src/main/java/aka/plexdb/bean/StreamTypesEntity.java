package aka.plexdb.bean;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.eclipse.jdt.annotation.NonNull;

/**
 * Persistent class for entity stored in table "stream_types".
 *
 * This file can be filled with association or specific method.
 * REGENERATION WILL ERASED YOUR CHANGES.
 */
@Entity
@Table(name = "\"stream_types\"")
public class StreamTypesEntity extends AbstractStreamTypesEntity {

    private static final long serialVersionUID = 1L;

    // bi-directional one-to-many association to MediaStreamsEntity
    @OneToMany(mappedBy = "streamTypesEntity")
    @NonNull
    private List<@NonNull MediaStreamsEntity> streamTypes = new ArrayList<>();

    /**
     * Get list of linked MediaStreamsEntity.
     *
     * @return list of linked MediaStreamsEntity
     * @see MediaStreamsEntity
     */
    @NonNull
    public List<@NonNull MediaStreamsEntity> getMediaStreams() {
        return this.streamTypes;
    }

    /**
     * Set list of linked MediaStreamsEntity.
     *
     * @param streamTypes list of linked MediaStreamsEntity
     * @see MediaStreamsEntity
     */
    public void setMediaStreams(@NonNull final List<@NonNull MediaStreamsEntity> streamTypes) {
        this.streamTypes = streamTypes;
    }

    /**
     * Add MediaItemsEntity.
     *
     * @param mediaStreams
     * @return added media streams with stream type set.
     * @see MediaItemsEntity
     */
    @NonNull
    public MediaStreamsEntity addMediaStream(@NonNull final MediaStreamsEntity mediaStreams) {
        getMediaStreams().add(mediaStreams);
        mediaStreams.setStreamType(this);

        return mediaStreams;
    }

    /**
     * Remove MediaItemsEntity.
     *
     * @param mediaStreams
     * @return removed media streams with stream type unset.
     * @see MediaItemsEntity
     */
    @NonNull
    public MediaStreamsEntity removeMediaStream(@NonNull final MediaStreamsEntity mediaStreams) {
        getMediaStreams().remove(mediaStreams);
        mediaStreams.setStreamType(null);

        return mediaStreams;
    }

}
