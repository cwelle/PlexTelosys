package aka.plexdb.bean;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;

/**
 * Persistent class for entity stored in table "section_locations".
 *
 * This file can be filled with association or specific method.
 * REGENERATION WILL ERASED YOUR CHANGES.
 */
@Entity
@Table(name = "\"section_locations\"")
public class SectionLocationsEntity extends AbstractSectionLocationsEntity {

    private static final long serialVersionUID = 1L;

    // bi-directional one-to-many association to MediaItemsEntity
    @OneToMany(mappedBy = "sectionLocation")
    @NonNull
    private List<@NonNull MediaItemsEntity> mediaItems = new ArrayList<>();

    // bi-directional many-to-one association to LibrarySectionsEntity
    @ManyToOne(targetEntity = LibrarySectionsEntity.class, optional = true)
    @JoinColumn(name = "\"library_section_id\"", referencedColumnName = "\"id\"")
    @Nullable
    private LibrarySectionsEntity librarySection;

    /**
     * Get list of linked MediaItemsEntity.
     *
     * @return list of linked MediaItemsEntity
     * @see MediaItemsEntity
     */
    @NonNull
    public List<@NonNull MediaItemsEntity> getMediaItems() {
        return this.mediaItems;
    }

    /**
     * Set list of linked MediaItemsEntity.
     *
     * @param mediaItems list of linked MediaItemsEntity
     * @see MediaItemsEntity
     */
    public void setMediaItems(@NonNull final List<@NonNull MediaItemsEntity> mediaItems) {
        this.mediaItems = mediaItems;
    }

    /**
     * Add MediaItemsEntity.
     *
     * @param mediaItem
     * @return added media item with section location set.
     * @see MediaItemsEntity
     */
    @NonNull
    public MediaItemsEntity addMediaItems(@NonNull final MediaItemsEntity mediaItem) {
        getMediaItems().add(mediaItem);
        mediaItem.setSectionLocation(this);

        return mediaItem;
    }

    /**
     * Remove MediaItemsEntity.
     *
     * @param mediaItem
     * @return removed media item with section location unset.
     * @see MediaItemsEntity
     */
    @NonNull
    public MediaItemsEntity removeMediaItems(@NonNull final MediaItemsEntity mediaItem) {
        getMediaItems().remove(mediaItem);
        mediaItem.setSectionLocation(null);

        return mediaItem;
    }

    /**
     * Get linked LibrarySectionsEntity.
     *
     * @return linked LibrarySectionsEntity
     * @see LibrarySectionsEntity
     */
    @Nullable
    public LibrarySectionsEntity getLibrarySection() {
        return this.librarySection;
    }

    /**
     * Set linked LibrarySectionsEntity.
     *
     * @param librarySection
     * @see LibrarySectionsEntity
     */
    public void setLibrarySection(@Nullable final LibrarySectionsEntity librarySection) {
        this.librarySection = librarySection;
    }
}
