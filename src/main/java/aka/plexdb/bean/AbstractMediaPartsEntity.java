package aka.plexdb.bean;

import java.io.Serializable;

import java.util.Date;
import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import org.eclipse.jdt.annotation.Nullable;

/**
 * Persistent class for entity stored in table "media_parts".
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class AbstractMediaPartsEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Nullable
    @Column(name = "\"id\"")
    private Integer id;

    @Nullable
    @Column(name = "\"hash\"")
    private String hash;

    @Nullable
    @Column(name = "\"open_subtitle_hash\"")
    private String openSubtitleHash;

    @Nullable
    @Column(name = "\"file\"")
    private String file;

    @Nullable
    @Column(name = "\"size\"")
    private Integer sizePart;

    @Nullable
    @Column(name = "\"duration\"")
    private Integer duration;

    @Nullable
    @Column(name = "\"created_at\"")
    private Date createdAt;

    @Nullable
    @Column(name = "\"updated_at\"")
    private Date updatedAt;

    @Nullable
    @Column(name = "\"deleted_at\"")
    private Date deletedAt;

    @Nullable
    @Column(name = "\"extra_data\"")
    private String extraData;

    /**
     * Set id.
     * 
     * @param id
     */  
    public void setId(@Nullable Integer id) {
        this.id = id;
    }

    /**
     * Get id.
     * 
     * @return id
     */
    @Nullable
    public Integer getId() {
        return this.id;
    }



    // DATABASE MAPPING : hash ( VARCHAR(255) ) 
    /**
     * Set hash.
     * 
     * @param hash
     */    
    public void setHash(@Nullable String hash) {
        this.hash = hash;
    }

    /**
     * Get hash.
     * 
     * @return hash
     */
    @Nullable
    public String getHash() {
        return this.hash;
    }

    // DATABASE MAPPING : open_subtitle_hash ( VARCHAR(255) ) 
    /**
     * Set openSubtitleHash.
     * 
     * @param openSubtitleHash
     */    
    public void setOpenSubtitleHash(@Nullable String openSubtitleHash) {
        this.openSubtitleHash = openSubtitleHash;
    }

    /**
     * Get openSubtitleHash.
     * 
     * @return openSubtitleHash
     */
    @Nullable
    public String getOpenSubtitleHash() {
        return this.openSubtitleHash;
    }

    // DATABASE MAPPING : file ( VARCHAR(255) ) 
    /**
     * Set file.
     * 
     * @param file
     */    
    public void setFile(@Nullable String file) {
        this.file = file;
    }

    /**
     * Get file.
     * 
     * @return file
     */
    @Nullable
    public String getFile() {
        return this.file;
    }


    // DATABASE MAPPING : size ( INTEGER(8) ) 
    /**
     * Set sizePart.
     * 
     * @param sizePart
     */    
    public void setSizePart(@Nullable Integer sizePart) {
        this.sizePart = sizePart;
    }

    /**
     * Get sizePart.
     * 
     * @return sizePart
     */
    @Nullable
    public Integer getSizePart() {
        return this.sizePart;
    }

    // DATABASE MAPPING : duration ( INTEGER ) 
    /**
     * Set duration.
     * 
     * @param duration
     */    
    public void setDuration(@Nullable Integer duration) {
        this.duration = duration;
    }

    /**
     * Get duration.
     * 
     * @return duration
     */
    @Nullable
    public Integer getDuration() {
        return this.duration;
    }

    // DATABASE MAPPING : created_at ( DATETIME ) 
    /**
     * Set createdAt.
     * 
     * @param createdAt
     */    
    public void setCreatedAt(@Nullable Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * Get createdAt.
     * 
     * @return createdAt
     */
    @Nullable
    public Date getCreatedAt() {
        return this.createdAt;
    }

    // DATABASE MAPPING : updated_at ( DATETIME ) 
    /**
     * Set updatedAt.
     * 
     * @param updatedAt
     */    
    public void setUpdatedAt(@Nullable Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * Get updatedAt.
     * 
     * @return updatedAt
     */
    @Nullable
    public Date getUpdatedAt() {
        return this.updatedAt;
    }

    // DATABASE MAPPING : deleted_at ( DATETIME ) 
    /**
     * Set deletedAt.
     * 
     * @param deletedAt
     */    
    public void setDeletedAt(@Nullable Date deletedAt) {
        this.deletedAt = deletedAt;
    }

    /**
     * Get deletedAt.
     * 
     * @return deletedAt
     */
    @Nullable
    public Date getDeletedAt() {
        return this.deletedAt;
    }

    // DATABASE MAPPING : extra_data ( VARCHAR(255) ) 
    /**
     * Set extraData.
     * 
     * @param extraData
     */    
    public void setExtraData(@Nullable String extraData) {
        this.extraData = extraData;
    }

    /**
     * Get extraData.
     * 
     * @return extraData
     */
    @Nullable
    public String getExtraData() {
        return this.extraData;
    }

}
