package aka.plexdb.bean;

import java.io.Serializable;

import java.util.Date;
import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import org.eclipse.jdt.annotation.Nullable;

/**
 * Persistent class for entity stored in table "library_sections".
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class AbstractLibrarySectionsEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Nullable
    @Column(name = "\"id\"")
    private Integer id;

    @Nullable
    @Column(name = "\"name\"")
    private String name;

    @Nullable
    @Column(name = "\"name_sort\"")
    private String nameSort;

    @Nullable
    @Column(name = "\"section_type\"")
    private Integer sectionType;

    @Nullable
    @Column(name = "\"language\"")
    private String language;

    @Nullable
    @Column(name = "\"agent\"")
    private String agent;

    @Nullable
    @Column(name = "\"scanner\"")
    private String scanner;

    @Nullable
    @Column(name = "\"user_thumb_url\"")
    private String userThumbUrl;

    @Nullable
    @Column(name = "\"user_art_url\"")
    private String userArtUrl;

    @Nullable
    @Column(name = "\"user_theme_music_url\"")
    private String userThemeMusicUrl;

    @Nullable
    @Column(name = "\"public\"")
    private Boolean isPublic;

    @Nullable
    @Column(name = "\"created_at\"")
    private Date createdAt;

    @Nullable
    @Column(name = "\"updated_at\"")
    private Date updatedAt;

    @Nullable
    @Column(name = "\"scanned_at\"")
    private Date scannedAt;

    @Nullable
    @Column(name = "\"display_secondary_level\"")
    private Boolean displaySecondaryLevel;

    @Nullable
    @Column(name = "\"user_fields\"")
    private String userFields;

    @Nullable
    @Column(name = "\"query_xml\"")
    private String queryXml;

    @Nullable
    @Column(name = "\"query_type\"")
    private Integer queryType;

    @Nullable
    @Column(name = "\"uuid\"")
    private String uuid;

    @Nullable
    @Column(name = "\"changed_at\"")
    private Integer changedAt;

    /**
     * Set id.
     * 
     * @param id
     */  
    public void setId(@Nullable Integer id) {
        this.id = id;
    }

    /**
     * Get id.
     * 
     * @return id
     */
    @Nullable
    public Integer getId() {
        return this.id;
    }


    // DATABASE MAPPING : name ( VARCHAR(255) ) 
    /**
     * Set name.
     * 
     * @param name
     */    
    public void setName(@Nullable String name) {
        this.name = name;
    }

    /**
     * Get name.
     * 
     * @return name
     */
    @Nullable
    public String getName() {
        return this.name;
    }

    // DATABASE MAPPING : name_sort ( VARCHAR(255) ) 
    /**
     * Set nameSort.
     * 
     * @param nameSort
     */    
    public void setNameSort(@Nullable String nameSort) {
        this.nameSort = nameSort;
    }

    /**
     * Get nameSort.
     * 
     * @return nameSort
     */
    @Nullable
    public String getNameSort() {
        return this.nameSort;
    }

    // DATABASE MAPPING : section_type ( INTEGER ) 
    /**
     * Set sectionType.
     * 
     * @param sectionType
     */    
    public void setSectionType(@Nullable Integer sectionType) {
        this.sectionType = sectionType;
    }

    /**
     * Get sectionType.
     * 
     * @return sectionType
     */
    @Nullable
    public Integer getSectionType() {
        return this.sectionType;
    }

    // DATABASE MAPPING : language ( VARCHAR(255) ) 
    /**
     * Set language.
     * 
     * @param language
     */    
    public void setLanguage(@Nullable String language) {
        this.language = language;
    }

    /**
     * Get language.
     * 
     * @return language
     */
    @Nullable
    public String getLanguage() {
        return this.language;
    }

    // DATABASE MAPPING : agent ( VARCHAR(255) ) 
    /**
     * Set agent.
     * 
     * @param agent
     */    
    public void setAgent(@Nullable String agent) {
        this.agent = agent;
    }

    /**
     * Get agent.
     * 
     * @return agent
     */
    @Nullable
    public String getAgent() {
        return this.agent;
    }

    // DATABASE MAPPING : scanner ( VARCHAR(255) ) 
    /**
     * Set scanner.
     * 
     * @param scanner
     */    
    public void setScanner(@Nullable String scanner) {
        this.scanner = scanner;
    }

    /**
     * Get scanner.
     * 
     * @return scanner
     */
    @Nullable
    public String getScanner() {
        return this.scanner;
    }

    // DATABASE MAPPING : user_thumb_url ( VARCHAR(255) ) 
    /**
     * Set userThumbUrl.
     * 
     * @param userThumbUrl
     */    
    public void setUserThumbUrl(@Nullable String userThumbUrl) {
        this.userThumbUrl = userThumbUrl;
    }

    /**
     * Get userThumbUrl.
     * 
     * @return userThumbUrl
     */
    @Nullable
    public String getUserThumbUrl() {
        return this.userThumbUrl;
    }

    // DATABASE MAPPING : user_art_url ( VARCHAR(255) ) 
    /**
     * Set userArtUrl.
     * 
     * @param userArtUrl
     */    
    public void setUserArtUrl(@Nullable String userArtUrl) {
        this.userArtUrl = userArtUrl;
    }

    /**
     * Get userArtUrl.
     * 
     * @return userArtUrl
     */
    @Nullable
    public String getUserArtUrl() {
        return this.userArtUrl;
    }

    // DATABASE MAPPING : user_theme_music_url ( VARCHAR(255) ) 
    /**
     * Set userThemeMusicUrl.
     * 
     * @param userThemeMusicUrl
     */    
    public void setUserThemeMusicUrl(@Nullable String userThemeMusicUrl) {
        this.userThemeMusicUrl = userThemeMusicUrl;
    }

    /**
     * Get userThemeMusicUrl.
     * 
     * @return userThemeMusicUrl
     */
    @Nullable
    public String getUserThemeMusicUrl() {
        return this.userThemeMusicUrl;
    }

    // DATABASE MAPPING : public ( BOOLEAN ) 
    /**
     * Set isPublic.
     * 
     * @param isPublic
     */    
    public void setIsPublic(@Nullable Boolean isPublic) {
        this.isPublic = isPublic;
    }

    /**
     * Get isPublic.
     * 
     * @return isPublic
     */
    @Nullable
    public Boolean getIsPublic() {
        return this.isPublic;
    }

    // DATABASE MAPPING : created_at ( DATETIME ) 
    /**
     * Set createdAt.
     * 
     * @param createdAt
     */    
    public void setCreatedAt(@Nullable Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * Get createdAt.
     * 
     * @return createdAt
     */
    @Nullable
    public Date getCreatedAt() {
        return this.createdAt;
    }

    // DATABASE MAPPING : updated_at ( DATETIME ) 
    /**
     * Set updatedAt.
     * 
     * @param updatedAt
     */    
    public void setUpdatedAt(@Nullable Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * Get updatedAt.
     * 
     * @return updatedAt
     */
    @Nullable
    public Date getUpdatedAt() {
        return this.updatedAt;
    }

    // DATABASE MAPPING : scanned_at ( DATETIME ) 
    /**
     * Set scannedAt.
     * 
     * @param scannedAt
     */    
    public void setScannedAt(@Nullable Date scannedAt) {
        this.scannedAt = scannedAt;
    }

    /**
     * Get scannedAt.
     * 
     * @return scannedAt
     */
    @Nullable
    public Date getScannedAt() {
        return this.scannedAt;
    }

    // DATABASE MAPPING : display_secondary_level ( BOOLEAN ) 
    /**
     * Set displaySecondaryLevel.
     * 
     * @param displaySecondaryLevel
     */    
    public void setDisplaySecondaryLevel(@Nullable Boolean displaySecondaryLevel) {
        this.displaySecondaryLevel = displaySecondaryLevel;
    }

    /**
     * Get displaySecondaryLevel.
     * 
     * @return displaySecondaryLevel
     */
    @Nullable
    public Boolean getDisplaySecondaryLevel() {
        return this.displaySecondaryLevel;
    }

    // DATABASE MAPPING : user_fields ( VARCHAR(255) ) 
    /**
     * Set userFields.
     * 
     * @param userFields
     */    
    public void setUserFields(@Nullable String userFields) {
        this.userFields = userFields;
    }

    /**
     * Get userFields.
     * 
     * @return userFields
     */
    @Nullable
    public String getUserFields() {
        return this.userFields;
    }

    // DATABASE MAPPING : query_xml ( TEXT ) 
    /**
     * Set queryXml.
     * 
     * @param queryXml
     */    
    public void setQueryXml(@Nullable String queryXml) {
        this.queryXml = queryXml;
    }

    /**
     * Get queryXml.
     * 
     * @return queryXml
     */
    @Nullable
    public String getQueryXml() {
        return this.queryXml;
    }

    // DATABASE MAPPING : query_type ( INTEGER ) 
    /**
     * Set queryType.
     * 
     * @param queryType
     */    
    public void setQueryType(@Nullable Integer queryType) {
        this.queryType = queryType;
    }

    /**
     * Get queryType.
     * 
     * @return queryType
     */
    @Nullable
    public Integer getQueryType() {
        return this.queryType;
    }

    // DATABASE MAPPING : uuid ( VARCHAR(255) ) 
    /**
     * Set uuid.
     * 
     * @param uuid
     */    
    public void setUuid(@Nullable String uuid) {
        this.uuid = uuid;
    }

    /**
     * Get uuid.
     * 
     * @return uuid
     */
    @Nullable
    public String getUuid() {
        return this.uuid;
    }

    // DATABASE MAPPING : changed_at ( INTEGER(8) ) 
    /**
     * Set changedAt.
     * 
     * @param changedAt
     */    
    public void setChangedAt(@Nullable Integer changedAt) {
        this.changedAt = changedAt;
    }

    /**
     * Get changedAt.
     * 
     * @return changedAt
     */
    @Nullable
    public Integer getChangedAt() {
        return this.changedAt;
    }

}
