package aka.plexdb.bean;

import java.io.Serializable;

import java.util.Date;
import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import org.eclipse.jdt.annotation.Nullable;

/**
 * Persistent class for entity stored in table "media_items".
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class AbstractMediaItemsEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Nullable
    @Column(name = "\"id\"")
    private Integer id;

    @Nullable
    @Column(name = "\"type_id\"")
    private Integer typeId;

    @Nullable
    @Column(name = "\"width\"")
    private Integer width;

    @Nullable
    @Column(name = "\"height\"")
    private Integer height;

    @Nullable
    @Column(name = "\"size\"")
    private Integer size;

    @Nullable
    @Column(name = "\"duration\"")
    private Integer duration;

    @Nullable
    @Column(name = "\"bitrate\"")
    private Integer bitrate;

    @Nullable
    @Column(name = "\"container\"")
    private String container;

    @Nullable
    @Column(name = "\"video_codec\"")
    private String videoCodec;

    @Nullable
    @Column(name = "\"audio_codec\"")
    private String audioCodec;

    @Nullable
    @Column(name = "\"display_aspect_ratio\"")
    private Float displayAspectRatio;

    @Nullable
    @Column(name = "\"frames_per_second\"")
    private Float framesPerSecond;

    @Nullable
    @Column(name = "\"audio_channels\"")
    private Integer audioChannels;

    @Nullable
    @Column(name = "\"interlaced\"")
    private Boolean interlaced;

    @Nullable
    @Column(name = "\"source\"")
    private String source;

    @Nullable
    @Column(name = "\"hints\"")
    private String hints;

    @Nullable
    @Column(name = "\"display_offset\"")
    private Integer displayOffset;

    @Nullable
    @Column(name = "\"settings\"")
    private String settings;

    @Nullable
    @Column(name = "\"created_at\"")
    private Date createdAt;

    @Nullable
    @Column(name = "\"updated_at\"")
    private Date updatedAt;

    @Nullable
    @Column(name = "\"optimized_for_streaming\"")
    private Boolean optimizedForStreaming;

    @Nullable
    @Column(name = "\"deleted_at\"")
    private Date deletedAt;

    @Nullable
    @Column(name = "\"media_analysis_version\"")
    private Integer mediaAnalysisVersion;

    @Nullable
    @Column(name = "\"sample_aspect_ratio\"")
    private Float sampleAspectRatio;

    @Nullable
    @Column(name = "\"extra_data\"")
    private String extraData;

    @Nullable
    @Column(name = "\"proxy_type\"")
    private Integer proxyType;

    /**
     * Set id.
     * 
     * @param id
     */  
    public void setId(@Nullable Integer id) {
        this.id = id;
    }

    /**
     * Get id.
     * 
     * @return id
     */
    @Nullable
    public Integer getId() {
        return this.id;
    }




    // DATABASE MAPPING : type_id ( INTEGER ) 
    /**
     * Set typeId.
     * 
     * @param typeId
     */    
    public void setTypeId(@Nullable Integer typeId) {
        this.typeId = typeId;
    }

    /**
     * Get typeId.
     * 
     * @return typeId
     */
    @Nullable
    public Integer getTypeId() {
        return this.typeId;
    }

    // DATABASE MAPPING : width ( INTEGER ) 
    /**
     * Set width.
     * 
     * @param width
     */    
    public void setWidth(@Nullable Integer width) {
        this.width = width;
    }

    /**
     * Get width.
     * 
     * @return width
     */
    @Nullable
    public Integer getWidth() {
        return this.width;
    }

    // DATABASE MAPPING : height ( INTEGER ) 
    /**
     * Set height.
     * 
     * @param height
     */    
    public void setHeight(@Nullable Integer height) {
        this.height = height;
    }

    /**
     * Get height.
     * 
     * @return height
     */
    @Nullable
    public Integer getHeight() {
        return this.height;
    }

    // DATABASE MAPPING : size ( INTEGER(8) ) 
    /**
     * Set size.
     * 
     * @param size
     */    
    public void setSize(@Nullable Integer size) {
        this.size = size;
    }

    /**
     * Get size.
     * 
     * @return size
     */
    @Nullable
    public Integer getSize() {
        return this.size;
    }

    // DATABASE MAPPING : duration ( INTEGER ) 
    /**
     * Set duration.
     * 
     * @param duration
     */    
    public void setDuration(@Nullable Integer duration) {
        this.duration = duration;
    }

    /**
     * Get duration.
     * 
     * @return duration
     */
    @Nullable
    public Integer getDuration() {
        return this.duration;
    }

    // DATABASE MAPPING : bitrate ( INTEGER ) 
    /**
     * Set bitrate.
     * 
     * @param bitrate
     */    
    public void setBitrate(@Nullable Integer bitrate) {
        this.bitrate = bitrate;
    }

    /**
     * Get bitrate.
     * 
     * @return bitrate
     */
    @Nullable
    public Integer getBitrate() {
        return this.bitrate;
    }

    // DATABASE MAPPING : container ( VARCHAR(255) ) 
    /**
     * Set container.
     * 
     * @param container
     */    
    public void setContainer(@Nullable String container) {
        this.container = container;
    }

    /**
     * Get container.
     * 
     * @return container
     */
    @Nullable
    public String getContainer() {
        return this.container;
    }

    // DATABASE MAPPING : video_codec ( VARCHAR(255) ) 
    /**
     * Set videoCodec.
     * 
     * @param videoCodec
     */    
    public void setVideoCodec(@Nullable String videoCodec) {
        this.videoCodec = videoCodec;
    }

    /**
     * Get videoCodec.
     * 
     * @return videoCodec
     */
    @Nullable
    public String getVideoCodec() {
        return this.videoCodec;
    }

    // DATABASE MAPPING : audio_codec ( VARCHAR(255) ) 
    /**
     * Set audioCodec.
     * 
     * @param audioCodec
     */    
    public void setAudioCodec(@Nullable String audioCodec) {
        this.audioCodec = audioCodec;
    }

    /**
     * Get audioCodec.
     * 
     * @return audioCodec
     */
    @Nullable
    public String getAudioCodec() {
        return this.audioCodec;
    }

    // DATABASE MAPPING : display_aspect_ratio ( FLOAT ) 
    /**
     * Set displayAspectRatio.
     * 
     * @param displayAspectRatio
     */    
    public void setDisplayAspectRatio(@Nullable Float displayAspectRatio) {
        this.displayAspectRatio = displayAspectRatio;
    }

    /**
     * Get displayAspectRatio.
     * 
     * @return displayAspectRatio
     */
    @Nullable
    public Float getDisplayAspectRatio() {
        return this.displayAspectRatio;
    }

    // DATABASE MAPPING : frames_per_second ( FLOAT ) 
    /**
     * Set framesPerSecond.
     * 
     * @param framesPerSecond
     */    
    public void setFramesPerSecond(@Nullable Float framesPerSecond) {
        this.framesPerSecond = framesPerSecond;
    }

    /**
     * Get framesPerSecond.
     * 
     * @return framesPerSecond
     */
    @Nullable
    public Float getFramesPerSecond() {
        return this.framesPerSecond;
    }

    // DATABASE MAPPING : audio_channels ( INTEGER ) 
    /**
     * Set audioChannels.
     * 
     * @param audioChannels
     */    
    public void setAudioChannels(@Nullable Integer audioChannels) {
        this.audioChannels = audioChannels;
    }

    /**
     * Get audioChannels.
     * 
     * @return audioChannels
     */
    @Nullable
    public Integer getAudioChannels() {
        return this.audioChannels;
    }

    // DATABASE MAPPING : interlaced ( BOOLEAN ) 
    /**
     * Set interlaced.
     * 
     * @param interlaced
     */    
    public void setInterlaced(@Nullable Boolean interlaced) {
        this.interlaced = interlaced;
    }

    /**
     * Get interlaced.
     * 
     * @return interlaced
     */
    @Nullable
    public Boolean getInterlaced() {
        return this.interlaced;
    }

    // DATABASE MAPPING : source ( VARCHAR(255) ) 
    /**
     * Set source.
     * 
     * @param source
     */    
    public void setSource(@Nullable String source) {
        this.source = source;
    }

    /**
     * Get source.
     * 
     * @return source
     */
    @Nullable
    public String getSource() {
        return this.source;
    }

    // DATABASE MAPPING : hints ( VARCHAR(255) ) 
    /**
     * Set hints.
     * 
     * @param hints
     */    
    public void setHints(@Nullable String hints) {
        this.hints = hints;
    }

    /**
     * Get hints.
     * 
     * @return hints
     */
    @Nullable
    public String getHints() {
        return this.hints;
    }

    // DATABASE MAPPING : display_offset ( INTEGER ) 
    /**
     * Set displayOffset.
     * 
     * @param displayOffset
     */    
    public void setDisplayOffset(@Nullable Integer displayOffset) {
        this.displayOffset = displayOffset;
    }

    /**
     * Get displayOffset.
     * 
     * @return displayOffset
     */
    @Nullable
    public Integer getDisplayOffset() {
        return this.displayOffset;
    }

    // DATABASE MAPPING : settings ( VARCHAR(255) ) 
    /**
     * Set settings.
     * 
     * @param settings
     */    
    public void setSettings(@Nullable String settings) {
        this.settings = settings;
    }

    /**
     * Get settings.
     * 
     * @return settings
     */
    @Nullable
    public String getSettings() {
        return this.settings;
    }

    // DATABASE MAPPING : created_at ( DATETIME ) 
    /**
     * Set createdAt.
     * 
     * @param createdAt
     */    
    public void setCreatedAt(@Nullable Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * Get createdAt.
     * 
     * @return createdAt
     */
    @Nullable
    public Date getCreatedAt() {
        return this.createdAt;
    }

    // DATABASE MAPPING : updated_at ( DATETIME ) 
    /**
     * Set updatedAt.
     * 
     * @param updatedAt
     */    
    public void setUpdatedAt(@Nullable Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * Get updatedAt.
     * 
     * @return updatedAt
     */
    @Nullable
    public Date getUpdatedAt() {
        return this.updatedAt;
    }

    // DATABASE MAPPING : optimized_for_streaming ( BOOLEAN ) 
    /**
     * Set optimizedForStreaming.
     * 
     * @param optimizedForStreaming
     */    
    public void setOptimizedForStreaming(@Nullable Boolean optimizedForStreaming) {
        this.optimizedForStreaming = optimizedForStreaming;
    }

    /**
     * Get optimizedForStreaming.
     * 
     * @return optimizedForStreaming
     */
    @Nullable
    public Boolean getOptimizedForStreaming() {
        return this.optimizedForStreaming;
    }

    // DATABASE MAPPING : deleted_at ( DATETIME ) 
    /**
     * Set deletedAt.
     * 
     * @param deletedAt
     */    
    public void setDeletedAt(@Nullable Date deletedAt) {
        this.deletedAt = deletedAt;
    }

    /**
     * Get deletedAt.
     * 
     * @return deletedAt
     */
    @Nullable
    public Date getDeletedAt() {
        return this.deletedAt;
    }

    // DATABASE MAPPING : media_analysis_version ( INTEGER ) 
    /**
     * Set mediaAnalysisVersion.
     * 
     * @param mediaAnalysisVersion
     */    
    public void setMediaAnalysisVersion(@Nullable Integer mediaAnalysisVersion) {
        this.mediaAnalysisVersion = mediaAnalysisVersion;
    }

    /**
     * Get mediaAnalysisVersion.
     * 
     * @return mediaAnalysisVersion
     */
    @Nullable
    public Integer getMediaAnalysisVersion() {
        return this.mediaAnalysisVersion;
    }

    // DATABASE MAPPING : sample_aspect_ratio ( FLOAT ) 
    /**
     * Set sampleAspectRatio.
     * 
     * @param sampleAspectRatio
     */    
    public void setSampleAspectRatio(@Nullable Float sampleAspectRatio) {
        this.sampleAspectRatio = sampleAspectRatio;
    }

    /**
     * Get sampleAspectRatio.
     * 
     * @return sampleAspectRatio
     */
    @Nullable
    public Float getSampleAspectRatio() {
        return this.sampleAspectRatio;
    }

    // DATABASE MAPPING : extra_data ( VARCHAR(255) ) 
    /**
     * Set extraData.
     * 
     * @param extraData
     */    
    public void setExtraData(@Nullable String extraData) {
        this.extraData = extraData;
    }

    /**
     * Get extraData.
     * 
     * @return extraData
     */
    @Nullable
    public String getExtraData() {
        return this.extraData;
    }

    // DATABASE MAPPING : proxy_type ( INTEGER ) 
    /**
     * Set proxyType.
     * 
     * @param proxyType
     */    
    public void setProxyType(@Nullable Integer proxyType) {
        this.proxyType = proxyType;
    }

    /**
     * Get proxyType.
     * 
     * @return proxyType
     */
    @Nullable
    public Integer getProxyType() {
        return this.proxyType;
    }

}
