package aka.plexdb.bean;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;

import aka.plexdb.bean.guid.GuID;

/**
 * Persistent class for entity stored in table "metadata_item_views".
 *
 * This file can be filled with association or specific method.
 * REGENERATION WILL ERASED YOUR CHANGES.
 */
@Entity
@Table(name = "\"metadata_item_views\"")
public class MetadataItemViewsEntity extends AbstractMetadataItemViewsEntity {

    private static final long serialVersionUID = 1L;

    // bi-directional many-to-one association to LibrarySectionsEntity
    @ManyToOne(targetEntity = LibrarySectionsEntity.class, optional = true)
    @JoinColumn(name = "\"library_section_id\"", referencedColumnName = "\"id\"")
    @Nullable
    private LibrarySectionsEntity librarySection;

    /**
     * Get linked LibrarySectionsEntity.
     *
     * @return linked LibrarySectionsEntity
     * @see LibrarySectionsEntity
     */
    @Nullable
    public LibrarySectionsEntity getLibrarySection() {
        return this.librarySection;
    }

    /**
     * Set linked LibrarySectionsEntity.
     *
     * @param librarySection linked LibrarySectionsEntity
     * @see LibrarySectionsEntity
     */
    public void setLibrarySection(@Nullable final LibrarySectionsEntity librarySection) {
        this.librarySection = librarySection;
    }

    /**
     * Get GuId.
     *
     * @return GuId
     * @see GuID
     */
    @NonNull
    public GuID getGuId() {
        GuID result = new GuID("");

        final String guidValue = getGuid();
        if (guidValue != null) {
            result = new GuID(guidValue);
        }

        return result;
    }
}
