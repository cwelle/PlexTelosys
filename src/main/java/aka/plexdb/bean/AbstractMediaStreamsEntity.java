package aka.plexdb.bean;

import java.io.Serializable;

import java.util.Date;
import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import org.eclipse.jdt.annotation.Nullable;

/**
 * Persistent class for entity stored in table "media_streams".
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class AbstractMediaStreamsEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Nullable
    @Column(name = "\"id\"")
    private Integer id;

    @Nullable
    @Column(name = "\"url\"")
    private String url;

    @Nullable
    @Column(name = "\"codec\"")
    private String codec;

    @Nullable
    @Column(name = "\"language\"")
    private String language;

    @Nullable
    @Column(name = "\"created_at\"")
    private Date createdAt;

    @Nullable
    @Column(name = "\"updated_at\"")
    private Date updatedAt;

    @Nullable
    @Column(name = "\"channels\"")
    private Integer channels;

    @Nullable
    @Column(name = "\"bitrate\"")
    private Integer bitrate;

    @Nullable
    @Column(name = "\"url_index\"")
    private Integer urlIndex;

    @Nullable
    @Column(name = "\"default\"")
    private Boolean isDefault;

    @Nullable
    @Column(name = "\"forced\"")
    private Boolean isForced;

    @Nullable
    @Column(name = "\"extra_data\"")
    private String extraData;

    /**
     * Set id.
     * 
     * @param id
     */  
    public void setId(@Nullable Integer id) {
        this.id = id;
    }

    /**
     * Get id.
     * 
     * @return id
     */
    @Nullable
    public Integer getId() {
        return this.id;
    }



    // DATABASE MAPPING : url ( VARCHAR(255) ) 
    /**
     * Set url.
     * 
     * @param url
     */    
    public void setUrl(@Nullable String url) {
        this.url = url;
    }

    /**
     * Get url.
     * 
     * @return url
     */
    @Nullable
    public String getUrl() {
        return this.url;
    }

    // DATABASE MAPPING : codec ( VARCHAR(255) ) 
    /**
     * Set codec.
     * 
     * @param codec
     */    
    public void setCodec(@Nullable String codec) {
        this.codec = codec;
    }

    /**
     * Get codec.
     * 
     * @return codec
     */
    @Nullable
    public String getCodec() {
        return this.codec;
    }

    // DATABASE MAPPING : language ( VARCHAR(255) ) 
    /**
     * Set language.
     * 
     * @param language
     */    
    public void setLanguage(@Nullable String language) {
        this.language = language;
    }

    /**
     * Get language.
     * 
     * @return language
     */
    @Nullable
    public String getLanguage() {
        return this.language;
    }

    // DATABASE MAPPING : created_at ( DATETIME ) 
    /**
     * Set createdAt.
     * 
     * @param createdAt
     */    
    public void setCreatedAt(@Nullable Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * Get createdAt.
     * 
     * @return createdAt
     */
    @Nullable
    public Date getCreatedAt() {
        return this.createdAt;
    }

    // DATABASE MAPPING : updated_at ( DATETIME ) 
    /**
     * Set updatedAt.
     * 
     * @param updatedAt
     */    
    public void setUpdatedAt(@Nullable Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * Get updatedAt.
     * 
     * @return updatedAt
     */
    @Nullable
    public Date getUpdatedAt() {
        return this.updatedAt;
    }



    // DATABASE MAPPING : channels ( INTEGER ) 
    /**
     * Set channels.
     * 
     * @param channels
     */    
    public void setChannels(@Nullable Integer channels) {
        this.channels = channels;
    }

    /**
     * Get channels.
     * 
     * @return channels
     */
    @Nullable
    public Integer getChannels() {
        return this.channels;
    }

    // DATABASE MAPPING : bitrate ( INTEGER ) 
    /**
     * Set bitrate.
     * 
     * @param bitrate
     */    
    public void setBitrate(@Nullable Integer bitrate) {
        this.bitrate = bitrate;
    }

    /**
     * Get bitrate.
     * 
     * @return bitrate
     */
    @Nullable
    public Integer getBitrate() {
        return this.bitrate;
    }

    // DATABASE MAPPING : url_index ( INTEGER ) 
    /**
     * Set urlIndex.
     * 
     * @param urlIndex
     */    
    public void setUrlIndex(@Nullable Integer urlIndex) {
        this.urlIndex = urlIndex;
    }

    /**
     * Get urlIndex.
     * 
     * @return urlIndex
     */
    @Nullable
    public Integer getUrlIndex() {
        return this.urlIndex;
    }

    // DATABASE MAPPING : default ( BOOLEAN ) 
    /**
     * Set isDefault.
     * 
     * @param isDefault
     */    
    public void setIsDefault(@Nullable Boolean isDefault) {
        this.isDefault = isDefault;
    }

    /**
     * Get isDefault.
     * 
     * @return isDefault
     */
    @Nullable
    public Boolean getIsDefault() {
        return this.isDefault;
    }

    // DATABASE MAPPING : forced ( BOOLEAN ) 
    /**
     * Set isForced.
     * 
     * @param isForced
     */    
    public void setIsForced(@Nullable Boolean isForced) {
        this.isForced = isForced;
    }

    /**
     * Get isForced.
     * 
     * @return isForced
     */
    @Nullable
    public Boolean getIsForced() {
        return this.isForced;
    }

    // DATABASE MAPPING : extra_data ( VARCHAR(255) ) 
    /**
     * Set extraData.
     * 
     * @param extraData
     */    
    public void setExtraData(@Nullable String extraData) {
        this.extraData = extraData;
    }

    /**
     * Get extraData.
     * 
     * @return extraData
     */
    @Nullable
    public String getExtraData() {
        return this.extraData;
    }

}
