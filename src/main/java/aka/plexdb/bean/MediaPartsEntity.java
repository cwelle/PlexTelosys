package aka.plexdb.bean;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;

/**
 * Persistent class for entity stored in table "media_parts".
 *
 * This file can be filled with association or specific method.
 * REGENERATION WILL ERASED YOUR CHANGES.
 */
@Entity
@Table(name = "\"media_parts\"")
public class MediaPartsEntity extends AbstractMediaPartsEntity {

    private static final long serialVersionUID = 1L;

    // bi-directional many-to-one association to MediaItemsEntity
    @ManyToOne(targetEntity = MediaItemsEntity.class, optional = false)
    @JoinColumn(name = "\"media_item_id\"", referencedColumnName = "\"id\"")
    @Nullable
    private MediaItemsEntity mediaItem;

    // bi-directional many-to-one association to DirectoriesEntity
    @ManyToOne(targetEntity = DirectoriesEntity.class, optional = false)
    @JoinColumn(name = "\"directory_id\"", referencedColumnName = "\"id\"")
    @Nullable
    private DirectoriesEntity directories;

    // bi-directional one-to-many association to MediaStreamsEntity
    @OneToMany(mappedBy = "mediaPart")
    @NonNull
    private List<@NonNull MediaStreamsEntity> mediaStreams = new ArrayList<>();

    /**
     * Get linked MediaItemsEntity.
     *
     * @return linked MediaItemsEntity
     * @see MediaItemsEntity
     */
    @Nullable
    public MediaItemsEntity getMediaItem() {
        return this.mediaItem;
    }

    /**
     * Set linked MediaItemsEntity.
     *
     * @param mediaItem
     * @see MediaPartsEntity
     */
    public void setMediaItem(@Nullable final MediaItemsEntity mediaItem) {
        this.mediaItem = mediaItem;
    }

    /**
     * Get linked DirectoriesEntity.
     *
     * @return linked DirectoriesEntity
     * @see DirectoriesEntity
     */
    @Nullable
    public DirectoriesEntity getDirectory() {
        return this.directories;
    }

    /**
     * Set linked DirectoriesEntity.
     *
     * @param directoriesEntity
     * @see MediaPartsEntity
     */
    public void setDirectory(@Nullable final DirectoriesEntity directoriesEntity) {
        this.directories = directoriesEntity;
    }

    /**
     * Get list of linked MediaStreamsEntity.
     *
     * @return list of linked MediaStreamsEntity
     * @see MediaStreamsEntity
     */
    @NonNull
    public List<@NonNull MediaStreamsEntity> getMediaStreams() {
        return this.mediaStreams;
    }

    /**
     * Set list of MediaStreamsEntity.
     *
     * @param mediaStreams
     * @see MediaStreamsEntity
     */
    public void setMediaStreams(@NonNull final List<@NonNull MediaStreamsEntity> mediaStreams) {
        this.mediaStreams = mediaStreams;
    }

    /**
     * Add MediaStreamsEntity.
     *
     * @param mediaStreamsEntity
     * @return added media stream with media part set.
     * @see MediaStreamsEntity
     */
    @NonNull
    public MediaStreamsEntity addMediaStream(@NonNull final MediaStreamsEntity mediaStreamsEntity) {
        getMediaStreams().add(mediaStreamsEntity);
        mediaStreamsEntity.setMediaPart(this);

        return mediaStreamsEntity;
    }

    /**
     * Remove MediaStreamsEntity.
     *
     * @param mediaStreamsEntity
     * @return removed media stream with media part unset.
     * @see MediaStreamsEntity
     */
    @NonNull
    public MediaStreamsEntity removeMediaStream(@NonNull final MediaStreamsEntity mediaStreamsEntity) {
        getMediaStreams().remove(mediaStreamsEntity);
        mediaStreamsEntity.setMediaPart(null);

        return mediaStreamsEntity;
    }
}
