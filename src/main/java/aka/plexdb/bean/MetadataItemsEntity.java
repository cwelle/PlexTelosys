package aka.plexdb.bean;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;

import aka.plexdb.bean.guid.GuID;

/**
 * Persistent class for entity stored in table "metadata_items".
 *
 * This file can be filled with association or specific method.
 * REGENERATION WILL ERASED YOUR CHANGES.
 */
@Entity
@Table(name = "\"metadata_items\"")
public class MetadataItemsEntity extends AbstractMetadataItemsEntity {

    private static final long serialVersionUID = 1L;

    // bi-directional one-to-many association to TagsEntity
    @OneToMany(mappedBy = "metadataItem")
    @NonNull
    private List<@NonNull TagsEntity> tags = new ArrayList<>();

    // bi-directional one-to-many association to MediaItemsEntity
    @OneToMany(mappedBy = "metadataItem")
    @NonNull
    private List<@NonNull MediaItemsEntity> mediaItems = new ArrayList<>();

    // bi-directional one-to-many association to TaggingsEntity
    @OneToMany(mappedBy = "metadataItem")
    @NonNull
    private List<@NonNull TaggingsEntity> taggins = new ArrayList<>();

    // bi-directional many-to-one association to LibrarySectionsEntity
    @ManyToOne(targetEntity = LibrarySectionsEntity.class, optional = true)
    @JoinColumn(name = "\"library_section_id\"", referencedColumnName = "\"id\"")
    @Nullable
    private LibrarySectionsEntity librarySection;

    /**
     * Get list of linked TagsEntity.
     *
     * @return list of linked TagsEntity
     * @see TagsEntity
     */
    @NonNull
    public List<@NonNull TagsEntity> getTags() {
        return this.tags;
    }

    /**
     * Set list of TagsEntity.
     *
     * @param tags
     * @see TagsEntity
     */
    public void setTags(@NonNull final List<@NonNull TagsEntity> tags) {
        this.tags = tags;
    }

    /**
     * Add TagsEntity.
     *
     * @param tag
     * @return added tags with meta data item set.
     * @see TagsEntity
     */
    @NonNull
    public TagsEntity addTags(@NonNull final TagsEntity tag) {
        getTags().add(tag);
        tag.setMetadataItem(this);

        return tag;
    }

    /**
     * Remove TagsEntity.
     *
     * @param tag
     * @return removed tags with meta data item unset.
     * @see TagsEntity
     */
    @NonNull
    public TagsEntity removeTags(@NonNull final TagsEntity tag) {
        getTags().remove(tag);
        tag.setMetadataItem(null);

        return tag;
    }

    /**
     * Get list of MediaItemsEntity.
     *
     * @return list of linked MediaItemsEntity
     * @see MediaItemsEntity
     */
    @NonNull
    public List<@NonNull MediaItemsEntity> getMediaItems() {
        return this.mediaItems;
    }

    /**
     * Set list of MediaItemsEntity.
     *
     * @param mediaItems list of MediaItemsEntity.
     * @see MediaItemsEntity
     */
    public void setMediaItems(@NonNull final List<@NonNull MediaItemsEntity> mediaItems) {
        this.mediaItems = mediaItems;
    }

    /**
     * Add MediaItemsEntity.
     *
     * @param mediaItemsEntity
     * @return added media item with meta data item set.
     * @see MediaItemsEntity
     */
    @NonNull
    public MediaItemsEntity addMediaItems(@NonNull final MediaItemsEntity mediaItemsEntity) {
        getMediaItems().add(mediaItemsEntity);
        mediaItemsEntity.setMetadataItem(this);

        return mediaItemsEntity;
    }

    /**
     * Remove MediaItemsEntity.
     *
     * @param mediaItemsEntity
     * @return removed media item with meta data item unset.
     * @see MediaItemsEntity
     */
    @NonNull
    public MediaItemsEntity removeMediaItems(@NonNull final MediaItemsEntity mediaItemsEntity) {
        getMediaItems().remove(mediaItemsEntity);
        mediaItemsEntity.setMetadataItem(null);

        return mediaItemsEntity;
    }

    /**
     * Get list of linked TaggingsEntity.
     *
     * @return list of linked TaggingsEntity
     * @see TaggingsEntity
     */
    @NonNull
    public List<@NonNull TaggingsEntity> getTaggings() {
        return this.taggins;
    }

    /**
     * Set list of TaggingsEntity.
     *
     * @param taggins
     * @see TaggingsEntity
     */
    public void setTaggings(@NonNull final List<@NonNull TaggingsEntity> taggins) {
        this.taggins = taggins;
    }

    /**
     * Add TaggingsEntity.
     *
     * @param tagging
     * @return added tagging with meta data item set.
     * @see TaggingsEntity
     */
    @NonNull
    public TaggingsEntity addTagging(@NonNull final TaggingsEntity tagging) {
        getTaggings().add(tagging);
        tagging.setMetadataItem(this);

        return tagging;
    }

    /**
     * Remove TaggingsEntity.
     *
     * @param tagging
     * @return removed tagging with meta data item unset.
     * @see TaggingsEntity
     */
    @NonNull
    public TaggingsEntity removeMediaPart(final TaggingsEntity tagging) {
        getTaggings().remove(tagging);
        tagging.setMetadataItem(null);

        return tagging;
    }

    /**
     * Get LibrarySectionsEntity.
     *
     * @return linked LibrarySectionsEntity
     * @see LibrarySectionsEntity
     */
    @Nullable
    public LibrarySectionsEntity getLibrarySection() {
        return this.librarySection;
    }

    /**
     * Set LibrarySectionsEntity.
     *
     * @param librarySection
     * @see LibrarySectionsEntity
     */
    public void setLibrarySection(@Nullable final LibrarySectionsEntity librarySection) {
        this.librarySection = librarySection;
    }

    /**
     * Get GuId.
     *
     * @return GuId
     * @see GuID
     */
    @NonNull
    public GuID getGuId() {
        GuID result = new GuID("");

        final String guidValue = getGuid();
        if (guidValue != null) {
            result = new GuID(guidValue);
        }

        return result;
    }
}
