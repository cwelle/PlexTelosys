package aka.plexdb.bean;

import java.io.Serializable;

import java.util.Date;
import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import org.eclipse.jdt.annotation.Nullable;

/**
 * Persistent class for entity stored in table "taggings".
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class AbstractTaggingsEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Nullable
    @Column(name = "\"id\"")
    private Integer id;

    @Nullable
    @Column(name = "\"text\"")
    private String text;

    @Nullable
    @Column(name = "\"time_offset\"")
    private Integer timeOffset;

    @Nullable
    @Column(name = "\"end_time_offset\"")
    private Integer endTimeOffset;

    @Nullable
    @Column(name = "\"thumb_url\"")
    private String thumbUrl;

    @Nullable
    @Column(name = "\"created_at\"")
    private Date createdAt;

    @Nullable
    @Column(name = "\"extra_data\"")
    private String extraData;

    /**
     * Set id.
     * 
     * @param id
     */  
    public void setId(@Nullable Integer id) {
        this.id = id;
    }

    /**
     * Get id.
     * 
     * @return id
     */
    @Nullable
    public Integer getId() {
        return this.id;
    }




    // DATABASE MAPPING : text ( VARCHAR(255) ) 
    /**
     * Set text.
     * 
     * @param text
     */    
    public void setText(@Nullable String text) {
        this.text = text;
    }

    /**
     * Get text.
     * 
     * @return text
     */
    @Nullable
    public String getText() {
        return this.text;
    }

    // DATABASE MAPPING : time_offset ( INTEGER ) 
    /**
     * Set timeOffset.
     * 
     * @param timeOffset
     */    
    public void setTimeOffset(@Nullable Integer timeOffset) {
        this.timeOffset = timeOffset;
    }

    /**
     * Get timeOffset.
     * 
     * @return timeOffset
     */
    @Nullable
    public Integer getTimeOffset() {
        return this.timeOffset;
    }

    // DATABASE MAPPING : end_time_offset ( INTEGER ) 
    /**
     * Set endTimeOffset.
     * 
     * @param endTimeOffset
     */    
    public void setEndTimeOffset(@Nullable Integer endTimeOffset) {
        this.endTimeOffset = endTimeOffset;
    }

    /**
     * Get endTimeOffset.
     * 
     * @return endTimeOffset
     */
    @Nullable
    public Integer getEndTimeOffset() {
        return this.endTimeOffset;
    }

    // DATABASE MAPPING : thumb_url ( VARCHAR(255) ) 
    /**
     * Set thumbUrl.
     * 
     * @param thumbUrl
     */    
    public void setThumbUrl(@Nullable String thumbUrl) {
        this.thumbUrl = thumbUrl;
    }

    /**
     * Get thumbUrl.
     * 
     * @return thumbUrl
     */
    @Nullable
    public String getThumbUrl() {
        return this.thumbUrl;
    }

    // DATABASE MAPPING : created_at ( DATETIME ) 
    /**
     * Set createdAt.
     * 
     * @param createdAt
     */    
    public void setCreatedAt(@Nullable Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * Get createdAt.
     * 
     * @return createdAt
     */
    @Nullable
    public Date getCreatedAt() {
        return this.createdAt;
    }

    // DATABASE MAPPING : extra_data ( VARCHAR(255) ) 
    /**
     * Set extraData.
     * 
     * @param extraData
     */    
    public void setExtraData(@Nullable String extraData) {
        this.extraData = extraData;
    }

    /**
     * Get extraData.
     * 
     * @return extraData
     */
    @Nullable
    public String getExtraData() {
        return this.extraData;
    }

}
