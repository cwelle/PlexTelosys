package aka.plexdb.bean;

import java.io.Serializable;

import java.util.Date;
import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import org.eclipse.jdt.annotation.Nullable;

/**
 * Persistent class for entity stored in table "metadata_items".
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class AbstractMetadataItemsEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Nullable
    @Column(name = "\"id\"")
    private Integer id;

    @Nullable
    @Column(name = "\"metadata_type\"")
    private Integer metadataType;

    @Nullable
    @Column(name = "\"guid\"")
    private String guid;

    @Nullable
    @Column(name = "\"media_item_count\"")
    private Integer mediaItemCount;

    @Nullable
    @Column(name = "\"title\"")
    private String title;

    @Nullable
    @Column(name = "\"title_sort\"")
    private String titleSort;

    @Nullable
    @Column(name = "\"original_title\"")
    private String originalTitle;

    @Nullable
    @Column(name = "\"studio\"")
    private String studio;

    @Nullable
    @Column(name = "\"rating\"")
    private Float rating;

    @Nullable
    @Column(name = "\"rating_count\"")
    private Integer ratingCount;

    @Nullable
    @Column(name = "\"tagline\"")
    private String tagline;

    @Nullable
    @Column(name = "\"summary\"")
    private String summary;

    @Nullable
    @Column(name = "\"trivia\"")
    private String trivia;

    @Nullable
    @Column(name = "\"quotes\"")
    private String quotes;

    @Nullable
    @Column(name = "\"content_rating\"")
    private String contentRating;

    @Nullable
    @Column(name = "\"content_rating_age\"")
    private Integer contentRatingAge;

    @Nullable
    @Column(name = "\"absolute_index\"")
    private Integer absoluteIndex;

    @Nullable
    @Column(name = "\"duration\"")
    private Integer duration;

    @Nullable
    @Column(name = "\"user_thumb_url\"")
    private String userThumbUrl;

    @Nullable
    @Column(name = "\"user_art_url\"")
    private String userArtUrl;

    @Nullable
    @Column(name = "\"user_banner_url\"")
    private String userBannerUrl;

    @Nullable
    @Column(name = "\"user_music_url\"")
    private String userMusicUrl;

    @Nullable
    @Column(name = "\"user_fields\"")
    private String userFields;

    @Nullable
    @Column(name = "\"tags_genre\"")
    private String tagsGenre;

    @Nullable
    @Column(name = "\"tags_collection\"")
    private String tagsCollection;

    @Nullable
    @Column(name = "\"tags_director\"")
    private String tagsDirector;

    @Nullable
    @Column(name = "\"tags_writer\"")
    private String tagsWriter;

    @Nullable
    @Column(name = "\"tags_star\"")
    private String tagsStar;

    @Nullable
    @Column(name = "\"originally_available_at\"")
    private Date originallyAvailableAt;

    @Nullable
    @Column(name = "\"available_at\"")
    private Date availableAt;

    @Nullable
    @Column(name = "\"expires_at\"")
    private Date expiresAt;

    @Nullable
    @Column(name = "\"refreshed_at\"")
    private Date refreshedAt;

    @Nullable
    @Column(name = "\"year\"")
    private Integer year;

    @Nullable
    @Column(name = "\"added_at\"")
    private Date addedAt;

    @Nullable
    @Column(name = "\"created_at\"")
    private Date createdAt;

    @Nullable
    @Column(name = "\"updated_at\"")
    private Date updatedAt;

    @Nullable
    @Column(name = "\"deleted_at\"")
    private Date deletedAt;

    @Nullable
    @Column(name = "\"tags_country\"")
    private String tagsCountry;

    @Nullable
    @Column(name = "\"extra_data\"")
    private String extraData;

    @Nullable
    @Column(name = "\"hash\"")
    private String hash;

    @Nullable
    @Column(name = "\"audience_rating\"")
    private Float audienceRating;

    @Nullable
    @Column(name = "\"changed_at\"")
    private Integer changedAt;

    @Nullable
    @Column(name = "\"resources_changed_at\"")
    private Integer resourcesChangedAt;

    /**
     * Set id.
     * 
     * @param id
     */  
    public void setId(@Nullable Integer id) {
        this.id = id;
    }

    /**
     * Get id.
     * 
     * @return id
     */
    @Nullable
    public Integer getId() {
        return this.id;
    }



    // DATABASE MAPPING : metadata_type ( INTEGER ) 
    /**
     * Set metadataType.
     * 
     * @param metadataType
     */    
    public void setMetadataType(@Nullable Integer metadataType) {
        this.metadataType = metadataType;
    }

    /**
     * Get metadataType.
     * 
     * @return metadataType
     */
    @Nullable
    public Integer getMetadataType() {
        return this.metadataType;
    }

    // DATABASE MAPPING : guid ( VARCHAR(255) ) 
    /**
     * Set guid.
     * 
     * @param guid
     */    
    public void setGuid(@Nullable String guid) {
        this.guid = guid;
    }

    /**
     * Get guid.
     * 
     * @return guid
     */
    @Nullable
    public String getGuid() {
        return this.guid;
    }

    // DATABASE MAPPING : media_item_count ( INTEGER ) 
    /**
     * Set mediaItemCount.
     * 
     * @param mediaItemCount
     */    
    public void setMediaItemCount(@Nullable Integer mediaItemCount) {
        this.mediaItemCount = mediaItemCount;
    }

    /**
     * Get mediaItemCount.
     * 
     * @return mediaItemCount
     */
    @Nullable
    public Integer getMediaItemCount() {
        return this.mediaItemCount;
    }

    // DATABASE MAPPING : title ( VARCHAR(255) ) 
    /**
     * Set title.
     * 
     * @param title
     */    
    public void setTitle(@Nullable String title) {
        this.title = title;
    }

    /**
     * Get title.
     * 
     * @return title
     */
    @Nullable
    public String getTitle() {
        return this.title;
    }

    // DATABASE MAPPING : title_sort ( VARCHAR(255) ) 
    /**
     * Set titleSort.
     * 
     * @param titleSort
     */    
    public void setTitleSort(@Nullable String titleSort) {
        this.titleSort = titleSort;
    }

    /**
     * Get titleSort.
     * 
     * @return titleSort
     */
    @Nullable
    public String getTitleSort() {
        return this.titleSort;
    }

    // DATABASE MAPPING : original_title ( VARCHAR(255) ) 
    /**
     * Set originalTitle.
     * 
     * @param originalTitle
     */    
    public void setOriginalTitle(@Nullable String originalTitle) {
        this.originalTitle = originalTitle;
    }

    /**
     * Get originalTitle.
     * 
     * @return originalTitle
     */
    @Nullable
    public String getOriginalTitle() {
        return this.originalTitle;
    }

    // DATABASE MAPPING : studio ( VARCHAR(255) ) 
    /**
     * Set studio.
     * 
     * @param studio
     */    
    public void setStudio(@Nullable String studio) {
        this.studio = studio;
    }

    /**
     * Get studio.
     * 
     * @return studio
     */
    @Nullable
    public String getStudio() {
        return this.studio;
    }

    // DATABASE MAPPING : rating ( FLOAT ) 
    /**
     * Set rating.
     * 
     * @param rating
     */    
    public void setRating(@Nullable Float rating) {
        this.rating = rating;
    }

    /**
     * Get rating.
     * 
     * @return rating
     */
    @Nullable
    public Float getRating() {
        return this.rating;
    }

    // DATABASE MAPPING : rating_count ( INTEGER ) 
    /**
     * Set ratingCount.
     * 
     * @param ratingCount
     */    
    public void setRatingCount(@Nullable Integer ratingCount) {
        this.ratingCount = ratingCount;
    }

    /**
     * Get ratingCount.
     * 
     * @return ratingCount
     */
    @Nullable
    public Integer getRatingCount() {
        return this.ratingCount;
    }

    // DATABASE MAPPING : tagline ( VARCHAR(255) ) 
    /**
     * Set tagline.
     * 
     * @param tagline
     */    
    public void setTagline(@Nullable String tagline) {
        this.tagline = tagline;
    }

    /**
     * Get tagline.
     * 
     * @return tagline
     */
    @Nullable
    public String getTagline() {
        return this.tagline;
    }

    // DATABASE MAPPING : summary ( TEXT ) 
    /**
     * Set summary.
     * 
     * @param summary
     */    
    public void setSummary(@Nullable String summary) {
        this.summary = summary;
    }

    /**
     * Get summary.
     * 
     * @return summary
     */
    @Nullable
    public String getSummary() {
        return this.summary;
    }

    // DATABASE MAPPING : trivia ( TEXT ) 
    /**
     * Set trivia.
     * 
     * @param trivia
     */    
    public void setTrivia(@Nullable String trivia) {
        this.trivia = trivia;
    }

    /**
     * Get trivia.
     * 
     * @return trivia
     */
    @Nullable
    public String getTrivia() {
        return this.trivia;
    }

    // DATABASE MAPPING : quotes ( TEXT ) 
    /**
     * Set quotes.
     * 
     * @param quotes
     */    
    public void setQuotes(@Nullable String quotes) {
        this.quotes = quotes;
    }

    /**
     * Get quotes.
     * 
     * @return quotes
     */
    @Nullable
    public String getQuotes() {
        return this.quotes;
    }

    // DATABASE MAPPING : content_rating ( VARCHAR(255) ) 
    /**
     * Set contentRating.
     * 
     * @param contentRating
     */    
    public void setContentRating(@Nullable String contentRating) {
        this.contentRating = contentRating;
    }

    /**
     * Get contentRating.
     * 
     * @return contentRating
     */
    @Nullable
    public String getContentRating() {
        return this.contentRating;
    }

    // DATABASE MAPPING : content_rating_age ( INTEGER ) 
    /**
     * Set contentRatingAge.
     * 
     * @param contentRatingAge
     */    
    public void setContentRatingAge(@Nullable Integer contentRatingAge) {
        this.contentRatingAge = contentRatingAge;
    }

    /**
     * Get contentRatingAge.
     * 
     * @return contentRatingAge
     */
    @Nullable
    public Integer getContentRatingAge() {
        return this.contentRatingAge;
    }


    // DATABASE MAPPING : absolute_index ( INTEGER ) 
    /**
     * Set absoluteIndex.
     * 
     * @param absoluteIndex
     */    
    public void setAbsoluteIndex(@Nullable Integer absoluteIndex) {
        this.absoluteIndex = absoluteIndex;
    }

    /**
     * Get absoluteIndex.
     * 
     * @return absoluteIndex
     */
    @Nullable
    public Integer getAbsoluteIndex() {
        return this.absoluteIndex;
    }

    // DATABASE MAPPING : duration ( INTEGER ) 
    /**
     * Set duration.
     * 
     * @param duration
     */    
    public void setDuration(@Nullable Integer duration) {
        this.duration = duration;
    }

    /**
     * Get duration.
     * 
     * @return duration
     */
    @Nullable
    public Integer getDuration() {
        return this.duration;
    }

    // DATABASE MAPPING : user_thumb_url ( VARCHAR(255) ) 
    /**
     * Set userThumbUrl.
     * 
     * @param userThumbUrl
     */    
    public void setUserThumbUrl(@Nullable String userThumbUrl) {
        this.userThumbUrl = userThumbUrl;
    }

    /**
     * Get userThumbUrl.
     * 
     * @return userThumbUrl
     */
    @Nullable
    public String getUserThumbUrl() {
        return this.userThumbUrl;
    }

    // DATABASE MAPPING : user_art_url ( VARCHAR(255) ) 
    /**
     * Set userArtUrl.
     * 
     * @param userArtUrl
     */    
    public void setUserArtUrl(@Nullable String userArtUrl) {
        this.userArtUrl = userArtUrl;
    }

    /**
     * Get userArtUrl.
     * 
     * @return userArtUrl
     */
    @Nullable
    public String getUserArtUrl() {
        return this.userArtUrl;
    }

    // DATABASE MAPPING : user_banner_url ( VARCHAR(255) ) 
    /**
     * Set userBannerUrl.
     * 
     * @param userBannerUrl
     */    
    public void setUserBannerUrl(@Nullable String userBannerUrl) {
        this.userBannerUrl = userBannerUrl;
    }

    /**
     * Get userBannerUrl.
     * 
     * @return userBannerUrl
     */
    @Nullable
    public String getUserBannerUrl() {
        return this.userBannerUrl;
    }

    // DATABASE MAPPING : user_music_url ( VARCHAR(255) ) 
    /**
     * Set userMusicUrl.
     * 
     * @param userMusicUrl
     */    
    public void setUserMusicUrl(@Nullable String userMusicUrl) {
        this.userMusicUrl = userMusicUrl;
    }

    /**
     * Get userMusicUrl.
     * 
     * @return userMusicUrl
     */
    @Nullable
    public String getUserMusicUrl() {
        return this.userMusicUrl;
    }

    // DATABASE MAPPING : user_fields ( VARCHAR(255) ) 
    /**
     * Set userFields.
     * 
     * @param userFields
     */    
    public void setUserFields(@Nullable String userFields) {
        this.userFields = userFields;
    }

    /**
     * Get userFields.
     * 
     * @return userFields
     */
    @Nullable
    public String getUserFields() {
        return this.userFields;
    }

    // DATABASE MAPPING : tags_genre ( VARCHAR(255) ) 
    /**
     * Set tagsGenre.
     * 
     * @param tagsGenre
     */    
    public void setTagsGenre(@Nullable String tagsGenre) {
        this.tagsGenre = tagsGenre;
    }

    /**
     * Get tagsGenre.
     * 
     * @return tagsGenre
     */
    @Nullable
    public String getTagsGenre() {
        return this.tagsGenre;
    }

    // DATABASE MAPPING : tags_collection ( VARCHAR(255) ) 
    /**
     * Set tagsCollection.
     * 
     * @param tagsCollection
     */    
    public void setTagsCollection(@Nullable String tagsCollection) {
        this.tagsCollection = tagsCollection;
    }

    /**
     * Get tagsCollection.
     * 
     * @return tagsCollection
     */
    @Nullable
    public String getTagsCollection() {
        return this.tagsCollection;
    }

    // DATABASE MAPPING : tags_director ( VARCHAR(255) ) 
    /**
     * Set tagsDirector.
     * 
     * @param tagsDirector
     */    
    public void setTagsDirector(@Nullable String tagsDirector) {
        this.tagsDirector = tagsDirector;
    }

    /**
     * Get tagsDirector.
     * 
     * @return tagsDirector
     */
    @Nullable
    public String getTagsDirector() {
        return this.tagsDirector;
    }

    // DATABASE MAPPING : tags_writer ( VARCHAR(255) ) 
    /**
     * Set tagsWriter.
     * 
     * @param tagsWriter
     */    
    public void setTagsWriter(@Nullable String tagsWriter) {
        this.tagsWriter = tagsWriter;
    }

    /**
     * Get tagsWriter.
     * 
     * @return tagsWriter
     */
    @Nullable
    public String getTagsWriter() {
        return this.tagsWriter;
    }

    // DATABASE MAPPING : tags_star ( VARCHAR(255) ) 
    /**
     * Set tagsStar.
     * 
     * @param tagsStar
     */    
    public void setTagsStar(@Nullable String tagsStar) {
        this.tagsStar = tagsStar;
    }

    /**
     * Get tagsStar.
     * 
     * @return tagsStar
     */
    @Nullable
    public String getTagsStar() {
        return this.tagsStar;
    }

    // DATABASE MAPPING : originally_available_at ( DATETIME ) 
    /**
     * Set originallyAvailableAt.
     * 
     * @param originallyAvailableAt
     */    
    public void setOriginallyAvailableAt(@Nullable Date originallyAvailableAt) {
        this.originallyAvailableAt = originallyAvailableAt;
    }

    /**
     * Get originallyAvailableAt.
     * 
     * @return originallyAvailableAt
     */
    @Nullable
    public Date getOriginallyAvailableAt() {
        return this.originallyAvailableAt;
    }

    // DATABASE MAPPING : available_at ( DATETIME ) 
    /**
     * Set availableAt.
     * 
     * @param availableAt
     */    
    public void setAvailableAt(@Nullable Date availableAt) {
        this.availableAt = availableAt;
    }

    /**
     * Get availableAt.
     * 
     * @return availableAt
     */
    @Nullable
    public Date getAvailableAt() {
        return this.availableAt;
    }

    // DATABASE MAPPING : expires_at ( DATETIME ) 
    /**
     * Set expiresAt.
     * 
     * @param expiresAt
     */    
    public void setExpiresAt(@Nullable Date expiresAt) {
        this.expiresAt = expiresAt;
    }

    /**
     * Get expiresAt.
     * 
     * @return expiresAt
     */
    @Nullable
    public Date getExpiresAt() {
        return this.expiresAt;
    }

    // DATABASE MAPPING : refreshed_at ( DATETIME ) 
    /**
     * Set refreshedAt.
     * 
     * @param refreshedAt
     */    
    public void setRefreshedAt(@Nullable Date refreshedAt) {
        this.refreshedAt = refreshedAt;
    }

    /**
     * Get refreshedAt.
     * 
     * @return refreshedAt
     */
    @Nullable
    public Date getRefreshedAt() {
        return this.refreshedAt;
    }

    // DATABASE MAPPING : year ( INTEGER ) 
    /**
     * Set year.
     * 
     * @param year
     */    
    public void setYear(@Nullable Integer year) {
        this.year = year;
    }

    /**
     * Get year.
     * 
     * @return year
     */
    @Nullable
    public Integer getYear() {
        return this.year;
    }

    // DATABASE MAPPING : added_at ( DATETIME ) 
    /**
     * Set addedAt.
     * 
     * @param addedAt
     */    
    public void setAddedAt(@Nullable Date addedAt) {
        this.addedAt = addedAt;
    }

    /**
     * Get addedAt.
     * 
     * @return addedAt
     */
    @Nullable
    public Date getAddedAt() {
        return this.addedAt;
    }

    // DATABASE MAPPING : created_at ( DATETIME ) 
    /**
     * Set createdAt.
     * 
     * @param createdAt
     */    
    public void setCreatedAt(@Nullable Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * Get createdAt.
     * 
     * @return createdAt
     */
    @Nullable
    public Date getCreatedAt() {
        return this.createdAt;
    }

    // DATABASE MAPPING : updated_at ( DATETIME ) 
    /**
     * Set updatedAt.
     * 
     * @param updatedAt
     */    
    public void setUpdatedAt(@Nullable Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * Get updatedAt.
     * 
     * @return updatedAt
     */
    @Nullable
    public Date getUpdatedAt() {
        return this.updatedAt;
    }

    // DATABASE MAPPING : deleted_at ( DATETIME ) 
    /**
     * Set deletedAt.
     * 
     * @param deletedAt
     */    
    public void setDeletedAt(@Nullable Date deletedAt) {
        this.deletedAt = deletedAt;
    }

    /**
     * Get deletedAt.
     * 
     * @return deletedAt
     */
    @Nullable
    public Date getDeletedAt() {
        return this.deletedAt;
    }

    // DATABASE MAPPING : tags_country ( VARCHAR(255) ) 
    /**
     * Set tagsCountry.
     * 
     * @param tagsCountry
     */    
    public void setTagsCountry(@Nullable String tagsCountry) {
        this.tagsCountry = tagsCountry;
    }

    /**
     * Get tagsCountry.
     * 
     * @return tagsCountry
     */
    @Nullable
    public String getTagsCountry() {
        return this.tagsCountry;
    }

    // DATABASE MAPPING : extra_data ( VARCHAR(255) ) 
    /**
     * Set extraData.
     * 
     * @param extraData
     */    
    public void setExtraData(@Nullable String extraData) {
        this.extraData = extraData;
    }

    /**
     * Get extraData.
     * 
     * @return extraData
     */
    @Nullable
    public String getExtraData() {
        return this.extraData;
    }

    // DATABASE MAPPING : hash ( VARCHAR(255) ) 
    /**
     * Set hash.
     * 
     * @param hash
     */    
    public void setHash(@Nullable String hash) {
        this.hash = hash;
    }

    /**
     * Get hash.
     * 
     * @return hash
     */
    @Nullable
    public String getHash() {
        return this.hash;
    }

    // DATABASE MAPPING : audience_rating ( FLOAT ) 
    /**
     * Set audienceRating.
     * 
     * @param audienceRating
     */    
    public void setAudienceRating(@Nullable Float audienceRating) {
        this.audienceRating = audienceRating;
    }

    /**
     * Get audienceRating.
     * 
     * @return audienceRating
     */
    @Nullable
    public Float getAudienceRating() {
        return this.audienceRating;
    }

    // DATABASE MAPPING : changed_at ( INTEGER(8) ) 
    /**
     * Set changedAt.
     * 
     * @param changedAt
     */    
    public void setChangedAt(@Nullable Integer changedAt) {
        this.changedAt = changedAt;
    }

    /**
     * Get changedAt.
     * 
     * @return changedAt
     */
    @Nullable
    public Integer getChangedAt() {
        return this.changedAt;
    }

    // DATABASE MAPPING : resources_changed_at ( INTEGER(8) ) 
    /**
     * Set resourcesChangedAt.
     * 
     * @param resourcesChangedAt
     */    
    public void setResourcesChangedAt(@Nullable Integer resourcesChangedAt) {
        this.resourcesChangedAt = resourcesChangedAt;
    }

    /**
     * Get resourcesChangedAt.
     * 
     * @return resourcesChangedAt
     */
    @Nullable
    public Integer getResourcesChangedAt() {
        return this.resourcesChangedAt;
    }

}
