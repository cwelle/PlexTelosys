package aka.plexdb.bean;

import java.io.Serializable;

import java.util.Date;
import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import org.eclipse.jdt.annotation.Nullable;

/**
 * Persistent class for entity stored in table "section_locations".
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class AbstractSectionLocationsEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Nullable
    @Column(name = "\"id\"")
    private Integer id;

    @Nullable
    @Column(name = "\"root_path\"")
    private String rootPath;

    @Nullable
    @Column(name = "\"available\"")
    private Boolean available;

    @Nullable
    @Column(name = "\"scanned_at\"")
    private Date scannedAt;

    @Nullable
    @Column(name = "\"created_at\"")
    private Date createdAt;

    @Nullable
    @Column(name = "\"updated_at\"")
    private Date updatedAt;

    /**
     * Set id.
     * 
     * @param id
     */  
    public void setId(@Nullable Integer id) {
        this.id = id;
    }

    /**
     * Get id.
     * 
     * @return id
     */
    @Nullable
    public Integer getId() {
        return this.id;
    }


    // DATABASE MAPPING : root_path ( VARCHAR(255) ) 
    /**
     * Set rootPath.
     * 
     * @param rootPath
     */    
    public void setRootPath(@Nullable String rootPath) {
        this.rootPath = rootPath;
    }

    /**
     * Get rootPath.
     * 
     * @return rootPath
     */
    @Nullable
    public String getRootPath() {
        return this.rootPath;
    }

    // DATABASE MAPPING : available ( BOOLEAN ) 
    /**
     * Set available.
     * 
     * @param available
     */    
    public void setAvailable(@Nullable Boolean available) {
        this.available = available;
    }

    /**
     * Get available.
     * 
     * @return available
     */
    @Nullable
    public Boolean getAvailable() {
        return this.available;
    }

    // DATABASE MAPPING : scanned_at ( DATETIME ) 
    /**
     * Set scannedAt.
     * 
     * @param scannedAt
     */    
    public void setScannedAt(@Nullable Date scannedAt) {
        this.scannedAt = scannedAt;
    }

    /**
     * Get scannedAt.
     * 
     * @return scannedAt
     */
    @Nullable
    public Date getScannedAt() {
        return this.scannedAt;
    }

    // DATABASE MAPPING : created_at ( DATETIME ) 
    /**
     * Set createdAt.
     * 
     * @param createdAt
     */    
    public void setCreatedAt(@Nullable Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * Get createdAt.
     * 
     * @return createdAt
     */
    @Nullable
    public Date getCreatedAt() {
        return this.createdAt;
    }

    // DATABASE MAPPING : updated_at ( DATETIME ) 
    /**
     * Set updatedAt.
     * 
     * @param updatedAt
     */    
    public void setUpdatedAt(@Nullable Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * Get updatedAt.
     * 
     * @return updatedAt
     */
    @Nullable
    public Date getUpdatedAt() {
        return this.updatedAt;
    }

}
