package aka.plexdb.bean;

import java.io.Serializable;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import org.eclipse.jdt.annotation.Nullable;

/**
 * Persistent class for entity stored in table "stream_types".
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class AbstractStreamTypesEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Nullable
    @Column(name = "\"id\"")
    private Integer id;

    @Nullable
    @Column(name = "\"name\"")
    private String name;

    /**
     * Set id.
     * 
     * @param id
     */  
    public void setId(@Nullable Integer id) {
        this.id = id;
    }

    /**
     * Get id.
     * 
     * @return id
     */
    @Nullable
    public Integer getId() {
        return this.id;
    }

    // DATABASE MAPPING : name ( VARCHAR(255) ) 
    /**
     * Set name.
     * 
     * @param name
     */    
    public void setName(@Nullable String name) {
        this.name = name;
    }

    /**
     * Get name.
     * 
     * @return name
     */
    @Nullable
    public String getName() {
        return this.name;
    }

}
