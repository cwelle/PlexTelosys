package aka.plexdb.ctestrategy;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;

/**
 * Bulk Id Strategy implementation for Hibernate using CTE instead of Temporary Table, reducing concurrency on system tables every time Postgresql creates temp tables.
 *
 * @author epiresdasilva
 * @see <a href="https://github.com/epiresdasilva/cte-multi-table-bulk-id-stategy">epiresdasilva repository</a>
 */
public class CTEValues {

    @Nullable
    private String idCteSelect;
    @NonNull
    private List<Object @NonNull []> selectResult = new LinkedList<>();

    /**
     * Constructor.
     */
    public CTEValues() {
        // Nothing to do
    }

    /**
     * Constructor.
     *
     * @param idCteSelect
     * @param selectResult
     */
    public CTEValues(@NonNull final String idCteSelect, @NonNull final List<Object @NonNull []> selectResult) {
        this.idCteSelect = idCteSelect;
        this.selectResult = selectResult;
    }

    /**
     * Get idCteSelect.
     *
     * @return idCteSelect
     */
    @Nullable
    public String getIdCteSelect() {
        return this.idCteSelect;
    }

    /**
     * Set idCteSelect.
     *
     * @param idCteSelect
     */
    public void setIdCteSelect(@Nullable final String idCteSelect) {
        this.idCteSelect = idCteSelect;
    }

    /**
     * Get select results.
     *
     * @return select results
     */
    @NonNull
    public List<Object @NonNull []> getSelectResult() {
        return this.selectResult;
    }

    /**
     * Set select results.
     *
     * @param selectResult
     */
    public void setSelectResult(@NonNull final List<Object @NonNull []> selectResult) {
        this.selectResult = selectResult;
    }
}
