package aka.plexdb.ctestrategy;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;
import org.hibernate.engine.spi.QueryParameters;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.hql.internal.ast.HqlSqlWalker;
import org.hibernate.hql.internal.ast.tree.DeleteStatement;
import org.hibernate.hql.internal.ast.tree.FromElement;
import org.hibernate.hql.spi.id.MultiTableBulkIdStrategy;
import org.hibernate.internal.util.StringHelper;
import org.hibernate.param.ParameterSpecification;
import org.hibernate.persister.collection.AbstractCollectionPersister;
import org.hibernate.persister.entity.Queryable;
import org.hibernate.sql.Delete;
import org.hibernate.type.CollectionType;
import org.hibernate.type.Type;
import org.jboss.logging.Logger;

/**
 * Bulk Id Strategy implementation for Hibernate using CTE instead of Temporary Table, reducing concurrency on system tables every time Postgresql creates temp tables.
 *
 * @author epiresdasilva
 * @see <a href="https://github.com/epiresdasilva/cte-multi-table-bulk-id-stategy">epiresdasilva repository</a>
 */
public class CTEBasedDeleteHandlerImpl extends AbstractCTEBasedBulkIdHandler implements MultiTableBulkIdStrategy.DeleteHandler {

    private static final Logger log = Logger.getLogger(CTEBasedDeleteHandlerImpl.class);

    @NonNull
    private final Queryable targetedPersister;
    private final String idSelect;
    private final List<ParameterSpecification> idSelectParameterSpecifications;
    private final List<String> deletes;

    /**
     * Constructor.
     *
     * @param factory
     * @param walker
     */
    public CTEBasedDeleteHandlerImpl(@NonNull final SessionFactoryImplementor factory, @NonNull final HqlSqlWalker walker) {
        this(factory, walker, null, null);
    }

    /**
     * Constructor.
     *
     * @param factory
     * @param walker
     * @param catalog
     * @param schema
     */
    public CTEBasedDeleteHandlerImpl(@NonNull final SessionFactoryImplementor factory, @NonNull final HqlSqlWalker walker, @Nullable final String catalog, @Nullable final String schema) {
        super(factory, walker, catalog, schema);

        final DeleteStatement deleteStatement = (DeleteStatement) walker.getAST();
        final FromElement fromElement = deleteStatement.getFromClause()
                .getFromElement();

        this.targetedPersister = fromElement.getQueryable();

        final ProcessedWhereClause processedWhereClause = processWhereClause(deleteStatement.getWhereClause());
        this.idSelectParameterSpecifications = processedWhereClause.getIdSelectParameterSpecifications();

        final String bulkTargetAlias = fromElement.getTableAlias();

        this.idSelect = generateIdSelect(this.targetedPersister, bulkTargetAlias, processedWhereClause);

        final String idSubselect = generateIdSubselect(this.targetedPersister);
        this.deletes = new ArrayList<>();

        // If many-to-many, delete the FK row in the collection table.
        // This partially overlaps with DeleteExecutor, but it instead uses the
        // temp table in the idSubselect.
        for (final Type type : this.targetedPersister.getPropertyTypes()) {
            if (type.isCollectionType()) {
                final CollectionType cType = (CollectionType) type;
                @SuppressWarnings("deprecation")
                final AbstractCollectionPersister cPersister = (AbstractCollectionPersister) factory.getCollectionPersister(cType.getRole());
                if (cPersister.isManyToMany()) {
                    this.deletes.add(generateDelete(cPersister.getTableName(), cPersister.getKeyColumnNames(), idSubselect, "bulk delete - m2m join table cleanup"));
                }
            }
        }

        final String[] tableNames = this.targetedPersister.getConstraintOrderedTableNameClosure();
        final String[][] columnNames = this.targetedPersister.getContraintOrderedTableKeyColumnClosure();
        for (int i = 0; i < tableNames.length; i++) {
            // TODO : an optimization here would be to consider cascade deletes
            // and not gen those delete statements;
            // the difficulty is the ordering of the tables here vs the cascade
            // attributes on the persisters ->
            // the table info gotten here should really be self-contained (i.e.,
            // a class representation
            // defining all the needed attributes), then we could then get an
            // array of those
            this.deletes.add(generateDelete(tableNames[i], columnNames[i],
                    idSubselect, "bulk delete"));
        }
    }

    @SuppressWarnings("deprecation")
    private String generateDelete(final String tableName, final String[] columnNames, final String idSubselect, final String comment) {
        final Delete delete = new Delete().setTableName(tableName).setWhere("(" + StringHelper.join(", ", columnNames) + ") IN (" + idSubselect + ")");
        if (factory().getSettings().isCommentsEnabled()) {
            delete.setComment(comment);
        }
        return delete.toStatementString();
    }

    @Override
    public Queryable getTargetedQueryable() {
        return this.targetedPersister;
    }

    @Override
    public String[] getSqlStatements() {
        return this.deletes.toArray(new String[this.deletes.size()]);
    }

    @NonNull
    private CTEValues prepareCteStatement(final SharedSessionContractImplementor session, final QueryParameters queryParameters) {
        final CTEValues values = new CTEValues();

        PreparedStatement ps = null;

        try {
            try {
                ps = session.getJdbcCoordinator().getStatementPreparer().prepareStatement(this.idSelect, false);
                if (ps != null) {
                    int sum = 1;
                    sum += handlePrependedParametersOnIdSelection(ps, session, sum);
                    for (final ParameterSpecification parameterSpecification : this.idSelectParameterSpecifications) {
                        sum += parameterSpecification.bind(ps, queryParameters, session, sum);
                    }
                    final ResultSet rs = session.getJdbcCoordinator().getResultSetReturn().extract(ps);
                    while (rs.next()) {
                        final Object[] result = new Object[this.targetedPersister.getIdentifierColumnNames().length];
                        for (final String columnName : this.targetedPersister.getIdentifierColumnNames()) {
                            final Object column = rs.getObject(columnName);
                            result[rs.findColumn(columnName) - 1] = column;
                        }
                        values.getSelectResult().add(result);
                    }

                    if (values.getSelectResult().isEmpty()) {
                        return values;
                    }

                    final String idCteSelect = generateIdCteSelect(this.targetedPersister, determineIdTableName(this.targetedPersister), values.getSelectResult());
                    values.setIdCteSelect(idCteSelect);
                    log.tracev("Generated ID-CTE-SELECT SQL (multi-table delete) : {0}", idCteSelect);
                }

                return values;
            } finally {
                if (ps != null) {
                    ps.close();
                }
            }
        } catch (final SQLException e) {
            throw convert(e, "could not insert/select ids for bulk delete", this.idSelect);
        }
    }

    @Override
    public int execute(final SharedSessionContractImplementor session, final QueryParameters queryParameters) {
        assert session != null;
        prepareForUse(this.targetedPersister, session);
        try {
            PreparedStatement ps = null;
            int resultCount = 0;

            final CTEValues values = prepareCteStatement(session, queryParameters);

            // Start performing the deletes
            for (String delete : this.deletes) {
                if (values.getIdCteSelect() == null) {
                    continue;
                }

                delete = values.getIdCteSelect() + delete;

                try {
                    try {
                        ps = session.getJdbcCoordinator().getStatementPreparer().prepareStatement(delete, false);
                        if (ps != null) {
                            int pos = 1;
                            pos += handlePrependedParametersOnIdSelection(ps, session, pos);
                            for (final Object[] result : values.getSelectResult()) {
                                for (final Object column : result) {
                                    ps.setObject(pos++, column);
                                }
                            }
                            handleAddedParametersOnDelete(ps, session);
                        }

                        resultCount = session.getJdbcCoordinator().getResultSetReturn().executeUpdate(ps);
                    } finally {
                        if (ps != null) {
                            ps.close();
                        }
                    }
                } catch (final SQLException e) {
                    throw convert(e, "error performing bulk delete", delete);
                }
            }

            return resultCount;

        } finally {
            assert session != null;
            releaseFromUse(this.targetedPersister, session);
        }
    }

    /**
     * Handle Prepended Parameters On Id Selection.
     *
     * @param ps
     * @param session
     * @param pos
     * @return int
     */
    int handlePrependedParametersOnIdSelection(@NonNull final PreparedStatement ps, @NonNull final SharedSessionContractImplementor session, final int pos) {
        return 0;
    }

    /**
     * Handle Added Parameters On Delete.
     *
     * @param ps
     * @param session
     */
    void handleAddedParametersOnDelete(@NonNull final PreparedStatement ps, @NonNull final SharedSessionContractImplementor session) {
        // Nothing to do
    }
}
