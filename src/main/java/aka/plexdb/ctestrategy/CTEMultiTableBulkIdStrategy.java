package aka.plexdb.ctestrategy;

import org.eclipse.jdt.annotation.NonNull;
import org.hibernate.boot.spi.MetadataImplementor;
import org.hibernate.boot.spi.SessionFactoryOptions;
import org.hibernate.engine.jdbc.connections.spi.JdbcConnectionAccess;
import org.hibernate.engine.jdbc.spi.JdbcServices;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.hql.internal.ast.HqlSqlWalker;
import org.hibernate.hql.spi.id.MultiTableBulkIdStrategy;
import org.hibernate.persister.entity.Queryable;

/**
 * Bulk Id Strategy implementation for Hibernate using CTE instead of Temporary Table, reducing concurrency on system tables every time Postgresql creates temp tables.
 *
 * @author epiresdasilva
 * @see <a href="https://github.com/epiresdasilva/cte-multi-table-bulk-id-stategy">epiresdasilva repository</a>
 */
public class CTEMultiTableBulkIdStrategy implements MultiTableBulkIdStrategy {

    /**
     * INSTANCE
     */
    @NonNull
    public static final CTEMultiTableBulkIdStrategy INSTANCE = new CTEMultiTableBulkIdStrategy();

    /**
     * SHORT NAME
     */
    @NonNull
    public static final String SHORT_NAME = "temporary";

    @Override
    public void prepare(final JdbcServices jdbcServices, final JdbcConnectionAccess jdbcConnectionAccess, final MetadataImplementor metadataImplementor, final SessionFactoryOptions sessionFactoryOptions) {
        // Nothing to do
    }

    @Override
    public void release(final JdbcServices jdbcServices, final JdbcConnectionAccess connectionAccess) {
        // nothing to do
    }

    @Override
    public UpdateHandler buildUpdateHandler(final SessionFactoryImplementor factory, final HqlSqlWalker walker) {
        assert factory != null;
        assert walker != null;

        return new CTEBasedUpdateHandlerImpl(factory, walker) {
            @Override
            protected void prepareForUse(final Queryable persister, final SharedSessionContractImplementor session) {
                // nothing to do
            }

            @Override
            protected void releaseFromUse(final Queryable persister, final SharedSessionContractImplementor session) {
                // nothing to do
            }
        };
    }

    @Override
    public DeleteHandler buildDeleteHandler(final SessionFactoryImplementor factory, final HqlSqlWalker walker) {
        assert factory != null;
        assert walker != null;

        return new CTEBasedDeleteHandlerImpl(factory, walker) {
            @Override
            protected void prepareForUse(final Queryable persister, final SharedSessionContractImplementor session) {
                // nothing to do
            }

            @Override
            protected void releaseFromUse(final Queryable persister, final SharedSessionContractImplementor session) {
                // nothing to do
            }
        };
    }

}
