package aka.plexdb.ctestrategy;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;
import org.hibernate.engine.spi.QueryParameters;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.hql.internal.ast.HqlSqlWalker;
import org.hibernate.hql.internal.ast.tree.AssignmentSpecification;
import org.hibernate.hql.internal.ast.tree.FromElement;
import org.hibernate.hql.internal.ast.tree.UpdateStatement;
import org.hibernate.hql.spi.id.MultiTableBulkIdStrategy;
import org.hibernate.internal.util.StringHelper;
import org.hibernate.param.ParameterSpecification;
import org.hibernate.persister.entity.Queryable;
import org.hibernate.sql.Update;

/**
 * Bulk Id Strategy implementation for Hibernate using CTE instead of Temporary Table, reducing concurrency on system tables every time Postgresql creates temp tables.
 *
 * @author epiresdasilva
 * @see <a href="https://github.com/epiresdasilva/cte-multi-table-bulk-id-stategy">epiresdasilva repository</a>
 */
public class CTEBasedUpdateHandlerImpl extends AbstractCTEBasedBulkIdHandler implements MultiTableBulkIdStrategy.UpdateHandler {

    @NonNull
    private static final Logger log = Logger.getAnonymousLogger();
    @NonNull
    private final Queryable targetedPersister;
    @NonNull
    private final String idSelect;
    @NonNull
    private final List<ParameterSpecification> idSelectParameterSpecifications;
    private final String @NonNull [] updates;
    private final ParameterSpecification[][] assignmentParameterSpecifications;

    /**
     * Constructor.
     *
     * @param factory
     * @param walker
     */
    public CTEBasedUpdateHandlerImpl(@NonNull final SessionFactoryImplementor factory, @NonNull final HqlSqlWalker walker) {
        this(factory, walker, null, null);
    }

    /**
     * Constructor.
     *
     * @param factory
     * @param walker
     * @param catalog
     * @param schema
     */
    @SuppressWarnings("deprecation")
    public CTEBasedUpdateHandlerImpl(@NonNull final SessionFactoryImplementor factory, @NonNull final HqlSqlWalker walker, @Nullable final String catalog, @Nullable final String schema) {
        super(factory, walker, catalog, schema);

        final UpdateStatement updateStatement = (UpdateStatement) walker.getAST();
        final FromElement fromElement = updateStatement.getFromClause().getFromElement();

        this.targetedPersister = fromElement.getQueryable();

        final ProcessedWhereClause processedWhereClause = processWhereClause(updateStatement.getWhereClause());
        this.idSelectParameterSpecifications = processedWhereClause.getIdSelectParameterSpecifications();

        final String bulkTargetAlias = fromElement.getTableAlias();

        this.idSelect = generateIdSelect(this.targetedPersister, bulkTargetAlias, processedWhereClause);

        final String[] tableNames = this.targetedPersister.getConstraintOrderedTableNameClosure();
        final String[][] columnNames = this.targetedPersister.getContraintOrderedTableKeyColumnClosure();
        final String idSubselect = generateIdSubselect(this.targetedPersister);

        this.updates = new String[tableNames.length];
        this.assignmentParameterSpecifications = new ParameterSpecification[tableNames.length][];
        for (int tableIndex = 0; tableIndex < tableNames.length; tableIndex++) {
            boolean affected = false;
            final List<ParameterSpecification> parameterList = new ArrayList<>();
            final Update update = new Update(factory().getDialect()).setTableName(tableNames[tableIndex]).setWhere("(" + StringHelper.join(", ", columnNames[tableIndex]) + ") IN (" + idSubselect + ")");
            if (factory().getSettings().isCommentsEnabled()) {
                update.setComment("bulk update");
            }
            @SuppressWarnings("unchecked")
            final List<AssignmentSpecification> assignmentSpecifications = walker.getAssignmentSpecifications();
            for (final AssignmentSpecification assignmentSpecification : assignmentSpecifications) {
                if (assignmentSpecification.affectsTable(tableNames[tableIndex])) {
                    affected = true;
                    update.appendAssignmentFragment(assignmentSpecification.getSqlAssignmentFragment());
                    if (assignmentSpecification.getParameters() != null) {
                        for (int paramIndex = 0; paramIndex < assignmentSpecification.getParameters().length; paramIndex++) {
                            parameterList.add(assignmentSpecification.getParameters()[paramIndex]);
                        }
                    }
                }
            }
            if (affected) {
                this.updates[tableIndex] = update.toStatementString();
                this.assignmentParameterSpecifications[tableIndex] = parameterList
                        .toArray(new ParameterSpecification[parameterList
                                .size()]);
            }
        }
    }

    @Override
    public Queryable getTargetedQueryable() {
        return this.targetedPersister;
    }

    @Override
    public String[] getSqlStatements() {
        return this.updates;
    }

    private CTEValues prepareCteStatement(final SharedSessionContractImplementor session, final QueryParameters queryParameters) {
        final CTEValues values = new CTEValues();

        PreparedStatement ps = null;

        try {
            try {
                ps = session.getJdbcCoordinator().getStatementPreparer().prepareStatement(this.idSelect, false);
                if (ps != null) {
                    int sum = 1;
                    sum += handlePrependedParametersOnIdSelection(ps, session, sum);
                    for (final ParameterSpecification parameterSpecification : this.idSelectParameterSpecifications) {
                        sum += parameterSpecification.bind(ps, queryParameters, session, sum);
                    }
                    final ResultSet rs = session.getJdbcCoordinator().getResultSetReturn().extract(ps);
                    while (rs.next()) {
                        final Object[] result = new Object[this.targetedPersister.getIdentifierColumnNames().length];
                        for (final String columnName : this.targetedPersister.getIdentifierColumnNames()) {
                            final Object column = rs.getObject(columnName);
                            result[rs.findColumn(columnName) - 1] = column;
                        }
                        values.getSelectResult().add(result);
                    }

                    if (values.getSelectResult().isEmpty()) {
                        return values;
                    }

                    final String idCteSelect = generateIdCteSelect(this.targetedPersister, determineIdTableName(this.targetedPersister), values.getSelectResult());
                    values.setIdCteSelect(idCteSelect);

                    log.log(Level.INFO, "Generated ID-CTE-SELECT SQL (multi-table update) : {0}", idCteSelect);
                }
                return values;
            } finally {
                if (ps != null) {
                    ps.close();
                }
            }
        } catch (final SQLException e) {
            throw convert(e, "could not insert/select ids for bulk update", this.idSelect);
        }
    }

    @Override
    public int execute(final SharedSessionContractImplementor session, final QueryParameters queryParameters) {
        assert session != null;
        prepareForUse(this.targetedPersister, session);
        try {
            // First, save off the pertinent ids, as the return value
            PreparedStatement ps = null;
            int resultCount = 0;

            final CTEValues values = prepareCteStatement(session, queryParameters);

            // Start performing the updates
            for (int i = 0; i < this.updates.length; i++) {
                if (this.updates[i] == null || values == null || values.getIdCteSelect() == null) {
                    continue;
                }
                try {
                    try {
                        String update = this.updates[i];
                        update = values.getIdCteSelect() + update;

                        ps = session.getJdbcCoordinator().getStatementPreparer().prepareStatement(update, false);
                        if (ps != null) {
                            int position = 1; // jdbc params are 1-based
                            position += handlePrependedParametersOnIdSelection(ps, session, position);
                            for (final Object[] result : values.getSelectResult()) {
                                for (final Object column : result) {
                                    ps.setObject(position++, column);
                                }
                            }
                            if (this.assignmentParameterSpecifications[i] != null) {
                                for (int x = 0; x < this.assignmentParameterSpecifications[i].length; x++) {
                                    position += this.assignmentParameterSpecifications[i][x].bind(ps, queryParameters, session, position);
                                }
                                handleAddedParametersOnUpdate(ps, session, position);
                            }
                            resultCount = session.getJdbcCoordinator().getResultSetReturn().executeUpdate(ps);
                        }
                    } finally {
                        if (ps != null) {
                            ps.close();
                        }
                    }
                } catch (final SQLException e) {
                    throw convert(e, "error performing bulk update", this.updates[i]);
                }
            }

            return resultCount;
        } finally {
            assert session != null;
            releaseFromUse(this.targetedPersister, session);
        }
    }

    /**
     * Handle Prepended Parameters On Id Selection.
     *
     * @param ps
     * @param session
     * @param pos
     * @return int
     */
    int handlePrependedParametersOnIdSelection(@NonNull final PreparedStatement ps, @NonNull final SharedSessionContractImplementor session, final int pos) {
        return 0;
    }

    /**
     * Handle Added Parameters On Update.
     *
     * @param ps
     * @param session
     * @param position
     */
    void handleAddedParametersOnUpdate(@NonNull final PreparedStatement ps, @NonNull final SharedSessionContractImplementor session, final int position) {
        // Nothing to do
    }
}
