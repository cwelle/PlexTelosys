package aka.plexdb.ctestrategy;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;

/**
 * Bulk Id Strategy implementation for Hibernate using CTE instead of Temporary Table, reducing concurrency on system tables every time Postgresql creates temp tables.
 *
 * @author epiresdasilva
 * @see <a href="https://github.com/epiresdasilva/cte-multi-table-bulk-id-stategy">epiresdasilva repository</a>
 */
public class CTE {

    @NonNull
    private String tableName = "";
    @NonNull
    private final List<@NonNull String> columnsList = new LinkedList<>();
    @NonNull
    private List<Object @NonNull []> selectResult = new LinkedList<>();

    /**
     * Constructor.
     *
     * @param tableName
     */
    public CTE(@Nullable final String tableName) {
        if (tableName != null) {
            this.tableName = tableName;
        }
    }

    /**
     * Add column.
     *
     * @param columnName
     * @return CTE
     */
    @NonNull
    public CTE addColumn(@NonNull final String columnName) {
        this.columnsList.add(columnName);
        return this;
    }

    /**
     * Add columnsList.
     *
     * @param columns
     * @return CTE
     */
    @NonNull
    public CTE addColumns(final String[] columns) {
        if (columns != null) {
            for (final String column : columns) {
                if (column != null) {
                    addColumn(column);
                }
            }
        }
        return this;
    }

    /**
     * Get table name.
     *
     * @return table name
     */
    @Nullable
    public String getTableName() {
        return this.tableName;
    }

    /**
     * Set table name.
     *
     * @param tableName
     */
    public void setTableName(@Nullable final String tableName) {
        if (tableName != null) {
            this.tableName = tableName;
        }
    }

    /**
     * Get Statement String.
     *
     * @return statement string
     */
    @NonNull
    public String toStatementString() {
        final StringBuilder buf = new StringBuilder((this.columnsList.size() * 15) + this.tableName.length() + 10);

        buf.append("with ").append(this.tableName).append(" ( ");
        final Iterator<String> iterator = this.columnsList.iterator();
        while (iterator.hasNext()) {
            buf.append(iterator.next());
            if (iterator.hasNext()) {
                buf.append(", ");
            }
        }
        final StringBuilder parameters = new StringBuilder();
        for (final Object[] result : this.selectResult) {
            if (parameters.length() > 0) {
                parameters.append(", ");
            }
            parameters.append("(");
            for (int i = 0; i < result.length; i++) {
                if (!parameters.toString().endsWith("(")) {
                    parameters.append(", ");
                }
                parameters.append("?");
            }
            parameters.append(")");
        }

        buf.append(" ) as ( values ").append(parameters).append(" )");
        return buf.toString();
    }

    /**
     * Get select results.
     *
     * @return select results
     */
    @NonNull
    public List<Object @NonNull []> getSelectResult() {
        return this.selectResult;
    }

    /**
     * Set select results.
     *
     * @param selectResult
     */
    public void setSelectResult(@NonNull final List<Object @NonNull []> selectResult) {
        this.selectResult = selectResult;
    }
}
