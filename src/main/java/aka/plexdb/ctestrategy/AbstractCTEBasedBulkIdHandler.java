package aka.plexdb.ctestrategy;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;
import org.hibernate.HibernateException;
import org.hibernate.JDBCException;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.hql.internal.ast.HqlSqlWalker;
import org.hibernate.hql.internal.ast.SqlGenerator;
import org.hibernate.internal.util.StringHelper;
import org.hibernate.mapping.Table;
import org.hibernate.param.ParameterSpecification;
import org.hibernate.persister.entity.Queryable;
import org.hibernate.sql.Select;
import org.hibernate.sql.SelectValues;

import antlr.RecognitionException;
import antlr.collections.AST;

/**
 * Bulk Id Strategy implementation for Hibernate using CTE instead of Temporary Table, reducing concurrency on system tables every time Postgresql creates temp tables.
 *
 * @author epiresdasilva
 * @see <a href="https://github.com/epiresdasilva/cte-multi-table-bulk-id-stategy">epiresdasilva repository</a>
 */
public class AbstractCTEBasedBulkIdHandler {

    @NonNull
    private final SessionFactoryImplementor sessionFactory;
    @NonNull
    private final HqlSqlWalker walker;
    @Nullable
    private final String catalog;
    @Nullable
    private final String schema;

    /**
     * Constructor.
     *
     * @param sessionFactory
     * @param walker
     * @param catalog
     * @param schema
     */
    public AbstractCTEBasedBulkIdHandler(@NonNull final SessionFactoryImplementor sessionFactory, @NonNull final HqlSqlWalker walker, @Nullable final String catalog, @Nullable final String schema) {
        this.sessionFactory = sessionFactory;
        this.walker = walker;
        this.catalog = catalog;
        this.schema = schema;
    }

    /**
     * Get factory.
     *
     * @return factory
     */
    @NonNull
    SessionFactoryImplementor factory() {
        return this.sessionFactory;
    }

    /**
     * Get the walker.
     *
     * @return walker
     */
    @NonNull
    HqlSqlWalker walker() {
        return this.walker;
    }

    /**
     * Convert.
     *
     * @param e
     * @param message
     * @param sql
     * @return JDBCException
     */
    @SuppressWarnings("deprecation")
    JDBCException convert(final SQLException e, final String message, final String sql) {
        throw factory().getSQLExceptionHelper().convert(e, message, sql);
    }

    /**
     * Bulk Id Strategy implementation for Hibernate using CTE instead of Temporary Table, reducing concurrency on system tables every time Postgresql creates temp tables.
     *
     * @author epiresdasilva
     * @see <a href="https://github.com/epiresdasilva/cte-multi-table-bulk-id-stategy">epiresdasilva repository</a>
     */
    static class ProcessedWhereClause {

        /**
         * NO WHERE CLAUSE
         */
        @NonNull
        public static final ProcessedWhereClause NO_WHERE_CLAUSE = new ProcessedWhereClause();
        @NonNull
        private final String userWhereClauseFragment;
        @NonNull
        private final List<ParameterSpecification> idSelectParameterSpecifications;

        private ProcessedWhereClause() {
            this("", Collections.<ParameterSpecification> emptyList());
        }

        /**
         * Constructor.
         *
         * @param userWhereClauseFragment
         * @param idSelectParameterSpecifications
         */
        public ProcessedWhereClause(@NonNull final String userWhereClauseFragment, @NonNull final List<ParameterSpecification> idSelectParameterSpecifications) {
            this.userWhereClauseFragment = userWhereClauseFragment;
            this.idSelectParameterSpecifications = idSelectParameterSpecifications;
        }

        /**
         * Get user where clause fragment.
         *
         * @return user where clause fragment
         */
        @NonNull
        public String getUserWhereClauseFragment() {
            return this.userWhereClauseFragment;
        }

        /**
         * Get list of parameter specification.
         *
         * @return list of parameter specification
         */
        @NonNull
        public List<ParameterSpecification> getIdSelectParameterSpecifications() {
            return this.idSelectParameterSpecifications;
        }
    }

    /**
     * Get ProcessedWhereClause.
     *
     * @param whereClause
     * @return process where clause
     */
    ProcessedWhereClause processWhereClause(final AST whereClause) {
        if (whereClause.getNumberOfChildren() != 0) {
            // If a where clause was specified in the update/delete query, use
            // it to limit the
            // returned ids here...
            try {
                final SqlGenerator sqlGenerator = new SqlGenerator(this.sessionFactory);
                sqlGenerator.whereClause(whereClause);
                final String userWhereClause = sqlGenerator.getSQL().substring(7);
                final List<ParameterSpecification> idSelectParameterSpecifications = sqlGenerator.getCollectedParameters();
                return new ProcessedWhereClause(userWhereClause, idSelectParameterSpecifications);
            } catch (final RecognitionException e) {
                throw new HibernateException("Unable to generate id select for DML operation", e);
            }
        } else {
            return ProcessedWhereClause.NO_WHERE_CLAUSE;
        }
    }

    /**
     * Generate IdCteSelect.
     *
     * @param persister
     * @param idTableName
     * @param selectResult
     * @return generateIdCteSelect
     */
    String generateIdCteSelect(@NonNull final Queryable persister, @NonNull final String idTableName, @NonNull final List<Object @NonNull []> selectResult) {
        final CTE cte = new CTE(idTableName);
        cte.addColumns(persister.getIdentifierColumnNames());
        cte.setSelectResult(selectResult);
        return cte.toStatementString();
    }

    /**
     * Generate IdSelect.
     *
     * @param persister
     * @param tableAlias
     * @param whereClause
     * @return generateIdSelect
     */
    @SuppressWarnings("deprecation")
    @NonNull
    String generateIdSelect(@NonNull final Queryable persister, @NonNull final String tableAlias, @NonNull final ProcessedWhereClause whereClause) {
        final Select select = new Select(this.sessionFactory.getDialect());
        final SelectValues selectClause = new SelectValues(this.sessionFactory.getDialect()).addColumns(tableAlias, persister.getIdentifierColumnNames(), persister.getIdentifierColumnNames());
        assert selectClause != null;
        addAnyExtraIdSelectValues(selectClause);
        select.setSelectClause(selectClause.render());
        final String rootTableName = persister.getTableName();
        final String fromJoinFragment = persister.fromJoinFragment(tableAlias, true, false);
        String whereJoinFragment = persister.whereJoinFragment(tableAlias, true, false);
        select.setFromClause(rootTableName + ' ' + tableAlias + fromJoinFragment);
        if (whereJoinFragment == null) {
            whereJoinFragment = "";
        } else {
            whereJoinFragment = whereJoinFragment.trim();
            if (whereJoinFragment.startsWith("and")) {
                whereJoinFragment = whereJoinFragment.substring(4);
            }
        }
        if (whereClause.getUserWhereClauseFragment().length() > 0) {
            if (whereJoinFragment.length() > 0) {
                whereJoinFragment += " and ";
            }
        }
        select.setWhereClause(whereJoinFragment
                + whereClause.getUserWhereClauseFragment());
        return select.toStatementString();
    }

    /**
     * Add Any Extra Id Select Values.
     *
     * @param selectClause
     */
    void addAnyExtraIdSelectValues(@NonNull final SelectValues selectClause) {
        // Nothing to do
    }

    /**
     * Determine Id Table Name.
     *
     * @param persister
     * @return determineIdTableName
     */
    @SuppressWarnings("deprecation")
    @NonNull
    String determineIdTableName(@NonNull final Queryable persister) {
        // todo : use the identifier/name qualifier service once we pull that
        // over to master
        return Table.qualify(this.catalog, this.schema, "HT_" + persister.getTableName());
    }

    /**
     * Generate Id Subselect.
     *
     * @param persister
     * @return generateIdSubselect
     */
    @NonNull
    String generateIdSubselect(@NonNull final Queryable persister) {
        return "select " + StringHelper.join(", ", persister.getIdentifierColumnNames()) + " from " + determineIdTableName(persister);
    }

    /**
     * Prepare For Use.
     *
     * @param persister
     * @param session
     */
    void prepareForUse(@NonNull final Queryable persister, @NonNull final SharedSessionContractImplementor session) {
        // Nothing to do
    }

    /**
     * Release From Use.
     *
     * @param persister
     * @param session
     */
    void releaseFromUse(@NonNull final Queryable persister, @NonNull final SharedSessionContractImplementor session) {
        // Nothing to do
    }
}
