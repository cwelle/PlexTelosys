package aka.plexdb.persistence.commons;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;

import aka.plexdb.persistence.finders.FinderMethod;

/**
 * Generic JPA service operations. <br>
 *
 * @param <T> Entity type
 * @param <PK> Primary key type
 */
@SuppressWarnings("unchecked")
public abstract class GenericJPAService<@NonNull T, @NonNull PK extends java.io.Serializable> {

    private static final boolean TRANSACTIONAL = true;
    @NonNull
    private static final Predicate[] VOID_PREDICATE_ARRAY = {};
    @NonNull
    private final Class<T> persistentClass;

    /**
     * Constructor.
     *
     * @param persistentClass
     */
    public GenericJPAService(@NonNull final Class<@NonNull T> persistentClass) {
        this.persistentClass = persistentClass;
    }

    /**
     * Executes a NON TRANSACTIONAL JPA operation with a specific EntityManager.<br>
     * ( created at the beginning of the execution and closed at the end )
     *
     * @param operation the operation to be executed
     * @return result
     * @throws PersistenceException
     */
    @Nullable
    protected final Object execute(final @NonNull JPAOperation operation) throws PersistenceException {
        return execute(operation, false);
    }

    /**
     * Executes a JPA operation ( TRANSACTIONAL or NOT ) with a specific EntityManager.<br>
     * ( created at the beginning of the execution and closed at the end )
     *
     * @param operation the operation to be executed
     * @param transactional true for a TRANSACTIONAL operation, false for NOT TRANSACTIONAL
     * @return result
     * @throws PersistenceException
     */
    @Nullable
    protected final Object execute(final @NonNull JPAOperation operation, final boolean transactional) throws PersistenceException {
        final JPAEnvironment jpaEnvironment = JPAEnvironments.getInstance().getCurrentJPAEnvironment();

        Object result = null;
        if (jpaEnvironment != null) {
            if (transactional) {
                result = jpaEnvironment.executeWithTransaction(operation);
            } else {
                result = jpaEnvironment.executeWithoutTransaction(operation);
            }
        }
        return result;
    }

    /**
     * Find entity by Primary Key.
     *
     * @param primaryKey
     * @return entity
     */
    @Nullable
    public T load(final @NonNull PK primaryKey) {
        final JPAOperation operation = em -> em.find(this.persistentClass, primaryKey);
        return (T) execute(operation);
    }

    /**
     * Load all entities.
     *
     * @return all entities
     */
    @NonNull
    public List<@NonNull T> loadAll() {
        final JPAOperation operation = em -> {
            final Query query = em.createQuery("from " + this.persistentClass.getName());
            return query.getResultList();
        };

        List<@NonNull T> result = (List<T>) execute(operation);
        if (result == null) {
            result = new ArrayList<>();
        }

        return result;
    }

    /**
     * Load a list of entities using a named query without parameter.
     *
     * @param queryName
     * @return result
     */
    @NonNull
    public List<@NonNull T> loadByNamedQuery(final @NonNull String queryName) {
        final JPAOperation operation = em -> {
            final Query query = em.createNamedQuery(queryName);
            return query.getResultList();
        };

        List<@NonNull T> result = (List<T>) execute(operation);
        if (result == null) {
            result = new ArrayList<>();
        }

        return result;
    }

    /**
     * Load a list of entities using a named query with parameters
     *
     * @param queryName
     * @param queryParameters
     * @return list of entities
     */
    @NonNull
    public List<@NonNull T> loadByNamedQuery(final @NonNull String queryName, @NonNull final Map<@NonNull String, @NonNull Object> queryParameters) {
        final JPAOperation operation = em -> {
            final Query query = em.createNamedQuery(queryName);
            final Iterator<String> i = queryParameters.keySet().iterator();
            while (i.hasNext()) {
                final String key = i.next();
                query.setParameter(key, queryParameters.get(key));
            }
            return query.getResultList();
        };

        List<@NonNull T> result = (List<T>) execute(operation);
        if (result == null) {
            result = new ArrayList<>();
        }

        return result;
    }

    /**
     * Insert entity ( TRANSACTIONAL ).
     *
     * @param entity
     * @throws PersistenceException
     */
    public void insert(final @NonNull T entity) throws PersistenceException {
        final JPAOperation operation = em -> {
            em.persist(entity);
            return null;
        };

        execute(operation, TRANSACTIONAL);
    }

    /**
     * Save the given entity ( TRANSACTIONAL ).
     *
     * @param entityToSave
     * @return saved entity
     * @throws PersistenceException
     */
    @NonNull
    public T save(final @NonNull T entityToSave) throws PersistenceException {
        final JPAOperation operation = em -> {
            final T managedEntity = em.merge(entityToSave);
            return managedEntity;
        };

        return (T) execute(operation, TRANSACTIONAL);
    }

    /**
     * Delete entity by primary key ( TRANSACTIONAL ).
     *
     * @param primaryKey
     * @return <code>true</code> if successfully deleted
     * @throws PersistenceException
     */
    public boolean delete(final @NonNull PK primaryKey) throws PersistenceException {
        final JPAOperation operation = em -> {
            final T entity = em.find(this.persistentClass, primaryKey);
            em.remove(entity);
            return Boolean.TRUE;
        };

        final Boolean b = (Boolean) execute(operation, TRANSACTIONAL);

        boolean result = false;
        if (b != null) {
            result = b.booleanValue();
        }
        return result;
    }

    /**
     * Search entities using the given query parameters. <br>
     * Returns all the entities if no query parameter
     *
     * @param queryParameters the query parameters to be used (can be null )
     * @return entities found
     */
    @NonNull
    public List<@NonNull T> search(@NonNull final Map<@NonNull String, @NonNull Object> queryParameters) {
        if (queryParameters.isEmpty()) {
            return loadAll();
        } else {
            return searchWithParameters(queryParameters);
        }
    }

    /**
     * Search entities using the given query parameters. <br>
     *
     * @param finder
     * @return entities found
     */
    @NonNull
    public List<@NonNull T> executeQuery(@NonNull final FinderMethod<@NonNull T> finder) {
        final JPAOperation jpaOperation = (@NonNull final EntityManager em) -> {
            final Query query = finder.getBuildedQuery(em);

            return query.getResultList();
        };

        List<@NonNull T> result = (List<T>) execute(jpaOperation);
        if (result == null) {
            result = new ArrayList<>();
        }

        return result;
    }

    /**
     * Search entities using given query parameters.
     *
     * @param queryParameters the query parameters to be used (cannot be null )
     * @return entities found
     */
    @NonNull
    private List<@NonNull T> searchWithParameters(final @NonNull Map<@NonNull String, @NonNull Object> queryParameters) {
        final JPAOperation operation = em -> {
            final CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
            final CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(this.persistentClass);
            final Root<T> from = criteriaQuery.from(this.persistentClass);

            final List<Predicate> predicates = new ArrayList<>();

            for (final Map.Entry<String, Object> e : queryParameters.entrySet()) {
                final String expression = e.getKey();
                final Object value = e.getValue();

                if (value != null) {
                    boolean operatorFound = false;
                    final int i = expression.indexOf(' ');
                    if (i >= 0) {
                        final String name = expression.substring(0, i);
                        final String oper = expression.substring(i, expression.length()).trim();
                        if (oper.length() > 0) {
                            operatorFound = true;
                            if (value instanceof String) {
                                final String strValue = (String) value;
                                if ("=".equalsIgnoreCase(oper)) {
                                    final Predicate p1 = criteriaBuilder.equal(from.get(name), strValue);
                                    predicates.add(p1);
                                } else if ("like".equalsIgnoreCase(oper)) {
                                    final Predicate p2 = criteriaBuilder.like(from.<String> get(name), strValue);
                                    predicates.add(p2);
                                } else {
                                    throw new RuntimeException("Search : invalid operator '" + oper + "' for String attribute");
                                }
                            } else if (value instanceof Number) {
                                final Number numValue = (Number) value;
                                if ("=".equalsIgnoreCase(oper)) {
                                    final Predicate p3 = criteriaBuilder.equal(from.<Number> get(name), numValue);
                                    predicates.add(p3);
                                } else if ("!=".equalsIgnoreCase(oper) || "<>".equalsIgnoreCase(oper)) {
                                    final Predicate p4 = criteriaBuilder.notEqual(from.<Number> get(name), numValue);
                                    predicates.add(p4);
                                } else if (">".equalsIgnoreCase(oper)) {
                                    final Predicate p5 = criteriaBuilder.gt(from.<Number> get(name), numValue);
                                    predicates.add(p5);
                                } else if ("<".equalsIgnoreCase(oper)) {
                                    final Predicate p6 = criteriaBuilder.lt(from.<Number> get(name), numValue);
                                    predicates.add(p6);
                                } else if (">=".equalsIgnoreCase(oper)) {
                                    final Predicate p7 = criteriaBuilder.ge(from.<Number> get(name), numValue);
                                    predicates.add(p7);
                                } else if ("<=".equalsIgnoreCase(oper)) {
                                    final Predicate p8 = criteriaBuilder.le(from.<Number> get(name), numValue);
                                    predicates.add(p8);
                                } else {
                                    throw new RuntimeException("Search : invalid operator '" + oper + "' for Number attribute");
                                }
                            }
                        }
                    }
                    if (!operatorFound) {
                        predicates.add(criteriaBuilder.equal(from.get(expression), value));
                    }
                }
            }

            criteriaQuery.where(predicates.toArray(VOID_PREDICATE_ARRAY));

            final TypedQuery<T> typedQuery = em.createQuery(criteriaQuery);
            return typedQuery.getResultList();
        };

        List<@NonNull T> result = (List<T>) execute(operation);
        if (result == null) {
            result = new ArrayList<>();
        }

        return result;
    }
}
