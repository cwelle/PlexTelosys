package aka.plexdb.persistence.commons;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;
import org.hibernate.exception.JDBCConnectionException;
import org.hibernate.tool.schema.spi.SchemaManagementException;

/**
 * JPAEnvironments manager.
 */
public class JPAEnvironments {

    @NonNull
    private final static JPAEnvironments singleInstance = new JPAEnvironments();
    @NonNull
    private final Map<@NonNull String, JPAEnvironment> environments = new HashMap<>();
    @Nullable
    private JPAEnvironment currentJPAEnvironment;

    private JPAEnvironments() {
    }

    /**
     * @return instance of JPAEnvironments
     */
    public final static JPAEnvironments getInstance() {
        return singleInstance;
    }

    /**
     * Set current JPAEnvironment.
     *
     * @param databaseName
     * @throws SchemaManagementException if schema does not correspond
     * @throws JDBCConnectionException if not a database file
     * @throws SQLException if a database access error occurs or the URL is null
     */
    public void setCurrentJPAEnvironment(@NonNull final String databaseName) throws SchemaManagementException, JDBCConnectionException, SQLException {
        closeCurrentJPAEnvironment();
        JPAEnvironment env = this.environments.get(databaseName);
        if (env == null) {
            env = createNewJPAEnvironment(databaseName);
        }
        env.openConnection();
        this.currentJPAEnvironment = env;
    }

    @NonNull
    private synchronized JPAEnvironment createNewJPAEnvironment(@NonNull final String databaseName) throws JDBCConnectionException, SchemaManagementException, SQLException {
        JPAEnvironment env = this.environments.get(databaseName); // double check
        if (env == null) {
            env = new JPAEnvironment(databaseName);
            this.environments.put(databaseName, env);
        }
        return env;
    }

    /**
     * @return current JPAEnvironment.
     */
    @Nullable
    public JPAEnvironment getCurrentJPAEnvironment() {
        return this.currentJPAEnvironment;
    }

    /**
     * Close current JPAEnvironment connection.
     */
    public void closeCurrentJPAEnvironment() {
        if (this.currentJPAEnvironment != null) {
            this.currentJPAEnvironment.close();
        }
    }

    /**
     * Close all sessions and sessionfactory.
     */
    public void destroy() {
        for (final Entry<@NonNull String, JPAEnvironment> entry : this.environments.entrySet()) {
            final JPAEnvironment jpaEnvironment = entry.getValue();
            if (jpaEnvironment != null) {
                jpaEnvironment.destroy();
            }
        }

        this.environments.clear();
        this.currentJPAEnvironment = null;
    }
}
