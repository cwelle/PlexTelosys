package aka.plexdb.persistence.commons;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;

/**
 * JPA operation interface.
 */
public interface JPAOperation {

    /**
     * Executes a JPA operation using the given EntityManager.
     * 
     * @param em the EntityManager to be used
     * @return result of operation
     * @throws PersistenceException
     */
    @Nullable
    public Object execute(final @NonNull EntityManager em) throws PersistenceException;
}
