package aka.plexdb.persistence.commons;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceException;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;
import org.hibernate.Session;
import org.hibernate.SessionBuilder;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AvailableSettings;
import org.hibernate.cfg.Configuration;
import org.hibernate.exception.JDBCConnectionException;
import org.hibernate.tool.schema.spi.SchemaManagementException;
import org.sqlite.SQLiteConfig;
import org.sqlite.SQLiteConfig.Pragma;

import aka.plexdb.bean.PlexBeans;
import aka.plexdb.ctestrategy.CTEMultiTableBulkIdStrategy;

/**
 * JPA environment for JPA operations execution.
 */
public class JPAEnvironment {

    @Nullable
    private SessionFactory emf;
    @Nullable
    private Session session;
    @Nullable
    private Connection conn;

    /**
     * Constructor.
     *
     * @param databaseName the name of the database
     * @throws SchemaManagementException if schema does not correspond
     * @throws JDBCConnectionException if not a database file
     * @throws SQLException if a database access error occurs or the URL is null
     */
    public JPAEnvironment(@NonNull final String databaseName) throws SchemaManagementException, JDBCConnectionException, SQLException {
        this.session = null;
        try {
            final SQLiteConfig sqLiteConfig = new SQLiteConfig();
            final Properties properties = sqLiteConfig.toProperties();
            properties.setProperty(Pragma.DATE_STRING_FORMAT.pragmaName, "yyyy-MM-dd HH:mm:ss");
            this.conn = DriverManager.getConnection("jdbc:sqlite:" + databaseName, properties);

            final Configuration annotationConfig = new Configuration();
            annotationConfig.addProperties(properties);
            annotationConfig.addPackage("bean");
            annotationConfig.setProperty(AvailableSettings.DIALECT, "org.hibernate.dialect.SQLiteDialect");
            annotationConfig.setProperty(AvailableSettings.URL, "jdbc:sqlite:" + databaseName);
            annotationConfig.setProperty(AvailableSettings.DRIVER, "org.sqlite.JDBC");
            annotationConfig.setProperty(AvailableSettings.HQL_BULK_ID_STRATEGY, CTEMultiTableBulkIdStrategy.class.getName());
            if (databaseName.contains("mem:")) {
                annotationConfig.setProperty(AvailableSettings.HBM2DDL_AUTO, "create-drop");
            } else {
                annotationConfig.setProperty(AvailableSettings.HBM2DDL_AUTO, "validate");
            }

            final List<Class<? extends Object>> allClasses = PlexBeans.PLEX_BEAN_LIST;
            for (final Class<?> class1 : allClasses) {
                annotationConfig.addAnnotatedClass(class1); // add all classes JPA annotated
            }

            this.emf = annotationConfig.buildSessionFactory();
        } catch (SchemaManagementException | JDBCConnectionException | SQLException e) {
            Logger.getAnonymousLogger().logp(Level.SEVERE, "JPAEnvironment", "JPAEnvironment", e.getMessage(), e);
            throw e;
        }
    }

    /**
     * Execute with transaction.
     *
     * @param operation
     * @return result
     * @throws PersistenceException
     */
    @Nullable
    public final Object executeWithTransaction(@NonNull final JPAOperation operation) throws PersistenceException {

        Object result = null;

        final Session currentEm = this.session;
        if (currentEm != null) {
            final EntityTransaction transaction = currentEm.getTransaction();
            try {
                // Begin Transaction
                transaction.begin();

                // Execute application JPA operation with the current EntityManager
                result = operation.execute(currentEm);

                // Commit Transaction
                transaction.commit();

            } finally {
                // Rollback Transaction if Transaction is still active ( Commit not done )
                if (transaction.isActive()) {
                    transaction.rollback();
                }
            }
        }

        return result;
    }

    /**
     * Execute without transaction.
     *
     * @param operation
     * @return result
     * @throws PersistenceException
     */
    @Nullable
    public final Object executeWithoutTransaction(@NonNull final JPAOperation operation) throws PersistenceException {
        @Nullable
        Object result = null;
        if (this.session != null) {
            result = operation.execute(this.session);
        }
        return result;
    }

    /**
     * Close connection.
     */
    public void close() {
        if (this.session != null) {
            this.session.close();
        }
    }

    /**
     * Open connection.
     */
    public void openConnection() {
        if (this.emf != null) {
            final SessionBuilder<?> sb = this.emf.withOptions();
            final SessionBuilder<?> connection = sb.connection(this.conn);
            if (connection != null) {
                this.session = connection.openSession();
            }
        }
    }

    /**
     * Close connection and session factory.
     */
    public void destroy() {
        close();
        if (this.emf != null) {
            this.emf.close();
        }
    }
}
