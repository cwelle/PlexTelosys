package aka.plexdb.persistence.finders.metadataitemviews;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.eclipse.jdt.annotation.NonNull;

import aka.plexdb.bean.MetadataItemViewsEntity;
import aka.plexdb.persistence.finders.FinderMethod;

/**
 * SELECT Count(x) FROM MetadataItemViewsEntity x
 */
public final class MetadataItemViewsCountAll implements FinderMethod<@NonNull MetadataItemViewsEntity> {

    @NonNull
    private static final String QUERY = "SELECT Count(x) FROM MetadataItemViewsEntity x";

    @Override
    @NonNull
    public Query getBuildedQuery(@NonNull final EntityManager em) {
        final Query query = em.createQuery(QUERY);
        assert query != null;
        return query;
    }
}
