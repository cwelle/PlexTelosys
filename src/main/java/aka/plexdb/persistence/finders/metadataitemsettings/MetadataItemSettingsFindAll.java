package aka.plexdb.persistence.finders.metadataitemsettings;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.eclipse.jdt.annotation.NonNull;

import aka.plexdb.bean.MetadataItemSettingsEntity;
import aka.plexdb.persistence.finders.FinderMethod;

/**
 * SELECT x FROM MetadataItemSettingsEntity x
 */
public final class MetadataItemSettingsFindAll implements FinderMethod<@NonNull MetadataItemSettingsEntity> {

    @NonNull
    private static final String QUERY = "SELECT x FROM MetadataItemSettingsEntity x";

    @Override
    @NonNull
    public Query getBuildedQuery(@NonNull final EntityManager em) {
        final Query query = em.createQuery(QUERY);
        assert query != null;
        return query;
    }
}
