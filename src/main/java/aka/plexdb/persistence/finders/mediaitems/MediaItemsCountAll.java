package aka.plexdb.persistence.finders.mediaitems;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.eclipse.jdt.annotation.NonNull;

import aka.plexdb.bean.MediaItemsEntity;
import aka.plexdb.persistence.finders.FinderMethod;

/**
 * SELECT Count(x) FROM MediaItemsEntity x
 */
public final class MediaItemsCountAll implements FinderMethod<@NonNull MediaItemsEntity> {

    @NonNull
    private static final String QUERY = "SELECT Count(x) FROM MediaItemsEntity x";

    @Override
    @NonNull
    public Query getBuildedQuery(@NonNull final EntityManager em) {
        final Query query = em.createQuery(QUERY);
        assert query != null;
        return query;
    }
}
