package aka.plexdb.persistence.finders.sectionlocations;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.eclipse.jdt.annotation.NonNull;

import aka.plexdb.bean.SectionLocationsEntity;
import aka.plexdb.persistence.finders.FinderMethod;

/**
 * SELECT x FROM SectionLocationsEntity x
 */
public final class SectionLocationsFindAll implements FinderMethod<@NonNull SectionLocationsEntity> {

    @NonNull
    private static final String QUERY = "SELECT x FROM SectionLocationsEntity x";

    @Override
    @NonNull
    public Query getBuildedQuery(@NonNull final EntityManager em) {
        final Query query = em.createQuery(QUERY);
        assert query != null;
        return query;
    }
}
