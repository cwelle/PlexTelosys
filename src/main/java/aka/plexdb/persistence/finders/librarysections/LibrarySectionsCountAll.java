package aka.plexdb.persistence.finders.librarysections;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.eclipse.jdt.annotation.NonNull;

import aka.plexdb.bean.LibrarySectionsEntity;
import aka.plexdb.persistence.finders.FinderMethod;

/**
 * SELECT Count(x) FROM LibrarySectionsEntity x
 */
public final class LibrarySectionsCountAll implements FinderMethod<@NonNull LibrarySectionsEntity> {

    @NonNull
    private static final String QUERY = "SELECT Count(x) FROM LibrarySectionsEntity x";

    @Override
    @NonNull
    public Query getBuildedQuery(@NonNull final EntityManager em) {
        final Query query = em.createQuery(QUERY);
        assert query != null;
        return query;
    }
}
