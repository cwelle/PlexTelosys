package aka.plexdb.persistence.finders.streamtypes;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.eclipse.jdt.annotation.NonNull;

import aka.plexdb.bean.StreamTypesEntity;
import aka.plexdb.persistence.finders.FinderMethod;

/**
 * SELECT x FROM StreamTypesEntity x
 */
public final class StreamTypesFindAll implements FinderMethod<@NonNull StreamTypesEntity> {

    @NonNull
    private static final String QUERY = "SELECT x FROM StreamTypesEntity x";

    @Override
    @NonNull
    public Query getBuildedQuery(@NonNull final EntityManager em) {
        final Query query = em.createQuery(QUERY);
        assert query != null;
        return query;
    }
}
