package aka.plexdb.persistence.finders.directories;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.eclipse.jdt.annotation.NonNull;

import aka.plexdb.bean.DirectoriesEntity;
import aka.plexdb.persistence.finders.FinderMethod;

/**
 * SELECT x FROM DirectoriesEntity x
 */
public final class DirectoriesFindAll implements FinderMethod<@NonNull DirectoriesEntity> {

    @NonNull
    private static final String QUERY = "SELECT x FROM DirectoriesEntity x";

    @Override
    @NonNull
    public Query getBuildedQuery(@NonNull final EntityManager em) {
        final Query query = em.createQuery(QUERY);
        assert query != null;
        return query;
    }
}
