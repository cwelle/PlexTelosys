package aka.plexdb.persistence.finders.mediastreams;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.eclipse.jdt.annotation.NonNull;

import aka.plexdb.bean.MediaStreamsEntity;
import aka.plexdb.persistence.finders.FinderMethod;

/**
 * SELECT x FROM MediaStreamsEntity x
 */
public final class MediaStreamsFindAll implements FinderMethod<@NonNull MediaStreamsEntity> {

    @NonNull
    private static final String QUERY = "SELECT x FROM MediaStreamsEntity x";

    @Override
    @NonNull
    public Query getBuildedQuery(@NonNull final EntityManager em) {
        final Query query = em.createQuery(QUERY);
        assert query != null;
        return query;
    }
}
