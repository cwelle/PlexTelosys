package aka.plexdb.persistence.finders.metadataitems;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.eclipse.jdt.annotation.NonNull;

import aka.plexdb.bean.MetadataItemsEntity;
import aka.plexdb.persistence.finders.FinderMethod;

/**
 * SELECT Count(x) FROM MetadataItemsEntity x
 */
public final class MetadataItemsCountAll implements FinderMethod<@NonNull MetadataItemsEntity> {

    @NonNull
    private static final String QUERY = "SELECT Count(x) FROM MetadataItemsEntity x";

    @Override
    @NonNull
    public Query getBuildedQuery(@NonNull final EntityManager em) {
        final Query query = em.createQuery(QUERY);
        assert query != null;
        return query;
    }
}
