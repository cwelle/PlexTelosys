package aka.plexdb.persistence.finders.taggings;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.eclipse.jdt.annotation.NonNull;

import aka.plexdb.bean.TaggingsEntity;
import aka.plexdb.persistence.finders.FinderMethod;

/**
 * SELECT x FROM TaggingsEntity x
 */
public final class TaggingsFindAll implements FinderMethod<@NonNull TaggingsEntity> {

    @NonNull
    private static final String QUERY = "SELECT x FROM TaggingsEntity x";

    @Override
    @NonNull
    public Query getBuildedQuery(@NonNull final EntityManager em) {
        final Query query = em.createQuery(QUERY);
        assert query != null;
        return query;
    }
}
