package aka.plexdb.persistence.finders.tags;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.eclipse.jdt.annotation.NonNull;

import aka.plexdb.bean.TagsEntity;
import aka.plexdb.persistence.finders.FinderMethod;

/**
 * SELECT Count(x) FROM TagsEntity x
 */
public final class TagsCountAll implements FinderMethod<@NonNull TagsEntity> {

    @NonNull
    private static final String QUERY = "SELECT Count(x) FROM TagsEntity x";

    @Override
    @NonNull
    public Query getBuildedQuery(@NonNull final EntityManager em) {
        final Query query = em.createQuery(QUERY);
        assert query != null;
        return query;
    }
}
