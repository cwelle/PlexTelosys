package aka.plexdb.persistence.finders.mediaparts;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.eclipse.jdt.annotation.NonNull;

import aka.plexdb.bean.MediaPartsEntity;
import aka.plexdb.persistence.finders.FinderMethod;

/**
 * SELECT x FROM MediaPartsEntity x
 */
public final class MediaPartsFindAll implements FinderMethod<@NonNull MediaPartsEntity> {

    @NonNull
    private static final String QUERY = "SELECT x FROM MediaPartsEntity x";

    @Override
    @NonNull
    public Query getBuildedQuery(@NonNull final EntityManager em) {
        final Query query = em.createQuery(QUERY);
        assert query != null;
        return query;
    }
}
