package aka.plexdb.persistence.finders;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.eclipse.jdt.annotation.NonNull;

/**
 * Must be implemented by finders.
 * 
 * @param <T>
 */
public interface FinderMethod<T> {

    /**
     * Build query.
     *
     * @param em
     * @return builded query
     */
    @NonNull
    Query getBuildedQuery(@NonNull EntityManager em);
}
