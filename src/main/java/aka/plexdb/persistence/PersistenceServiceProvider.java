package aka.plexdb.persistence;

import org.eclipse.jdt.annotation.NonNull;

/**
 * Very simple persistence service provider.
 */
public final class PersistenceServiceProvider {

    @NonNull
    private final static String SERVICES_ROOT_PACKAGE = PersistenceServiceProvider.class.getPackage().getName() + ".services";

    /**
     * Returns a persistence service for the given persistence implementation.
     *
     * @param serviceInterface
     * @return T
     */
    @SuppressWarnings("unchecked")
    @NonNull
    public final static <T> T getService(@NonNull final Class<@NonNull T> serviceInterface) {
        // --- 1) define the full class name
        final String pkg = SERVICES_ROOT_PACKAGE + ".impl";
        final String suffix = "Impl";
        final String serviceClassName = pkg + "." + serviceInterface.getSimpleName() + suffix;

        // --- 2) try to load the class
        Class<?> clazz;
        Object instance = null;
        try {
            clazz = Class.forName(serviceClassName);
        } catch (final ClassNotFoundException e) {
            throw new RuntimeException("Cannot load class " + serviceClassName, e);
        }

        if (serviceInterface.isAssignableFrom(clazz)) {

            // --- 3) try to create an instance of this class
            try {
                instance = clazz.newInstance();
                return (T) instance;
            } catch (final InstantiationException e) {
                throw new RuntimeException("Cannot create instance for class " + serviceClassName + " (InstantiationException)", e);
            } catch (final IllegalAccessException e) {
                throw new RuntimeException("Cannot create instance for class " + serviceClassName + " (IllegalAccessException)", e);
            }
        } else {
            throw new RuntimeException("Class " + serviceClassName + " is not an implementation of " + serviceInterface.getSimpleName());
        }
    }
}
