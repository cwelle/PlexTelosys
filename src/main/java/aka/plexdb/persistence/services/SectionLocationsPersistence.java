package aka.plexdb.persistence.services;

import java.util.List;
import java.util.Map;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;

import aka.plexdb.bean.SectionLocationsEntity;
import aka.plexdb.persistence.finders.FinderMethod;

/**
 * Basic persistence operations for entity "SectionLocations".
 * 
 * This Bean has a basic Primary Key : Integer
 *
 */
public interface SectionLocationsPersistence {

    /**
     * Deletes the given entity. <br>
     * Transactional operation ( begin transaction and commit )
     * 
     * @param sectionLocations
     * @return <code>true</code> if found and deleted, <code>false</code> if not found
     */
    public boolean delete(final @NonNull SectionLocationsEntity sectionLocations);

    /**
     * Deletes the entity by its Primary Key. <br>
     * Transactional operation ( begin transaction and commit )
     * 
     * @param id
     * @return <code>true</code> if found and deleted, <code>false</code> if not found
     */
    public boolean delete(final @NonNull Integer id);

    /**
     * Inserts the given entity and commit. <br>
     * Transactional operation ( begin transaction and commit )
     * 
     * @param sectionLocations
     */
    public void insert(final @NonNull SectionLocationsEntity sectionLocations);

    /**
     * Loads the entity for the given Primary Key. <br>
     * 
     * @param id
     * @return the entity loaded (or null if not found)
     */
    @Nullable
    public SectionLocationsEntity load(final @NonNull Integer id);

    /**
     * Loads ALL the entities (use with caution).
     * 
     * @return all entities
     */
    @NonNull
    public List<@NonNull SectionLocationsEntity> loadAll();

    /**
     * Loads a list of entities using the given "named query" without parameter.
     * 
     * @param queryName name of the query
     * @return list of entities
     */
    @NonNull
    public List<@NonNull SectionLocationsEntity> loadByNamedQuery(final @NonNull String queryName);

    /**
     * Loads a list of entities using the given "named query" with parameters. 
     * 
     * @param queryName name of the query
     * @param queryParameters parameters of the query
     * @return liste of entities
     */
    @NonNull
	public List<@NonNull SectionLocationsEntity> loadByNamedQuery(final @NonNull String queryName, final @NonNull Map<@NonNull String, @NonNull Object> queryParameters);

    /**
     * Saves (create or update) the given entity. <br>
     * 
     * Transactional operation ( begin transaction and commit )
     * @param sectionLocations
     * @return save of create entity
     */
    @NonNull
    public SectionLocationsEntity save(final @NonNull SectionLocationsEntity sectionLocations);

    /**
     * Search the entities matching the given search criteria.
     * 
     * @param criteria
     * @return entities matching criteria
     */
    @NonNull    
    public List<@NonNull SectionLocationsEntity> search(final @NonNull Map<@NonNull String, @NonNull Object> criteria);

    /**
     * Execute the given finder.
     *
     * @param finder
     * @return entities matching finder
     */
    @NonNull
    public List<@NonNull SectionLocationsEntity> executeQuery(@NonNull FinderMethod<@NonNull SectionLocationsEntity> finder);

    /**
     * Count all the occurrences
     * 
     * @return number of occurrences
     */
	public long countAll();
}
