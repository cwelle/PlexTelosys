package aka.plexdb.persistence.services.impl;

import javax.persistence.Query;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;

import aka.plexdb.bean.MediaStreamsEntity;
import aka.plexdb.persistence.commons.GenericJPAService;
import aka.plexdb.persistence.commons.JPAOperation;
import aka.plexdb.persistence.finders.mediastreams.MediaStreamsCountAll;
import aka.plexdb.persistence.services.MediaStreamsPersistence;

/**
 * JPA implementation for basic persistence operations ( entity "MediaStreams" ).
 */
public final class MediaStreamsPersistenceImpl extends GenericJPAService<@NonNull MediaStreamsEntity, @NonNull Integer> implements MediaStreamsPersistence {

	/**
	 * Constructor.
	 */
	public MediaStreamsPersistenceImpl() {
		super(MediaStreamsEntity.class);
	}

	@Override
    @Nullable 
	public MediaStreamsEntity load(final @NonNull Integer id) {
		return super.load(id);
	}

	@Override
	public boolean delete(final @NonNull Integer id) {
		return super.delete(id);
	}

	@Override
	public boolean delete(final @NonNull MediaStreamsEntity entity) {
        boolean result = false;
        Integer key = entity.getId();
        if (key != null) {
            result = super.delete(key);
        }
		return result;
	}

    @Override
    public long countAll() {
        final JPAOperation operation = em -> {
            final MediaStreamsCountAll finder = new MediaStreamsCountAll();
            final Query query = finder.getBuildedQuery(em);
            return query.getSingleResult();
        };

        final Long value = (Long) execute(operation);
        long result = 0;
        if (value != null) {
            result = value.longValue();
        }

        return result;
    }
}
