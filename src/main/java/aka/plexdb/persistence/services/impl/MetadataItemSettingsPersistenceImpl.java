package aka.plexdb.persistence.services.impl;

import javax.persistence.Query;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;

import aka.plexdb.bean.MetadataItemSettingsEntity;
import aka.plexdb.persistence.commons.GenericJPAService;
import aka.plexdb.persistence.commons.JPAOperation;
import aka.plexdb.persistence.finders.metadataitemsettings.MetadataItemSettingsCountAll;
import aka.plexdb.persistence.services.MetadataItemSettingsPersistence;

/**
 * JPA implementation for basic persistence operations ( entity "MetadataItemSettings" ).
 */
public final class MetadataItemSettingsPersistenceImpl extends GenericJPAService<@NonNull MetadataItemSettingsEntity, @NonNull Integer> implements MetadataItemSettingsPersistence {

	/**
	 * Constructor.
	 */
	public MetadataItemSettingsPersistenceImpl() {
		super(MetadataItemSettingsEntity.class);
	}

	@Override
    @Nullable 
	public MetadataItemSettingsEntity load(final @NonNull Integer id) {
		return super.load(id);
	}

	@Override
	public boolean delete(final @NonNull Integer id) {
		return super.delete(id);
	}

	@Override
	public boolean delete(final @NonNull MetadataItemSettingsEntity entity) {
        boolean result = false;
        Integer key = entity.getId();
        if (key != null) {
            result = super.delete(key);
        }
		return result;
	}

    @Override
    public long countAll() {
        final JPAOperation operation = em -> {
            final MetadataItemSettingsCountAll finder = new MetadataItemSettingsCountAll();
            final Query query = finder.getBuildedQuery(em);
            return query.getSingleResult();
        };

        final Long value = (Long) execute(operation);
        long result = 0;
        if (value != null) {
            result = value.longValue();
        }

        return result;
    }
}
