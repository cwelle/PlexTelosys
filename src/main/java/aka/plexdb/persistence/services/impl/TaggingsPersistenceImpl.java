package aka.plexdb.persistence.services.impl;

import javax.persistence.Query;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;

import aka.plexdb.bean.TaggingsEntity;
import aka.plexdb.persistence.commons.GenericJPAService;
import aka.plexdb.persistence.commons.JPAOperation;
import aka.plexdb.persistence.finders.taggings.TaggingsCountAll;
import aka.plexdb.persistence.services.TaggingsPersistence;

/**
 * JPA implementation for basic persistence operations ( entity "Taggings" ).
 */
public final class TaggingsPersistenceImpl extends GenericJPAService<@NonNull TaggingsEntity, @NonNull Integer> implements TaggingsPersistence {

	/**
	 * Constructor.
	 */
	public TaggingsPersistenceImpl() {
		super(TaggingsEntity.class);
	}

	@Override
    @Nullable 
	public TaggingsEntity load(final @NonNull Integer id) {
		return super.load(id);
	}

	@Override
	public boolean delete(final @NonNull Integer id) {
		return super.delete(id);
	}

	@Override
	public boolean delete(final @NonNull TaggingsEntity entity) {
        boolean result = false;
        Integer key = entity.getId();
        if (key != null) {
            result = super.delete(key);
        }
		return result;
	}

    @Override
    public long countAll() {
        final JPAOperation operation = em -> {
            final TaggingsCountAll finder = new TaggingsCountAll();
            final Query query = finder.getBuildedQuery(em);
            return query.getSingleResult();
        };

        final Long value = (Long) execute(operation);
        long result = 0;
        if (value != null) {
            result = value.longValue();
        }

        return result;
    }
}
