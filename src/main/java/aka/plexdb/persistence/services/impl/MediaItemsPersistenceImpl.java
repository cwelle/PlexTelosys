package aka.plexdb.persistence.services.impl;

import javax.persistence.Query;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;

import aka.plexdb.bean.MediaItemsEntity;
import aka.plexdb.persistence.commons.GenericJPAService;
import aka.plexdb.persistence.commons.JPAOperation;
import aka.plexdb.persistence.finders.mediaitems.MediaItemsCountAll;
import aka.plexdb.persistence.services.MediaItemsPersistence;

/**
 * JPA implementation for basic persistence operations ( entity "MediaItems" ).
 */
public final class MediaItemsPersistenceImpl extends GenericJPAService<@NonNull MediaItemsEntity, @NonNull Integer> implements MediaItemsPersistence {

	/**
	 * Constructor.
	 */
	public MediaItemsPersistenceImpl() {
		super(MediaItemsEntity.class);
	}

	@Override
    @Nullable 
	public MediaItemsEntity load(final @NonNull Integer id) {
		return super.load(id);
	}

	@Override
	public boolean delete(final @NonNull Integer id) {
		return super.delete(id);
	}

	@Override
	public boolean delete(final @NonNull MediaItemsEntity entity) {
        boolean result = false;
        Integer key = entity.getId();
        if (key != null) {
            result = super.delete(key);
        }
		return result;
	}

    @Override
    public long countAll() {
        final JPAOperation operation = em -> {
            final MediaItemsCountAll finder = new MediaItemsCountAll();
            final Query query = finder.getBuildedQuery(em);
            return query.getSingleResult();
        };

        final Long value = (Long) execute(operation);
        long result = 0;
        if (value != null) {
            result = value.longValue();
        }

        return result;
    }
}
