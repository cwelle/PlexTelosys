package aka.plexdb.persistence.services.impl;

import javax.persistence.Query;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;

import aka.plexdb.bean.MediaPartsEntity;
import aka.plexdb.persistence.commons.GenericJPAService;
import aka.plexdb.persistence.commons.JPAOperation;
import aka.plexdb.persistence.finders.mediaparts.MediaPartsCountAll;
import aka.plexdb.persistence.services.MediaPartsPersistence;

/**
 * JPA implementation for basic persistence operations ( entity "MediaParts" ).
 */
public final class MediaPartsPersistenceImpl extends GenericJPAService<@NonNull MediaPartsEntity, @NonNull Integer> implements MediaPartsPersistence {

	/**
	 * Constructor.
	 */
	public MediaPartsPersistenceImpl() {
		super(MediaPartsEntity.class);
	}

	@Override
    @Nullable 
	public MediaPartsEntity load(final @NonNull Integer id) {
		return super.load(id);
	}

	@Override
	public boolean delete(final @NonNull Integer id) {
		return super.delete(id);
	}

	@Override
	public boolean delete(final @NonNull MediaPartsEntity entity) {
        boolean result = false;
        Integer key = entity.getId();
        if (key != null) {
            result = super.delete(key);
        }
		return result;
	}

    @Override
    public long countAll() {
        final JPAOperation operation = em -> {
            final MediaPartsCountAll finder = new MediaPartsCountAll();
            final Query query = finder.getBuildedQuery(em);
            return query.getSingleResult();
        };

        final Long value = (Long) execute(operation);
        long result = 0;
        if (value != null) {
            result = value.longValue();
        }

        return result;
    }
}
