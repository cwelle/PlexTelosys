package aka.plexdb.persistence.services.impl;

import javax.persistence.Query;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;

import aka.plexdb.bean.MetadataItemViewsEntity;
import aka.plexdb.persistence.commons.GenericJPAService;
import aka.plexdb.persistence.commons.JPAOperation;
import aka.plexdb.persistence.finders.metadataitemviews.MetadataItemViewsCountAll;
import aka.plexdb.persistence.services.MetadataItemViewsPersistence;

/**
 * JPA implementation for basic persistence operations ( entity "MetadataItemViews" ).
 */
public final class MetadataItemViewsPersistenceImpl extends GenericJPAService<@NonNull MetadataItemViewsEntity, @NonNull Integer> implements MetadataItemViewsPersistence {

	/**
	 * Constructor.
	 */
	public MetadataItemViewsPersistenceImpl() {
		super(MetadataItemViewsEntity.class);
	}

	@Override
    @Nullable 
	public MetadataItemViewsEntity load(final @NonNull Integer id) {
		return super.load(id);
	}

	@Override
	public boolean delete(final @NonNull Integer id) {
		return super.delete(id);
	}

	@Override
	public boolean delete(final @NonNull MetadataItemViewsEntity entity) {
        boolean result = false;
        Integer key = entity.getId();
        if (key != null) {
            result = super.delete(key);
        }
		return result;
	}

    @Override
    public long countAll() {
        final JPAOperation operation = em -> {
            final MetadataItemViewsCountAll finder = new MetadataItemViewsCountAll();
            final Query query = finder.getBuildedQuery(em);
            return query.getSingleResult();
        };

        final Long value = (Long) execute(operation);
        long result = 0;
        if (value != null) {
            result = value.longValue();
        }

        return result;
    }
}
