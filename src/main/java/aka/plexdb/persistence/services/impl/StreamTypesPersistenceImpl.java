package aka.plexdb.persistence.services.impl;

import javax.persistence.Query;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;

import aka.plexdb.bean.StreamTypesEntity;
import aka.plexdb.persistence.commons.GenericJPAService;
import aka.plexdb.persistence.commons.JPAOperation;
import aka.plexdb.persistence.finders.streamtypes.StreamTypesCountAll;
import aka.plexdb.persistence.services.StreamTypesPersistence;

/**
 * JPA implementation for basic persistence operations ( entity "StreamTypes" ).
 */
public final class StreamTypesPersistenceImpl extends GenericJPAService<@NonNull StreamTypesEntity, @NonNull Integer> implements StreamTypesPersistence {

	/**
	 * Constructor.
	 */
	public StreamTypesPersistenceImpl() {
		super(StreamTypesEntity.class);
	}

	@Override
    @Nullable 
	public StreamTypesEntity load(final @NonNull Integer id) {
		return super.load(id);
	}

	@Override
	public boolean delete(final @NonNull Integer id) {
		return super.delete(id);
	}

	@Override
	public boolean delete(final @NonNull StreamTypesEntity entity) {
        boolean result = false;
        Integer key = entity.getId();
        if (key != null) {
            result = super.delete(key);
        }
		return result;
	}

    @Override
    public long countAll() {
        final JPAOperation operation = em -> {
            final StreamTypesCountAll finder = new StreamTypesCountAll();
            final Query query = finder.getBuildedQuery(em);
            return query.getSingleResult();
        };

        final Long value = (Long) execute(operation);
        long result = 0;
        if (value != null) {
            result = value.longValue();
        }

        return result;
    }
}
