package aka.plexdb.persistence.services.impl;

import javax.persistence.Query;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;

import aka.plexdb.bean.MetadataItemsEntity;
import aka.plexdb.persistence.commons.GenericJPAService;
import aka.plexdb.persistence.commons.JPAOperation;
import aka.plexdb.persistence.finders.metadataitems.MetadataItemsCountAll;
import aka.plexdb.persistence.services.MetadataItemsPersistence;

/**
 * JPA implementation for basic persistence operations ( entity "MetadataItems" ).
 */
public final class MetadataItemsPersistenceImpl extends GenericJPAService<@NonNull MetadataItemsEntity, @NonNull Integer> implements MetadataItemsPersistence {

	/**
	 * Constructor.
	 */
	public MetadataItemsPersistenceImpl() {
		super(MetadataItemsEntity.class);
	}

	@Override
    @Nullable 
	public MetadataItemsEntity load(final @NonNull Integer id) {
		return super.load(id);
	}

	@Override
	public boolean delete(final @NonNull Integer id) {
		return super.delete(id);
	}

	@Override
	public boolean delete(final @NonNull MetadataItemsEntity entity) {
        boolean result = false;
        Integer key = entity.getId();
        if (key != null) {
            result = super.delete(key);
        }
		return result;
	}

    @Override
    public long countAll() {
        final JPAOperation operation = em -> {
            final MetadataItemsCountAll finder = new MetadataItemsCountAll();
            final Query query = finder.getBuildedQuery(em);
            return query.getSingleResult();
        };

        final Long value = (Long) execute(operation);
        long result = 0;
        if (value != null) {
            result = value.longValue();
        }

        return result;
    }
}
