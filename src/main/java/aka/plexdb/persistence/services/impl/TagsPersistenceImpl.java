package aka.plexdb.persistence.services.impl;

import javax.persistence.Query;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;

import aka.plexdb.bean.TagsEntity;
import aka.plexdb.persistence.commons.GenericJPAService;
import aka.plexdb.persistence.commons.JPAOperation;
import aka.plexdb.persistence.finders.tags.TagsCountAll;
import aka.plexdb.persistence.services.TagsPersistence;

/**
 * JPA implementation for basic persistence operations ( entity "Tags" ).
 */
public final class TagsPersistenceImpl extends GenericJPAService<@NonNull TagsEntity, @NonNull Integer> implements TagsPersistence {

	/**
	 * Constructor.
	 */
	public TagsPersistenceImpl() {
		super(TagsEntity.class);
	}

	@Override
    @Nullable 
	public TagsEntity load(final @NonNull Integer id) {
		return super.load(id);
	}

	@Override
	public boolean delete(final @NonNull Integer id) {
		return super.delete(id);
	}

	@Override
	public boolean delete(final @NonNull TagsEntity entity) {
        boolean result = false;
        Integer key = entity.getId();
        if (key != null) {
            result = super.delete(key);
        }
		return result;
	}

    @Override
    public long countAll() {
        final JPAOperation operation = em -> {
            final TagsCountAll finder = new TagsCountAll();
            final Query query = finder.getBuildedQuery(em);
            return query.getSingleResult();
        };

        final Long value = (Long) execute(operation);
        long result = 0;
        if (value != null) {
            result = value.longValue();
        }

        return result;
    }
}
