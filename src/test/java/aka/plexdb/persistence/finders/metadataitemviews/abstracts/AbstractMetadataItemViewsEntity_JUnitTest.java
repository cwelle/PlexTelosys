package aka.plexdb.persistence.finders.metadataitemviews.abstracts;

import java.util.Date;

import java.time.Instant;

import org.eclipse.jdt.annotation.NonNull;

import aka.plexdb.bean.MetadataItemViewsEntity;

/**
 * JUnitTest for AbstractMetadataItemViewsEntity.
 */
public class AbstractMetadataItemViewsEntity_JUnitTest extends AbstractBean_JUnitTest {

    /**
     * MetadataItemViewsEntity.
     */
    @NonNull
    public final MetadataItemViewsEntity metadataItemViewsEntity = new MetadataItemViewsEntity();

    /**
     * Test save.
     */
    @org.junit.Before
    public void before() {
        // Primary key
        final Integer id = Integer.valueOf(1);
        // Fields
        final String guid = "guid";
        final Integer metadataType = Integer.valueOf(1);
        final String grandparentTitle = "grandparentTitle";
        final Integer parentIndex = Integer.valueOf(1);
        final String parentTitle = "parentTitle";
        final String title = "title";
        final String thumbUrl = "thumbUrl";
        final Date viewedAt = Date.from(Instant.now());
        final String grandparentGuid = "grandparentGuid";
        final Date originallyAvailableAt = Date.from(Instant.now());

        this.metadataItemViewsEntity.setId(id);
        this.metadataItemViewsEntity.setGuid(guid);
        this.metadataItemViewsEntity.setMetadataType(metadataType);
        this.metadataItemViewsEntity.setGrandparentTitle(grandparentTitle);
        this.metadataItemViewsEntity.setParentIndex(parentIndex);
        this.metadataItemViewsEntity.setParentTitle(parentTitle);
        this.metadataItemViewsEntity.setTitle(title);
        this.metadataItemViewsEntity.setThumbUrl(thumbUrl);
        this.metadataItemViewsEntity.setViewedAt(viewedAt);
        this.metadataItemViewsEntity.setGrandparentGuid(grandparentGuid);
        this.metadataItemViewsEntity.setOriginallyAvailableAt(originallyAvailableAt);
    }
}
