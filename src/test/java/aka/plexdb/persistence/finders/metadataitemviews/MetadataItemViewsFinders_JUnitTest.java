package aka.plexdb.persistence.finders.metadataitemviews;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.eclipse.jdt.annotation.NonNull;

import aka.plexdb.bean.MetadataItemViewsEntity;
import aka.plexdb.bean.abstracts.AbstractMetadataItemViewsEntity_JUnitTest;
import aka.plexdb.persistence.PersistenceServiceProvider;
import aka.plexdb.persistence.services.MetadataItemViewsPersistence;

/**
 * JUnitTest for MetadataItemViewsEntity.
 */
public class MetadataItemViewsFinders_JUnitTest extends AbstractMetadataItemViewsEntity_JUnitTest {

    /**
     * Test Count all.
     */
    @org.junit.Test
    public void testMetadataItemViewsEntityCountAll() {
        final MetadataItemViewsPersistence metadataItemViewsPersistence = PersistenceServiceProvider.getService(MetadataItemViewsPersistence.class);

        // Setup valid object metadataItemViewsEntity

        metadataItemViewsPersistence.save(this.metadataItemViewsEntity);
        final Integer id = this.metadataItemViewsEntity.getId();
        if (id != null) {

            final long result = metadataItemViewsPersistence.countAll();
            assertEquals(1, result);
        }
    }

    /**
     * Test Find all.
     */
    @org.junit.Test
    public void testMetadataItemViewsEntityFindAll() {
        final MetadataItemViewsPersistence metadataItemViewsPersistence = PersistenceServiceProvider.getService(MetadataItemViewsPersistence.class);

        // Setup valid object metadataItemViewsEntity

        metadataItemViewsPersistence.save(this.metadataItemViewsEntity);
        final Integer id = this.metadataItemViewsEntity.getId();
        if (id != null) {

            final MetadataItemViewsFindAll finder = new MetadataItemViewsFindAll();
            final List<@NonNull MetadataItemViewsEntity> results = metadataItemViewsPersistence.executeQuery(finder);

            assertTrue(results.size() == 1);
        }
    }
}
