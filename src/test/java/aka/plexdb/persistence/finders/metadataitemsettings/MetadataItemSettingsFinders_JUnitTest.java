package aka.plexdb.persistence.finders.metadataitemsettings;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.eclipse.jdt.annotation.NonNull;

import aka.plexdb.bean.MetadataItemSettingsEntity;
import aka.plexdb.bean.abstracts.AbstractMetadataItemSettingsEntity_JUnitTest;
import aka.plexdb.persistence.PersistenceServiceProvider;
import aka.plexdb.persistence.services.MetadataItemSettingsPersistence;

/**
 * JUnitTest for MetadataItemSettingsEntity.
 */
public class MetadataItemSettingsFinders_JUnitTest extends AbstractMetadataItemSettingsEntity_JUnitTest {

    /**
     * Test Count all.
     */
    @org.junit.Test
    public void testMetadataItemSettingsEntityCountAll() {
        final MetadataItemSettingsPersistence metadataItemSettingsPersistence = PersistenceServiceProvider.getService(MetadataItemSettingsPersistence.class);

        // Setup valid object metadataItemSettingsEntity

        metadataItemSettingsPersistence.save(this.metadataItemSettingsEntity);
        final Integer id = this.metadataItemSettingsEntity.getId();
        if (id != null) {

            final long result = metadataItemSettingsPersistence.countAll();
            assertEquals(1, result);
        }
    }

    /**
     * Test Find all.
     */
    @org.junit.Test
    public void testMetadataItemSettingsEntityFindAll() {
        final MetadataItemSettingsPersistence metadataItemSettingsPersistence = PersistenceServiceProvider.getService(MetadataItemSettingsPersistence.class);

        // Setup valid object metadataItemSettingsEntity

        metadataItemSettingsPersistence.save(this.metadataItemSettingsEntity);
        final Integer id = this.metadataItemSettingsEntity.getId();
        if (id != null) {

            final MetadataItemSettingsFindAll finder = new MetadataItemSettingsFindAll();
            final List<@NonNull MetadataItemSettingsEntity> results = metadataItemSettingsPersistence.executeQuery(finder);

            assertTrue(results.size() == 1);
        }
    }
}
