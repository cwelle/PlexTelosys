package aka.plexdb.persistence.finders.mediaparts.abstracts;

import java.util.Date;

import java.time.Instant;

import org.eclipse.jdt.annotation.NonNull;

import aka.plexdb.bean.MediaPartsEntity;

/**
 * JUnitTest for AbstractMediaPartsEntity.
 */
public class AbstractMediaPartsEntity_JUnitTest extends AbstractBean_JUnitTest {

    /**
     * MediaPartsEntity.
     */
    @NonNull
    public final MediaPartsEntity mediaPartsEntity = new MediaPartsEntity();

    /**
     * Test save.
     */
    @org.junit.Before
    public void before() {
        // Primary key
        final Integer id = Integer.valueOf(1);
        // Fields
        final String hash = "hash";
        final String openSubtitleHash = "openSubtitleHash";
        final String file = "file";
        final Integer sizePart = Integer.valueOf(1);
        final Integer duration = Integer.valueOf(1);
        final Date createdAt = Date.from(Instant.now());
        final Date updatedAt = Date.from(Instant.now());
        final Date deletedAt = Date.from(Instant.now());
        final String extraData = "extraData";

        this.mediaPartsEntity.setId(id);
        this.mediaPartsEntity.setHash(hash);
        this.mediaPartsEntity.setOpenSubtitleHash(openSubtitleHash);
        this.mediaPartsEntity.setFile(file);
        this.mediaPartsEntity.setSizePart(sizePart);
        this.mediaPartsEntity.setDuration(duration);
        this.mediaPartsEntity.setCreatedAt(createdAt);
        this.mediaPartsEntity.setUpdatedAt(updatedAt);
        this.mediaPartsEntity.setDeletedAt(deletedAt);
        this.mediaPartsEntity.setExtraData(extraData);
    }
}
