package aka.plexdb.persistence.finders.mediaparts;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.eclipse.jdt.annotation.NonNull;

import aka.plexdb.bean.DirectoriesEntity;
import aka.plexdb.bean.MediaItemsEntity;
import aka.plexdb.bean.MediaPartsEntity;
import aka.plexdb.bean.abstracts.AbstractMediaPartsEntity_JUnitTest;
import aka.plexdb.persistence.PersistenceServiceProvider;
import aka.plexdb.persistence.services.DirectoriesPersistence;
import aka.plexdb.persistence.services.MediaItemsPersistence;
import aka.plexdb.persistence.services.MediaPartsPersistence;

/**
 * JUnitTest for MediaPartsEntity.
 */
public class MediaPartsFinders_JUnitTest extends AbstractMediaPartsEntity_JUnitTest {

    /**
     * Test Count all.
     */
    @org.junit.Test
    public void testMediaPartsEntityCountAll() {
        final MediaPartsPersistence mediaPartsPersistence = PersistenceServiceProvider.getService(MediaPartsPersistence.class);

        // Setup valid object mediaPartsEntity
        final DirectoriesEntity directoriesEntity = new DirectoriesEntity();
        directoriesEntity.setId(Integer.valueOf(1));
        final DirectoriesPersistence directoriesPersistence = PersistenceServiceProvider.getService(DirectoriesPersistence.class);
        directoriesPersistence.save(directoriesEntity);
        this.mediaPartsEntity.setDirectory(directoriesEntity);

        final MediaItemsEntity mediaItemsEntity = new MediaItemsEntity();
        mediaItemsEntity.setId(Integer.valueOf(1));
        final MediaItemsPersistence mediaItemsPersistence = PersistenceServiceProvider.getService(MediaItemsPersistence.class);
        mediaItemsPersistence.save(mediaItemsEntity);
        this.mediaPartsEntity.setMediaItem(mediaItemsEntity);

        mediaPartsPersistence.save(this.mediaPartsEntity);
        final Integer id = this.mediaPartsEntity.getId();
        if (id != null) {

            final long result = mediaPartsPersistence.countAll();
            assertEquals(1, result);
        }
    }

    /**
     * Test Find all.
     */
    @org.junit.Test
    public void testMediaPartsEntityFindAll() {
        final MediaPartsPersistence mediaPartsPersistence = PersistenceServiceProvider.getService(MediaPartsPersistence.class);

        // Setup valid object mediaPartsEntity
        final DirectoriesEntity directoriesEntity = new DirectoriesEntity();
        directoriesEntity.setId(Integer.valueOf(1));
        final DirectoriesPersistence directoriesPersistence = PersistenceServiceProvider.getService(DirectoriesPersistence.class);
        directoriesPersistence.save(directoriesEntity);
        this.mediaPartsEntity.setDirectory(directoriesEntity);

        final MediaItemsEntity mediaItemsEntity = new MediaItemsEntity();
        mediaItemsEntity.setId(Integer.valueOf(1));
        final MediaItemsPersistence mediaItemsPersistence = PersistenceServiceProvider.getService(MediaItemsPersistence.class);
        mediaItemsPersistence.save(mediaItemsEntity);
        this.mediaPartsEntity.setMediaItem(mediaItemsEntity);

        mediaPartsPersistence.save(this.mediaPartsEntity);
        final Integer id = this.mediaPartsEntity.getId();
        if (id != null) {

            final MediaPartsFindAll finder = new MediaPartsFindAll();
            final List<@NonNull MediaPartsEntity> results = mediaPartsPersistence.executeQuery(finder);

            assertTrue(results.size() == 1);
        }
    }
}
