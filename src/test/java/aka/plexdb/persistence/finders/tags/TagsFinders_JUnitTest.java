package aka.plexdb.persistence.finders.tags;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.eclipse.jdt.annotation.NonNull;

import aka.plexdb.bean.MetadataItemsEntity;
import aka.plexdb.bean.TagsEntity;
import aka.plexdb.bean.abstracts.AbstractTagsEntity_JUnitTest;
import aka.plexdb.persistence.PersistenceServiceProvider;
import aka.plexdb.persistence.services.MetadataItemsPersistence;
import aka.plexdb.persistence.services.TagsPersistence;

/**
 * JUnitTest for TagsEntity.
 */
public class TagsFinders_JUnitTest extends AbstractTagsEntity_JUnitTest {

    /**
     * Test Count all.
     */
    @org.junit.Test
    public void testTagsEntityCountAll() {
        final TagsPersistence tagsPersistence = PersistenceServiceProvider.getService(TagsPersistence.class);

        // Setup valid object mediaPartsEntity
        final MetadataItemsEntity metadataItemsEntity = new MetadataItemsEntity();
        metadataItemsEntity.setId(Integer.valueOf(1));
        final MetadataItemsPersistence metadataItemsPersistence = PersistenceServiceProvider.getService(MetadataItemsPersistence.class);
        metadataItemsPersistence.save(metadataItemsEntity);
        this.tagsEntity.setMetadataItem(metadataItemsEntity);

        tagsPersistence.save(this.tagsEntity);
        final Integer id = this.tagsEntity.getId();
        if (id != null) {

            final long result = tagsPersistence.countAll();
            assertEquals(1, result);
        }
    }

    /**
     * Test Find all.
     */
    @org.junit.Test
    public void testTagsEntityFindAll() {
        final TagsPersistence tagsPersistence = PersistenceServiceProvider.getService(TagsPersistence.class);

        // Setup valid object mediaPartsEntity
        final MetadataItemsEntity metadataItemsEntity = new MetadataItemsEntity();
        metadataItemsEntity.setId(Integer.valueOf(1));
        final MetadataItemsPersistence metadataItemsPersistence = PersistenceServiceProvider.getService(MetadataItemsPersistence.class);
        metadataItemsPersistence.save(metadataItemsEntity);
        this.tagsEntity.setMetadataItem(metadataItemsEntity);

        tagsPersistence.save(this.tagsEntity);
        final Integer id = this.tagsEntity.getId();
        if (id != null) {

            final TagsFindAll finder = new TagsFindAll();
            final List<@NonNull TagsEntity> results = tagsPersistence.executeQuery(finder);

            assertTrue(results.size() == 1);
        }
    }
}
