package aka.plexdb.persistence.finders.mediaitems.abstracts;

import java.sql.SQLException;

import org.hibernate.exception.JDBCConnectionException;
import org.hibernate.tool.schema.spi.SchemaManagementException;
import org.junit.AfterClass;
import org.junit.BeforeClass;

import aka.plexdb.persistence.commons.JPAEnvironments;

/**
 * Abstract class for Bean JUnitTest.
 *
 * @author Cha
 */
public class AbstractBean_JUnitTest {

    /**
     * Init.
     * 
     * @throws SchemaManagementException if schema does not correspond
     * @throws JDBCConnectionException if not a database file
     * @throws SQLException if a database access error occurs or the URL is null
     */
    @BeforeClass
    public static void beforeClass() throws JDBCConnectionException, SchemaManagementException, SQLException {
        JPAEnvironments.getInstance().setCurrentJPAEnvironment("mem:JUnitTest");
    }

    /**
     * Deinit.
     */
    @AfterClass
    public static void afterClass() {
        JPAEnvironments.getInstance().destroy();
    }

}
