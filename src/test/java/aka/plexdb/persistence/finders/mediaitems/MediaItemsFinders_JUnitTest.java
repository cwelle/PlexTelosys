package aka.plexdb.persistence.finders.mediaitems;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.eclipse.jdt.annotation.NonNull;

import aka.plexdb.bean.MediaItemsEntity;
import aka.plexdb.bean.abstracts.AbstractMediaItemsEntity_JUnitTest;
import aka.plexdb.persistence.PersistenceServiceProvider;
import aka.plexdb.persistence.services.MediaItemsPersistence;

/**
 * JUnitTest for MediaItemsEntity.
 */
public class MediaItemsFinders_JUnitTest extends AbstractMediaItemsEntity_JUnitTest {

    /**
     * Test Count all.
     */
    @org.junit.Test
    public void testMediaItemsEntityCountAll() {
        final MediaItemsPersistence mediaItemsPersistence = PersistenceServiceProvider.getService(MediaItemsPersistence.class);

        // Setup valid object mediaItemsEntity

        mediaItemsPersistence.save(this.mediaItemsEntity);
        final Integer id = this.mediaItemsEntity.getId();
        if (id != null) {

            final long result = mediaItemsPersistence.countAll();
            assertEquals(1, result);
        }
    }

    /**
     * Test Find all.
     */
    @org.junit.Test
    public void testMediaItemsEntityFindAll() {
        final MediaItemsPersistence mediaItemsPersistence = PersistenceServiceProvider.getService(MediaItemsPersistence.class);

        // Setup valid object mediaItemsEntity

        mediaItemsPersistence.save(this.mediaItemsEntity);
        final Integer id = this.mediaItemsEntity.getId();
        if (id != null) {

            final MediaItemsFindAll finder = new MediaItemsFindAll();
            final List<@NonNull MediaItemsEntity> results = mediaItemsPersistence.executeQuery(finder);

            assertTrue(results.size() == 1);
        }
    }
}
