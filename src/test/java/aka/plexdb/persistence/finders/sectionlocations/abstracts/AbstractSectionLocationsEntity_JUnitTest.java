package aka.plexdb.persistence.finders.sectionlocations.abstracts;

import java.util.Date;

import java.time.Instant;

import org.eclipse.jdt.annotation.NonNull;

import aka.plexdb.bean.SectionLocationsEntity;

/**
 * JUnitTest for AbstractSectionLocationsEntity.
 */
public class AbstractSectionLocationsEntity_JUnitTest extends AbstractBean_JUnitTest {

    /**
     * SectionLocationsEntity.
     */
    @NonNull
    public final SectionLocationsEntity sectionLocationsEntity = new SectionLocationsEntity();

    /**
     * Test save.
     */
    @org.junit.Before
    public void before() {
        // Primary key
        final Integer id = Integer.valueOf(1);
        // Fields
        final String rootPath = "rootPath";
        final Boolean available = new Boolean("'t'");
        final Date scannedAt = Date.from(Instant.now());
        final Date createdAt = Date.from(Instant.now());
        final Date updatedAt = Date.from(Instant.now());

        this.sectionLocationsEntity.setId(id);
        this.sectionLocationsEntity.setRootPath(rootPath);
        this.sectionLocationsEntity.setAvailable(available);
        this.sectionLocationsEntity.setScannedAt(scannedAt);
        this.sectionLocationsEntity.setCreatedAt(createdAt);
        this.sectionLocationsEntity.setUpdatedAt(updatedAt);
    }
}
