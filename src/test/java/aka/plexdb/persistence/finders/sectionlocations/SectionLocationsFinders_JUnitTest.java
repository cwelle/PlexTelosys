package aka.plexdb.persistence.finders.sectionlocations;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.eclipse.jdt.annotation.NonNull;

import aka.plexdb.bean.SectionLocationsEntity;
import aka.plexdb.bean.abstracts.AbstractSectionLocationsEntity_JUnitTest;
import aka.plexdb.persistence.PersistenceServiceProvider;
import aka.plexdb.persistence.services.SectionLocationsPersistence;

/**
 * JUnitTest for SectionLocationsEntity.
 */
public class SectionLocationsFinders_JUnitTest extends AbstractSectionLocationsEntity_JUnitTest {

    /**
     * Test Count all.
     */
    @org.junit.Test
    public void testSectionLocationsEntityCountAll() {
        final SectionLocationsPersistence sectionLocationsPersistence = PersistenceServiceProvider.getService(SectionLocationsPersistence.class);

        // Setup valid object sectionLocationsEntity

        sectionLocationsPersistence.save(this.sectionLocationsEntity);
        final Integer id = this.sectionLocationsEntity.getId();
        if (id != null) {

            final long result = sectionLocationsPersistence.countAll();
            assertEquals(1, result);
        }
    }

    /**
     * Test Find all.
     */
    @org.junit.Test
    public void testSectionLocationsEntityFindAll() {
        final SectionLocationsPersistence sectionLocationsPersistence = PersistenceServiceProvider.getService(SectionLocationsPersistence.class);

        // Setup valid object sectionLocationsEntity

        sectionLocationsPersistence.save(this.sectionLocationsEntity);
        final Integer id = this.sectionLocationsEntity.getId();
        if (id != null) {

            final SectionLocationsFindAll finder = new SectionLocationsFindAll();
            final List<@NonNull SectionLocationsEntity> results = sectionLocationsPersistence.executeQuery(finder);

            assertTrue(results.size() == 1);
        }
    }
}
