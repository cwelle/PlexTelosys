package aka.plexdb.persistence.finders.mediastreams.abstracts;

import java.util.Date;

import java.time.Instant;

import org.eclipse.jdt.annotation.NonNull;

import aka.plexdb.bean.MediaStreamsEntity;

/**
 * JUnitTest for AbstractMediaStreamsEntity.
 */
public class AbstractMediaStreamsEntity_JUnitTest extends AbstractBean_JUnitTest {

    /**
     * MediaStreamsEntity.
     */
    @NonNull
    public final MediaStreamsEntity mediaStreamsEntity = new MediaStreamsEntity();

    /**
     * Test save.
     */
    @org.junit.Before
    public void before() {
        // Primary key
        final Integer id = Integer.valueOf(1);
        // Fields
        final String url = "url";
        final String codec = "codec";
        final String language = "language";
        final Date createdAt = Date.from(Instant.now());
        final Date updatedAt = Date.from(Instant.now());
        final Integer channels = Integer.valueOf(1);
        final Integer bitrate = Integer.valueOf(1);
        final Integer urlIndex = Integer.valueOf(1);
        final Boolean isDefault = new Boolean("0");
        final Boolean isForced = new Boolean("0");
        final String extraData = "extraData";

        this.mediaStreamsEntity.setId(id);
        this.mediaStreamsEntity.setUrl(url);
        this.mediaStreamsEntity.setCodec(codec);
        this.mediaStreamsEntity.setLanguage(language);
        this.mediaStreamsEntity.setCreatedAt(createdAt);
        this.mediaStreamsEntity.setUpdatedAt(updatedAt);
        this.mediaStreamsEntity.setChannels(channels);
        this.mediaStreamsEntity.setBitrate(bitrate);
        this.mediaStreamsEntity.setUrlIndex(urlIndex);
        this.mediaStreamsEntity.setIsDefault(isDefault);
        this.mediaStreamsEntity.setIsForced(isForced);
        this.mediaStreamsEntity.setExtraData(extraData);
    }
}
