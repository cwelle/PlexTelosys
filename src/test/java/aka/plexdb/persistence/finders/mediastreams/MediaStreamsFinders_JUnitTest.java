package aka.plexdb.persistence.finders.mediastreams;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.eclipse.jdt.annotation.NonNull;

import aka.plexdb.bean.MediaStreamsEntity;
import aka.plexdb.bean.abstracts.AbstractMediaStreamsEntity_JUnitTest;
import aka.plexdb.persistence.PersistenceServiceProvider;
import aka.plexdb.persistence.services.MediaStreamsPersistence;

/**
 * JUnitTest for MediaStreamsEntity.
 */
public class MediaStreamsFinders_JUnitTest extends AbstractMediaStreamsEntity_JUnitTest {

    /**
     * Test Count all.
     */
    @org.junit.Test
    public void testMediaStreamsEntityCountAll() {
        final MediaStreamsPersistence mediaStreamsPersistence = PersistenceServiceProvider.getService(MediaStreamsPersistence.class);

        // Setup valid object mediaStreamsEntity

        mediaStreamsPersistence.save(this.mediaStreamsEntity);
        final Integer id = this.mediaStreamsEntity.getId();
        if (id != null) {

            final long result = mediaStreamsPersistence.countAll();
            assertEquals(1, result);
        }
    }

    /**
     * Test Find all.
     */
    @org.junit.Test
    public void testMediaStreamsEntityFindAll() {
        final MediaStreamsPersistence mediaStreamsPersistence = PersistenceServiceProvider.getService(MediaStreamsPersistence.class);

        // Setup valid object mediaStreamsEntity

        mediaStreamsPersistence.save(this.mediaStreamsEntity);
        final Integer id = this.mediaStreamsEntity.getId();
        if (id != null) {

            final MediaStreamsFindAll finder = new MediaStreamsFindAll();
            final List<@NonNull MediaStreamsEntity> results = mediaStreamsPersistence.executeQuery(finder);

            assertTrue(results.size() == 1);
        }
    }
}
