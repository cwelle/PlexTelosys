package aka.plexdb.persistence.finders.librarysections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.eclipse.jdt.annotation.NonNull;

import aka.plexdb.bean.LibrarySectionsEntity;
import aka.plexdb.bean.abstracts.AbstractLibrarySectionsEntity_JUnitTest;
import aka.plexdb.persistence.PersistenceServiceProvider;
import aka.plexdb.persistence.services.LibrarySectionsPersistence;

/**
 * JUnitTest for LibrarySectionsEntity.
 */
public class LibrarySectionsFinders_JUnitTest extends AbstractLibrarySectionsEntity_JUnitTest {

    /**
     * Test Count all.
     */
    @org.junit.Test
    public void testLibrarySectionsEntityCountAll() {
        final LibrarySectionsPersistence librarySectionsPersistence = PersistenceServiceProvider.getService(LibrarySectionsPersistence.class);

        // Setup valid object librarySectionsEntity

        librarySectionsPersistence.save(this.librarySectionsEntity);
        final Integer id = this.librarySectionsEntity.getId();
        if (id != null) {

            final long result = librarySectionsPersistence.countAll();
            assertEquals(1, result);
        }
    }

    /**
     * Test Find all.
     */
    @org.junit.Test
    public void testLibrarySectionsEntityFindAll() {
        final LibrarySectionsPersistence librarySectionsPersistence = PersistenceServiceProvider.getService(LibrarySectionsPersistence.class);

        // Setup valid object librarySectionsEntity

        librarySectionsPersistence.save(this.librarySectionsEntity);
        final Integer id = this.librarySectionsEntity.getId();
        if (id != null) {

            final LibrarySectionsFindAll finder = new LibrarySectionsFindAll();
            final List<@NonNull LibrarySectionsEntity> results = librarySectionsPersistence.executeQuery(finder);

            assertTrue(results.size() == 1);
        }
    }
}
