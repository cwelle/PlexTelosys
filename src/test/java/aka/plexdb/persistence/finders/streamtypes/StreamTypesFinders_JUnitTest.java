package aka.plexdb.persistence.finders.streamtypes;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.eclipse.jdt.annotation.NonNull;

import aka.plexdb.bean.StreamTypesEntity;
import aka.plexdb.bean.abstracts.AbstractStreamTypesEntity_JUnitTest;
import aka.plexdb.persistence.PersistenceServiceProvider;
import aka.plexdb.persistence.services.StreamTypesPersistence;

/**
 * JUnitTest for StreamTypesEntity.
 */
public class StreamTypesFinders_JUnitTest extends AbstractStreamTypesEntity_JUnitTest {

    /**
     * Test Count all.
     */
    @org.junit.Test
    public void testStreamTypesEntityCountAll() {
        final StreamTypesPersistence streamTypesPersistence = PersistenceServiceProvider.getService(StreamTypesPersistence.class);

        // Setup valid object streamTypesEntity

        streamTypesPersistence.save(this.streamTypesEntity);
        final Integer id = this.streamTypesEntity.getId();
        if (id != null) {

            final long result = streamTypesPersistence.countAll();
            assertEquals(1, result);
        }
    }

    /**
     * Test Find all.
     */
    @org.junit.Test
    public void testStreamTypesEntityFindAll() {
        final StreamTypesPersistence streamTypesPersistence = PersistenceServiceProvider.getService(StreamTypesPersistence.class);

        // Setup valid object streamTypesEntity

        streamTypesPersistence.save(this.streamTypesEntity);
        final Integer id = this.streamTypesEntity.getId();
        if (id != null) {

            final StreamTypesFindAll finder = new StreamTypesFindAll();
            final List<@NonNull StreamTypesEntity> results = streamTypesPersistence.executeQuery(finder);

            assertTrue(results.size() == 1);
        }
    }
}
