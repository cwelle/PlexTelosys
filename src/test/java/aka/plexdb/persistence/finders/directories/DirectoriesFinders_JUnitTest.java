package aka.plexdb.persistence.finders.directories;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.eclipse.jdt.annotation.NonNull;

import aka.plexdb.bean.DirectoriesEntity;
import aka.plexdb.bean.abstracts.AbstractDirectoriesEntity_JUnitTest;
import aka.plexdb.persistence.PersistenceServiceProvider;
import aka.plexdb.persistence.services.DirectoriesPersistence;

/**
 * JUnitTest for DirectoriesEntity.
 */
public class DirectoriesFinders_JUnitTest extends AbstractDirectoriesEntity_JUnitTest {

    /**
     * Test Count all.
     */
    @org.junit.Test
    public void testDirectoriesEntityCountAll() {
        final DirectoriesPersistence directoriesPersistence = PersistenceServiceProvider.getService(DirectoriesPersistence.class);

        // Setup valid object directoriesEntity

        directoriesPersistence.save(this.directoriesEntity);
        final Integer id = this.directoriesEntity.getId();
        if (id != null) {

            final long result = directoriesPersistence.countAll();
            assertEquals(1, result);
        }
    }

    /**
     * Test Find all.
     */
    @org.junit.Test
    public void testDirectoriesEntityFindAll() {
        final DirectoriesPersistence directoriesPersistence = PersistenceServiceProvider.getService(DirectoriesPersistence.class);

        // Setup valid object directoriesEntity

        directoriesPersistence.save(this.directoriesEntity);
        final Integer id = this.directoriesEntity.getId();
        if (id != null) {

            final DirectoriesFindAll finder = new DirectoriesFindAll();
            final List<@NonNull DirectoriesEntity> results = directoriesPersistence.executeQuery(finder);

            assertTrue(results.size() == 1);
        }
    }
}
