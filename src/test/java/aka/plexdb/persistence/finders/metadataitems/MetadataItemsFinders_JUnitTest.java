package aka.plexdb.persistence.finders.metadataitems;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.eclipse.jdt.annotation.NonNull;

import aka.plexdb.bean.MetadataItemsEntity;
import aka.plexdb.bean.abstracts.AbstractMetadataItemsEntity_JUnitTest;
import aka.plexdb.persistence.PersistenceServiceProvider;
import aka.plexdb.persistence.services.MetadataItemsPersistence;

/**
 * JUnitTest for MetadataItemsEntity.
 */
public class MetadataItemsFinders_JUnitTest extends AbstractMetadataItemsEntity_JUnitTest {

    /**
     * Test Count all.
     */
    @org.junit.Test
    public void testMetadataItemsEntityCountAll() {
        final MetadataItemsPersistence metadataItemsPersistence = PersistenceServiceProvider.getService(MetadataItemsPersistence.class);

        // Setup valid object metadataItemsEntity

        metadataItemsPersistence.save(this.metadataItemsEntity);
        final Integer id = this.metadataItemsEntity.getId();
        if (id != null) {

            final long result = metadataItemsPersistence.countAll();
            assertEquals(1, result);
        }
    }

    /**
     * Test Find all.
     */
    @org.junit.Test
    public void testMetadataItemsEntityFindAll() {
        final MetadataItemsPersistence metadataItemsPersistence = PersistenceServiceProvider.getService(MetadataItemsPersistence.class);

        // Setup valid object metadataItemsEntity

        metadataItemsPersistence.save(this.metadataItemsEntity);
        final Integer id = this.metadataItemsEntity.getId();
        if (id != null) {

            final MetadataItemsFindAll finder = new MetadataItemsFindAll();
            final List<@NonNull MetadataItemsEntity> results = metadataItemsPersistence.executeQuery(finder);

            assertTrue(results.size() == 1);
        }
    }
}
