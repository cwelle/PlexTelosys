package aka.plexdb.persistence.finders.taggings;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.eclipse.jdt.annotation.NonNull;

import aka.plexdb.bean.TaggingsEntity;
import aka.plexdb.bean.abstracts.AbstractTaggingsEntity_JUnitTest;
import aka.plexdb.persistence.PersistenceServiceProvider;
import aka.plexdb.persistence.services.TaggingsPersistence;

/**
 * JUnitTest for TaggingsEntity.
 */
public class TaggingsFinders_JUnitTest extends AbstractTaggingsEntity_JUnitTest {

    /**
     * Test Count all.
     */
    @org.junit.Test
    public void testTaggingsEntityCountAll() {
        final TaggingsPersistence taggingsPersistence = PersistenceServiceProvider.getService(TaggingsPersistence.class);

        // Setup valid object taggingsEntity

        taggingsPersistence.save(this.taggingsEntity);
        final Integer id = this.taggingsEntity.getId();
        if (id != null) {

            final long result = taggingsPersistence.countAll();
            assertEquals(1, result);
        }
    }

    /**
     * Test Find all.
     */
    @org.junit.Test
    public void testTaggingsEntityFindAll() {
        final TaggingsPersistence taggingsPersistence = PersistenceServiceProvider.getService(TaggingsPersistence.class);

        // Setup valid object taggingsEntity

        taggingsPersistence.save(this.taggingsEntity);
        final Integer id = this.taggingsEntity.getId();
        if (id != null) {

            final TaggingsFindAll finder = new TaggingsFindAll();
            final List<@NonNull TaggingsEntity> results = taggingsPersistence.executeQuery(finder);

            assertTrue(results.size() == 1);
        }
    }
}
