package aka.plexdb.persistence.commons;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.sql.SQLException;

import org.hibernate.exception.JDBCConnectionException;
import org.hibernate.tool.schema.spi.SchemaManagementException;

/**
 * JUnit tests for JPAEnvironments.
 */
public class JPAEnvironmentsTest_JUnitTest {

    /**
     * Database registered.
     *
     * @throws SchemaManagementException if schema does not correspond
     * @throws JDBCConnectionException if not a database file
     * @throws SQLException if a database access error occurs or the URL is null
     */
    @org.junit.Test
    public void testJPAEnvironmentsTestExist() throws JDBCConnectionException, SchemaManagementException, SQLException {
        JPAEnvironments.getInstance().setCurrentJPAEnvironment("mem:JUnitTest");
        final JPAEnvironment jpaEnvironment = JPAEnvironments.getInstance().getCurrentJPAEnvironment();

        assertNotNull(jpaEnvironment);

        JPAEnvironments.getInstance().destroy();
    }

    /**
     * Database not registered.
     *
     * @throws SchemaManagementException if schema does not correspond
     * @throws JDBCConnectionException if not a database file
     * @throws SQLException if a database access error occurs or the URL is null
     */
    @org.junit.Test
    public void testJPAEnvironmentsTestNotExist() throws JDBCConnectionException, SchemaManagementException, SQLException {
        JPAEnvironments.getInstance().setCurrentJPAEnvironment("mem:JUnitTest");
        JPAEnvironments.getInstance().destroy();

        final JPAEnvironment jpaEnvironment = JPAEnvironments.getInstance().getCurrentJPAEnvironment();

        assertNull(jpaEnvironment);
    }

    /**
     * Not a Plex database.
     *
     * @throws JDBCConnectionException if not a database file
     * @throws SQLException if a database access error occurs or the URL is null
     */
    @org.junit.Test(expected = org.hibernate.tool.schema.spi.SchemaManagementException.class)
    public void testJPAEnvironmentsTestNotAPlexDB() throws JDBCConnectionException, SQLException {
        JPAEnvironments.getInstance().setCurrentJPAEnvironment("./src/test/resources/sqlite.db");

        fail("expected an exception");
    }

    /**
     * Not a database file.
     *
     * @throws SchemaManagementException if schema does not correspond
     * @throws SQLException if a database access error occurs or the URL is null
     */
    @org.junit.Test(expected = org.hibernate.exception.JDBCConnectionException.class)
    public void testJPAEnvironmentsTestNotAPSQLiteDB() throws SchemaManagementException, SQLException {
        JPAEnvironments.getInstance().setCurrentJPAEnvironment("./src/test/resources/notadbfile");

        fail("expected an exception");
    }

    /**
     * Can not connect.
     */
    @org.junit.Test
    public void testJPAEnvironmentsTestCanNotConnect() {
        try {
            JPAEnvironments.getInstance().setCurrentJPAEnvironment("");
            fail("expected an exception");
        } catch (JDBCConnectionException | SchemaManagementException | SQLException e) {
            // Success
        }
    }
}
