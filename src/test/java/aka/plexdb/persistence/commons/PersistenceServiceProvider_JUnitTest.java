package aka.plexdb.persistence.commons;

import static org.junit.Assert.assertNotNull;

import aka.plexdb.persistence.PersistenceServiceProvider;

/**
 * JUnit tests for PersistenceServiceProvider.
 */
public class PersistenceServiceProvider_JUnitTest {

    /**
     * Test for DirectoriesPersistence.
     */
    @org.junit.Test
    public void testDirectoriesPersistence() {
        final aka.plexdb.persistence.services.DirectoriesPersistence directoriesPersistence = PersistenceServiceProvider.getService(aka.plexdb.persistence.services.DirectoriesPersistence.class);

        assertNotNull(directoriesPersistence);
    }

    /**
     * Test for LibrarySectionsPersistence.
     */
    @org.junit.Test
    public void testLibrarySectionsPersistence() {
        final aka.plexdb.persistence.services.LibrarySectionsPersistence librarySectionsPersistence = PersistenceServiceProvider.getService(aka.plexdb.persistence.services.LibrarySectionsPersistence.class);

        assertNotNull(librarySectionsPersistence);
    }

    /**
     * Test for MediaItemsPersistence.
     */
    @org.junit.Test
    public void testMediaItemsPersistence() {
        final aka.plexdb.persistence.services.MediaItemsPersistence mediaItemsPersistence = PersistenceServiceProvider.getService(aka.plexdb.persistence.services.MediaItemsPersistence.class);

        assertNotNull(mediaItemsPersistence);
    }

    /**
     * Test for MediaPartsPersistence.
     */
    @org.junit.Test
    public void testMediaPartsPersistence() {
        final aka.plexdb.persistence.services.MediaPartsPersistence mediaPartsPersistence = PersistenceServiceProvider.getService(aka.plexdb.persistence.services.MediaPartsPersistence.class);

        assertNotNull(mediaPartsPersistence);
    }

    /**
     * Test for MediaStreamsPersistence.
     */
    @org.junit.Test
    public void testMediaStreamsPersistence() {
        final aka.plexdb.persistence.services.MediaStreamsPersistence mediaStreamsPersistence = PersistenceServiceProvider.getService(aka.plexdb.persistence.services.MediaStreamsPersistence.class);

        assertNotNull(mediaStreamsPersistence);
    }

    /**
     * Test for MetadataItemSettingsPersistence.
     */
    @org.junit.Test
    public void testMetadataItemSettingsPersistence() {
        final aka.plexdb.persistence.services.MetadataItemSettingsPersistence metadataItemSettingsPersistence = PersistenceServiceProvider.getService(aka.plexdb.persistence.services.MetadataItemSettingsPersistence.class);

        assertNotNull(metadataItemSettingsPersistence);
    }

    /**
     * Test for MetadataItemViewsPersistence.
     */
    @org.junit.Test
    public void testMetadataItemViewsPersistence() {
        final aka.plexdb.persistence.services.MetadataItemViewsPersistence metadataItemViewsPersistence = PersistenceServiceProvider.getService(aka.plexdb.persistence.services.MetadataItemViewsPersistence.class);

        assertNotNull(metadataItemViewsPersistence);
    }

    /**
     * Test for MetadataItemsPersistence.
     */
    @org.junit.Test
    public void testMetadataItemsPersistence() {
        final aka.plexdb.persistence.services.MetadataItemsPersistence metadataItemsPersistence = PersistenceServiceProvider.getService(aka.plexdb.persistence.services.MetadataItemsPersistence.class);

        assertNotNull(metadataItemsPersistence);
    }

    /**
     * Test for SectionLocationsPersistence.
     */
    @org.junit.Test
    public void testSectionLocationsPersistence() {
        final aka.plexdb.persistence.services.SectionLocationsPersistence sectionLocationsPersistence = PersistenceServiceProvider.getService(aka.plexdb.persistence.services.SectionLocationsPersistence.class);

        assertNotNull(sectionLocationsPersistence);
    }

    /**
     * Test for StreamTypesPersistence.
     */
    @org.junit.Test
    public void testStreamTypesPersistence() {
        final aka.plexdb.persistence.services.StreamTypesPersistence streamTypesPersistence = PersistenceServiceProvider.getService(aka.plexdb.persistence.services.StreamTypesPersistence.class);

        assertNotNull(streamTypesPersistence);
    }

    /**
     * Test for TaggingsPersistence.
     */
    @org.junit.Test
    public void testTaggingsPersistence() {
        final aka.plexdb.persistence.services.TaggingsPersistence taggingsPersistence = PersistenceServiceProvider.getService(aka.plexdb.persistence.services.TaggingsPersistence.class);

        assertNotNull(taggingsPersistence);
    }

    /**
     * Test for TagsPersistence.
     */
    @org.junit.Test
    public void testTagsPersistence() {
        final aka.plexdb.persistence.services.TagsPersistence tagsPersistence = PersistenceServiceProvider.getService(aka.plexdb.persistence.services.TagsPersistence.class);

        assertNotNull(tagsPersistence);
    }

}
