package aka.plexdb.bean;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import aka.plexdb.bean.abstracts.AbstractSectionLocationsEntity_JUnitTest;
import aka.plexdb.persistence.PersistenceServiceProvider;
import aka.plexdb.persistence.services.SectionLocationsPersistence;

/**
 * JUnitTest for SectionLocationsEntity.
 */
public class SectionLocationsEntity_JUnitTest extends AbstractSectionLocationsEntity_JUnitTest {

    /**
     * Test save successful.
     */
    @org.junit.Test
    public void testAbstractSectionLocationsEntitySaveSuccessful() {
        final SectionLocationsPersistence sectionLocationsPersistence = PersistenceServiceProvider.getService(SectionLocationsPersistence.class);

        // Setup valid object sectionLocationsEntity

        sectionLocationsPersistence.save(this.sectionLocationsEntity);
        Integer id = this.sectionLocationsEntity.getId();
        if (id != null) {
            final SectionLocationsEntity savedSectionLocationsEntity = sectionLocationsPersistence.load(id);
            assertNotNull(savedSectionLocationsEntity);
        }
    }

    /**
     * Test save failed.
     */
    @org.junit.Test(expected = javax.persistence.PersistenceException.class)
    public void testAbstractSectionLocationsEntitySaveFailed() {
        final SectionLocationsPersistence sectionLocationsPersistence = PersistenceServiceProvider.getService(SectionLocationsPersistence.class);

        // Setup invalid object sectionLocationsEntity

        this.sectionLocationsEntity.setId(null);
        sectionLocationsPersistence.save(this.sectionLocationsEntity);

        fail("expected an exception");
    }

    /**
     * Test search successful.
     */
    @org.junit.Test
    public void testAbstractSectionLocationsEntitySearchSuccessful() {
        final SectionLocationsPersistence sectionLocationsPersistence = PersistenceServiceProvider.getService(SectionLocationsPersistence.class);

        // Setup valid object sectionLocationsEntity

        sectionLocationsPersistence.save(this.sectionLocationsEntity);
        Integer id = this.sectionLocationsEntity.getId();
        if (id != null) {
            final SectionLocationsEntity savedSectionLocationsEntity = sectionLocationsPersistence.load(id);
            assertNotNull(savedSectionLocationsEntity);

            assertEquals(this.sectionLocationsEntity.getId(), savedSectionLocationsEntity.getId());
            assertEquals(this.sectionLocationsEntity.getRootPath(), savedSectionLocationsEntity.getRootPath());
            assertEquals(this.sectionLocationsEntity.getAvailable(), savedSectionLocationsEntity.getAvailable());
            assertEquals(this.sectionLocationsEntity.getScannedAt(), savedSectionLocationsEntity.getScannedAt());
            assertEquals(this.sectionLocationsEntity.getCreatedAt(), savedSectionLocationsEntity.getCreatedAt());
            assertEquals(this.sectionLocationsEntity.getUpdatedAt(), savedSectionLocationsEntity.getUpdatedAt());
        }
    }

    /**
     * Test search failed.
     */
    @org.junit.Test
    public void testAbstractSectionLocationsEntitySearchFailed() {
        final SectionLocationsPersistence sectionLocationsPersistence = PersistenceServiceProvider.getService(SectionLocationsPersistence.class);

        // Setup invalid object sectionLocationsEntity

        sectionLocationsPersistence.save(this.sectionLocationsEntity);
        final Integer id = Integer.valueOf(-1);
        final SectionLocationsEntity savedSectionLocationsEntity = sectionLocationsPersistence.load(id);

        assertNull(savedSectionLocationsEntity);
    }

    /**
     * Test delete.
     */
    @org.junit.Test
    public void testAbstractSectionLocationsEntityDelete() {
        final SectionLocationsPersistence sectionLocationsPersistence = PersistenceServiceProvider.getService(SectionLocationsPersistence.class);

        // Setup valid object sectionLocationsEntity

        final Integer id = this.sectionLocationsEntity.getId();
        if (id != null) {
            sectionLocationsPersistence.save(this.sectionLocationsEntity);
            final SectionLocationsEntity savedSectionLocationsEntity = sectionLocationsPersistence.load(id);
            assertNotNull(savedSectionLocationsEntity);

            sectionLocationsPersistence.delete(this.sectionLocationsEntity);
            final SectionLocationsEntity deletedSectionLocationsEntity = sectionLocationsPersistence.load(id);
            assertNull(deletedSectionLocationsEntity);
        }
    }
}
