package aka.plexdb.bean;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import aka.plexdb.bean.abstracts.AbstractLibrarySectionsEntity_JUnitTest;
import aka.plexdb.persistence.PersistenceServiceProvider;
import aka.plexdb.persistence.services.LibrarySectionsPersistence;

/**
 * JUnitTest for LibrarySectionsEntity.
 */
public class LibrarySectionsEntity_JUnitTest extends AbstractLibrarySectionsEntity_JUnitTest {

    /**
     * Test save successful.
     */
    @org.junit.Test
    public void testAbstractLibrarySectionsEntitySaveSuccessful() {
        final LibrarySectionsPersistence librarySectionsPersistence = PersistenceServiceProvider.getService(LibrarySectionsPersistence.class);

        // Setup valid object librarySectionsEntity

        librarySectionsPersistence.save(this.librarySectionsEntity);
        Integer id = this.librarySectionsEntity.getId();
        if (id != null) {
            final LibrarySectionsEntity savedLibrarySectionsEntity = librarySectionsPersistence.load(id);
            assertNotNull(savedLibrarySectionsEntity);
        }
    }

    /**
     * Test save failed.
     */
    @org.junit.Test(expected = javax.persistence.PersistenceException.class)
    public void testAbstractLibrarySectionsEntitySaveFailed() {
        final LibrarySectionsPersistence librarySectionsPersistence = PersistenceServiceProvider.getService(LibrarySectionsPersistence.class);

        // Setup invalid object librarySectionsEntity

        this.librarySectionsEntity.setId(null);
        librarySectionsPersistence.save(this.librarySectionsEntity);

        fail("expected an exception");
    }

    /**
     * Test search successful.
     */
    @org.junit.Test
    public void testAbstractLibrarySectionsEntitySearchSuccessful() {
        final LibrarySectionsPersistence librarySectionsPersistence = PersistenceServiceProvider.getService(LibrarySectionsPersistence.class);

        // Setup valid object librarySectionsEntity

        librarySectionsPersistence.save(this.librarySectionsEntity);
        Integer id = this.librarySectionsEntity.getId();
        if (id != null) {
            final LibrarySectionsEntity savedLibrarySectionsEntity = librarySectionsPersistence.load(id);
            assertNotNull(savedLibrarySectionsEntity);

            assertEquals(this.librarySectionsEntity.getId(), savedLibrarySectionsEntity.getId());
            assertEquals(this.librarySectionsEntity.getName(), savedLibrarySectionsEntity.getName());
            assertEquals(this.librarySectionsEntity.getNameSort(), savedLibrarySectionsEntity.getNameSort());
            assertEquals(this.librarySectionsEntity.getSectionType(), savedLibrarySectionsEntity.getSectionType());
            assertEquals(this.librarySectionsEntity.getLanguage(), savedLibrarySectionsEntity.getLanguage());
            assertEquals(this.librarySectionsEntity.getAgent(), savedLibrarySectionsEntity.getAgent());
            assertEquals(this.librarySectionsEntity.getScanner(), savedLibrarySectionsEntity.getScanner());
            assertEquals(this.librarySectionsEntity.getUserThumbUrl(), savedLibrarySectionsEntity.getUserThumbUrl());
            assertEquals(this.librarySectionsEntity.getUserArtUrl(), savedLibrarySectionsEntity.getUserArtUrl());
            assertEquals(this.librarySectionsEntity.getUserThemeMusicUrl(), savedLibrarySectionsEntity.getUserThemeMusicUrl());
            assertEquals(this.librarySectionsEntity.getIsPublic(), savedLibrarySectionsEntity.getIsPublic());
            assertEquals(this.librarySectionsEntity.getCreatedAt(), savedLibrarySectionsEntity.getCreatedAt());
            assertEquals(this.librarySectionsEntity.getUpdatedAt(), savedLibrarySectionsEntity.getUpdatedAt());
            assertEquals(this.librarySectionsEntity.getScannedAt(), savedLibrarySectionsEntity.getScannedAt());
            assertEquals(this.librarySectionsEntity.getDisplaySecondaryLevel(), savedLibrarySectionsEntity.getDisplaySecondaryLevel());
            assertEquals(this.librarySectionsEntity.getUserFields(), savedLibrarySectionsEntity.getUserFields());
            assertEquals(this.librarySectionsEntity.getQueryXml(), savedLibrarySectionsEntity.getQueryXml());
            assertEquals(this.librarySectionsEntity.getQueryType(), savedLibrarySectionsEntity.getQueryType());
            assertEquals(this.librarySectionsEntity.getUuid(), savedLibrarySectionsEntity.getUuid());
            assertEquals(this.librarySectionsEntity.getChangedAt(), savedLibrarySectionsEntity.getChangedAt());
        }
    }

    /**
     * Test search failed.
     */
    @org.junit.Test
    public void testAbstractLibrarySectionsEntitySearchFailed() {
        final LibrarySectionsPersistence librarySectionsPersistence = PersistenceServiceProvider.getService(LibrarySectionsPersistence.class);

        // Setup invalid object librarySectionsEntity

        librarySectionsPersistence.save(this.librarySectionsEntity);
        final Integer id = Integer.valueOf(-1);
        final LibrarySectionsEntity savedLibrarySectionsEntity = librarySectionsPersistence.load(id);

        assertNull(savedLibrarySectionsEntity);
    }

    /**
     * Test delete.
     */
    @org.junit.Test
    public void testAbstractLibrarySectionsEntityDelete() {
        final LibrarySectionsPersistence librarySectionsPersistence = PersistenceServiceProvider.getService(LibrarySectionsPersistence.class);

        // Setup valid object librarySectionsEntity

        final Integer id = this.librarySectionsEntity.getId();
        if (id != null) {
            librarySectionsPersistence.save(this.librarySectionsEntity);
            final LibrarySectionsEntity savedLibrarySectionsEntity = librarySectionsPersistence.load(id);
            assertNotNull(savedLibrarySectionsEntity);

            librarySectionsPersistence.delete(this.librarySectionsEntity);
            final LibrarySectionsEntity deletedLibrarySectionsEntity = librarySectionsPersistence.load(id);
            assertNull(deletedLibrarySectionsEntity);
        }
    }
}
