package aka.plexdb.bean;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import aka.plexdb.bean.abstracts.AbstractMediaPartsEntity_JUnitTest;
import aka.plexdb.persistence.PersistenceServiceProvider;
import aka.plexdb.persistence.services.DirectoriesPersistence;
import aka.plexdb.persistence.services.MediaItemsPersistence;
import aka.plexdb.persistence.services.MediaPartsPersistence;

/**
 * JUnitTest for MediaPartsEntity.
 */
public class MediaPartsEntity_JUnitTest extends AbstractMediaPartsEntity_JUnitTest {

    /**
     * Test save successful.
     */
    @org.junit.Test
    public void testAbstractMediaPartsEntitySaveSuccessful() {
        final MediaPartsPersistence mediaPartsPersistence = PersistenceServiceProvider.getService(MediaPartsPersistence.class);

        // Setup valid object mediaPartsEntity
        final DirectoriesEntity directoriesEntity = new DirectoriesEntity();
        directoriesEntity.setId(Integer.valueOf(1));
        final DirectoriesPersistence directoriesPersistence = PersistenceServiceProvider.getService(DirectoriesPersistence.class);
        directoriesPersistence.save(directoriesEntity);
        this.mediaPartsEntity.setDirectory(directoriesEntity);

        final MediaItemsEntity mediaItemsEntity = new MediaItemsEntity();
        mediaItemsEntity.setId(Integer.valueOf(1));
        final MediaItemsPersistence mediaItemsPersistence = PersistenceServiceProvider.getService(MediaItemsPersistence.class);
        mediaItemsPersistence.save(mediaItemsEntity);
        this.mediaPartsEntity.setMediaItem(mediaItemsEntity);

        mediaPartsPersistence.save(this.mediaPartsEntity);
        final Integer id = this.mediaPartsEntity.getId();
        if (id != null) {
            final MediaPartsEntity savedMediaPartsEntity = mediaPartsPersistence.load(id);
            assertNotNull(savedMediaPartsEntity);
        }
    }

    /**
     * Test save failed.
     */
    @org.junit.Test(expected = javax.persistence.PersistenceException.class)
    public void testAbstractMediaPartsEntitySaveFailed() {
        final MediaPartsPersistence mediaPartsPersistence = PersistenceServiceProvider.getService(MediaPartsPersistence.class);

        // Setup invalid object mediaPartsEntity

        this.mediaPartsEntity.setId(null);
        mediaPartsPersistence.save(this.mediaPartsEntity);

        fail("expected an exception");
    }

    /**
     * Test search successful.
     */
    @org.junit.Test
    public void testAbstractMediaPartsEntitySearchSuccessful() {
        final MediaPartsPersistence mediaPartsPersistence = PersistenceServiceProvider.getService(MediaPartsPersistence.class);

        // Setup valid object mediaPartsEntity
        final DirectoriesEntity directoriesEntity = new DirectoriesEntity();
        directoriesEntity.setId(Integer.valueOf(1));
        final DirectoriesPersistence directoriesPersistence = PersistenceServiceProvider.getService(DirectoriesPersistence.class);
        directoriesPersistence.save(directoriesEntity);
        this.mediaPartsEntity.setDirectory(directoriesEntity);

        final MediaItemsEntity mediaItemsEntity = new MediaItemsEntity();
        mediaItemsEntity.setId(Integer.valueOf(1));
        final MediaItemsPersistence mediaItemsPersistence = PersistenceServiceProvider.getService(MediaItemsPersistence.class);
        mediaItemsPersistence.save(mediaItemsEntity);
        this.mediaPartsEntity.setMediaItem(mediaItemsEntity);

        mediaPartsPersistence.save(this.mediaPartsEntity);
        final Integer id = this.mediaPartsEntity.getId();
        if (id != null) {
            final MediaPartsEntity savedMediaPartsEntity = mediaPartsPersistence.load(id);
            assertNotNull(savedMediaPartsEntity);

            assertEquals(this.mediaPartsEntity.getId(), savedMediaPartsEntity.getId());
            assertEquals(this.mediaPartsEntity.getHash(), savedMediaPartsEntity.getHash());
            assertEquals(this.mediaPartsEntity.getOpenSubtitleHash(), savedMediaPartsEntity.getOpenSubtitleHash());
            assertEquals(this.mediaPartsEntity.getFile(), savedMediaPartsEntity.getFile());
            assertEquals(this.mediaPartsEntity.getSizePart(), savedMediaPartsEntity.getSizePart());
            assertEquals(this.mediaPartsEntity.getDuration(), savedMediaPartsEntity.getDuration());
            assertEquals(this.mediaPartsEntity.getCreatedAt(), savedMediaPartsEntity.getCreatedAt());
            assertEquals(this.mediaPartsEntity.getUpdatedAt(), savedMediaPartsEntity.getUpdatedAt());
            assertEquals(this.mediaPartsEntity.getDeletedAt(), savedMediaPartsEntity.getDeletedAt());
            assertEquals(this.mediaPartsEntity.getExtraData(), savedMediaPartsEntity.getExtraData());
        }
    }

    /**
     * Test search failed.
     */
    @org.junit.Test
    public void testAbstractMediaPartsEntitySearchFailed() {
        final MediaPartsPersistence mediaPartsPersistence = PersistenceServiceProvider.getService(MediaPartsPersistence.class);

        // Setup valid object mediaPartsEntity
        final DirectoriesEntity directoriesEntity = new DirectoriesEntity();
        directoriesEntity.setId(Integer.valueOf(1));
        final DirectoriesPersistence directoriesPersistence = PersistenceServiceProvider.getService(DirectoriesPersistence.class);
        directoriesPersistence.save(directoriesEntity);
        this.mediaPartsEntity.setDirectory(directoriesEntity);

        final MediaItemsEntity mediaItemsEntity = new MediaItemsEntity();
        mediaItemsEntity.setId(Integer.valueOf(1));
        final MediaItemsPersistence mediaItemsPersistence = PersistenceServiceProvider.getService(MediaItemsPersistence.class);
        mediaItemsPersistence.save(mediaItemsEntity);
        this.mediaPartsEntity.setMediaItem(mediaItemsEntity);

        mediaPartsPersistence.save(this.mediaPartsEntity);
        final Integer id = Integer.valueOf(-1);
        final MediaPartsEntity savedMediaPartsEntity = mediaPartsPersistence.load(id);

        assertNull(savedMediaPartsEntity);
    }

    /**
     * Test delete.
     */
    @org.junit.Test
    public void testAbstractMediaPartsEntityDelete() {
        final MediaPartsPersistence mediaPartsPersistence = PersistenceServiceProvider.getService(MediaPartsPersistence.class);

        // Setup valid object mediaPartsEntity
        final DirectoriesEntity directoriesEntity = new DirectoriesEntity();
        directoriesEntity.setId(Integer.valueOf(1));
        final DirectoriesPersistence directoriesPersistence = PersistenceServiceProvider.getService(DirectoriesPersistence.class);
        directoriesPersistence.save(directoriesEntity);
        this.mediaPartsEntity.setDirectory(directoriesEntity);

        final MediaItemsEntity mediaItemsEntity = new MediaItemsEntity();
        mediaItemsEntity.setId(Integer.valueOf(1));
        final MediaItemsPersistence mediaItemsPersistence = PersistenceServiceProvider.getService(MediaItemsPersistence.class);
        mediaItemsPersistence.save(mediaItemsEntity);
        this.mediaPartsEntity.setMediaItem(mediaItemsEntity);

        final Integer id = this.mediaPartsEntity.getId();
        if (id != null) {
            mediaPartsPersistence.save(this.mediaPartsEntity);
            final MediaPartsEntity savedMediaPartsEntity = mediaPartsPersistence.load(id);
            assertNotNull(savedMediaPartsEntity);

            mediaPartsPersistence.delete(this.mediaPartsEntity);
            final MediaPartsEntity deletedMediaPartsEntity = mediaPartsPersistence.load(id);
            assertNull(deletedMediaPartsEntity);
        }
    }
}
