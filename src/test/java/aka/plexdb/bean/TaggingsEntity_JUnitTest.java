package aka.plexdb.bean;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import aka.plexdb.bean.abstracts.AbstractTaggingsEntity_JUnitTest;
import aka.plexdb.persistence.PersistenceServiceProvider;
import aka.plexdb.persistence.services.TaggingsPersistence;

/**
 * JUnitTest for TaggingsEntity.
 */
public class TaggingsEntity_JUnitTest extends AbstractTaggingsEntity_JUnitTest {

    /**
     * Test save successful.
     */
    @org.junit.Test
    public void testAbstractTaggingsEntitySaveSuccessful() {
        final TaggingsPersistence taggingsPersistence = PersistenceServiceProvider.getService(TaggingsPersistence.class);

        // Setup valid object taggingsEntity

        taggingsPersistence.save(this.taggingsEntity);
        Integer id = this.taggingsEntity.getId();
        if (id != null) {
            final TaggingsEntity savedTaggingsEntity = taggingsPersistence.load(id);
            assertNotNull(savedTaggingsEntity);
        }
    }

    /**
     * Test save failed.
     */
    @org.junit.Test(expected = javax.persistence.PersistenceException.class)
    public void testAbstractTaggingsEntitySaveFailed() {
        final TaggingsPersistence taggingsPersistence = PersistenceServiceProvider.getService(TaggingsPersistence.class);

        // Setup invalid object taggingsEntity

        this.taggingsEntity.setId(null);
        taggingsPersistence.save(this.taggingsEntity);

        fail("expected an exception");
    }

    /**
     * Test search successful.
     */
    @org.junit.Test
    public void testAbstractTaggingsEntitySearchSuccessful() {
        final TaggingsPersistence taggingsPersistence = PersistenceServiceProvider.getService(TaggingsPersistence.class);

        // Setup valid object taggingsEntity

        taggingsPersistence.save(this.taggingsEntity);
        Integer id = this.taggingsEntity.getId();
        if (id != null) {
            final TaggingsEntity savedTaggingsEntity = taggingsPersistence.load(id);
            assertNotNull(savedTaggingsEntity);

            assertEquals(this.taggingsEntity.getId(), savedTaggingsEntity.getId());
            assertEquals(this.taggingsEntity.getText(), savedTaggingsEntity.getText());
            assertEquals(this.taggingsEntity.getTimeOffset(), savedTaggingsEntity.getTimeOffset());
            assertEquals(this.taggingsEntity.getEndTimeOffset(), savedTaggingsEntity.getEndTimeOffset());
            assertEquals(this.taggingsEntity.getThumbUrl(), savedTaggingsEntity.getThumbUrl());
            assertEquals(this.taggingsEntity.getCreatedAt(), savedTaggingsEntity.getCreatedAt());
            assertEquals(this.taggingsEntity.getExtraData(), savedTaggingsEntity.getExtraData());
        }
    }

    /**
     * Test search failed.
     */
    @org.junit.Test
    public void testAbstractTaggingsEntitySearchFailed() {
        final TaggingsPersistence taggingsPersistence = PersistenceServiceProvider.getService(TaggingsPersistence.class);

        // Setup invalid object taggingsEntity

        taggingsPersistence.save(this.taggingsEntity);
        final Integer id = Integer.valueOf(-1);
        final TaggingsEntity savedTaggingsEntity = taggingsPersistence.load(id);

        assertNull(savedTaggingsEntity);
    }

    /**
     * Test delete.
     */
    @org.junit.Test
    public void testAbstractTaggingsEntityDelete() {
        final TaggingsPersistence taggingsPersistence = PersistenceServiceProvider.getService(TaggingsPersistence.class);

        // Setup valid object taggingsEntity

        final Integer id = this.taggingsEntity.getId();
        if (id != null) {
            taggingsPersistence.save(this.taggingsEntity);
            final TaggingsEntity savedTaggingsEntity = taggingsPersistence.load(id);
            assertNotNull(savedTaggingsEntity);

            taggingsPersistence.delete(this.taggingsEntity);
            final TaggingsEntity deletedTaggingsEntity = taggingsPersistence.load(id);
            assertNull(deletedTaggingsEntity);
        }
    }
}
