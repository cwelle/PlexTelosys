package aka.plexdb.bean;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import aka.plexdb.bean.abstracts.AbstractMetadataItemSettingsEntity_JUnitTest;
import aka.plexdb.persistence.PersistenceServiceProvider;
import aka.plexdb.persistence.services.MetadataItemSettingsPersistence;

/**
 * JUnitTest for MetadataItemSettingsEntity.
 */
public class MetadataItemSettingsEntity_JUnitTest extends AbstractMetadataItemSettingsEntity_JUnitTest {

    /**
     * Test save successful.
     */
    @org.junit.Test
    public void testAbstractMetadataItemSettingsEntitySaveSuccessful() {
        final MetadataItemSettingsPersistence metadataItemSettingsPersistence = PersistenceServiceProvider.getService(MetadataItemSettingsPersistence.class);

        // Setup valid object metadataItemSettingsEntity

        metadataItemSettingsPersistence.save(this.metadataItemSettingsEntity);
        Integer id = this.metadataItemSettingsEntity.getId();
        if (id != null) {
            final MetadataItemSettingsEntity savedMetadataItemSettingsEntity = metadataItemSettingsPersistence.load(id);
            assertNotNull(savedMetadataItemSettingsEntity);
        }
    }

    /**
     * Test save failed.
     */
    @org.junit.Test(expected = javax.persistence.PersistenceException.class)
    public void testAbstractMetadataItemSettingsEntitySaveFailed() {
        final MetadataItemSettingsPersistence metadataItemSettingsPersistence = PersistenceServiceProvider.getService(MetadataItemSettingsPersistence.class);

        // Setup invalid object metadataItemSettingsEntity

        this.metadataItemSettingsEntity.setId(null);
        metadataItemSettingsPersistence.save(this.metadataItemSettingsEntity);

        fail("expected an exception");
    }

    /**
     * Test search successful.
     */
    @org.junit.Test
    public void testAbstractMetadataItemSettingsEntitySearchSuccessful() {
        final MetadataItemSettingsPersistence metadataItemSettingsPersistence = PersistenceServiceProvider.getService(MetadataItemSettingsPersistence.class);

        // Setup valid object metadataItemSettingsEntity

        metadataItemSettingsPersistence.save(this.metadataItemSettingsEntity);
        Integer id = this.metadataItemSettingsEntity.getId();
        if (id != null) {
            final MetadataItemSettingsEntity savedMetadataItemSettingsEntity = metadataItemSettingsPersistence.load(id);
            assertNotNull(savedMetadataItemSettingsEntity);

            assertEquals(this.metadataItemSettingsEntity.getId(), savedMetadataItemSettingsEntity.getId());
            assertEquals(this.metadataItemSettingsEntity.getGuid(), savedMetadataItemSettingsEntity.getGuid());
            assertEquals(this.metadataItemSettingsEntity.getRating(), savedMetadataItemSettingsEntity.getRating());
            assertEquals(this.metadataItemSettingsEntity.getViewOffset(), savedMetadataItemSettingsEntity.getViewOffset());
            assertEquals(this.metadataItemSettingsEntity.getViewCount(), savedMetadataItemSettingsEntity.getViewCount());
            assertEquals(this.metadataItemSettingsEntity.getLastViewedAt(), savedMetadataItemSettingsEntity.getLastViewedAt());
            assertEquals(this.metadataItemSettingsEntity.getCreatedAt(), savedMetadataItemSettingsEntity.getCreatedAt());
            assertEquals(this.metadataItemSettingsEntity.getUpdatedAt(), savedMetadataItemSettingsEntity.getUpdatedAt());
            assertEquals(this.metadataItemSettingsEntity.getSkipCount(), savedMetadataItemSettingsEntity.getSkipCount());
            assertEquals(this.metadataItemSettingsEntity.getLastSkippedAt(), savedMetadataItemSettingsEntity.getLastSkippedAt());
            assertEquals(this.metadataItemSettingsEntity.getChangedAt(), savedMetadataItemSettingsEntity.getChangedAt());
        }
    }

    /**
     * Test search failed.
     */
    @org.junit.Test
    public void testAbstractMetadataItemSettingsEntitySearchFailed() {
        final MetadataItemSettingsPersistence metadataItemSettingsPersistence = PersistenceServiceProvider.getService(MetadataItemSettingsPersistence.class);

        // Setup invalid object metadataItemSettingsEntity

        metadataItemSettingsPersistence.save(this.metadataItemSettingsEntity);
        final Integer id = Integer.valueOf(-1);
        final MetadataItemSettingsEntity savedMetadataItemSettingsEntity = metadataItemSettingsPersistence.load(id);

        assertNull(savedMetadataItemSettingsEntity);
    }

    /**
     * Test delete.
     */
    @org.junit.Test
    public void testAbstractMetadataItemSettingsEntityDelete() {
        final MetadataItemSettingsPersistence metadataItemSettingsPersistence = PersistenceServiceProvider.getService(MetadataItemSettingsPersistence.class);

        // Setup valid object metadataItemSettingsEntity

        final Integer id = this.metadataItemSettingsEntity.getId();
        if (id != null) {
            metadataItemSettingsPersistence.save(this.metadataItemSettingsEntity);
            final MetadataItemSettingsEntity savedMetadataItemSettingsEntity = metadataItemSettingsPersistence.load(id);
            assertNotNull(savedMetadataItemSettingsEntity);

            metadataItemSettingsPersistence.delete(this.metadataItemSettingsEntity);
            final MetadataItemSettingsEntity deletedMetadataItemSettingsEntity = metadataItemSettingsPersistence.load(id);
            assertNull(deletedMetadataItemSettingsEntity);
        }
    }
}
