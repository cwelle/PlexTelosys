package aka.plexdb.bean;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import aka.plexdb.bean.abstracts.AbstractMetadataItemViewsEntity_JUnitTest;
import aka.plexdb.persistence.PersistenceServiceProvider;
import aka.plexdb.persistence.services.MetadataItemViewsPersistence;

/**
 * JUnitTest for MetadataItemViewsEntity.
 */
public class MetadataItemViewsEntity_JUnitTest extends AbstractMetadataItemViewsEntity_JUnitTest {

    /**
     * Test save successful.
     */
    @org.junit.Test
    public void testAbstractMetadataItemViewsEntitySaveSuccessful() {
        final MetadataItemViewsPersistence metadataItemViewsPersistence = PersistenceServiceProvider.getService(MetadataItemViewsPersistence.class);

        // Setup valid object metadataItemViewsEntity

        metadataItemViewsPersistence.save(this.metadataItemViewsEntity);
        Integer id = this.metadataItemViewsEntity.getId();
        if (id != null) {
            final MetadataItemViewsEntity savedMetadataItemViewsEntity = metadataItemViewsPersistence.load(id);
            assertNotNull(savedMetadataItemViewsEntity);
        }
    }

    /**
     * Test save failed.
     */
    @org.junit.Test(expected = javax.persistence.PersistenceException.class)
    public void testAbstractMetadataItemViewsEntitySaveFailed() {
        final MetadataItemViewsPersistence metadataItemViewsPersistence = PersistenceServiceProvider.getService(MetadataItemViewsPersistence.class);

        // Setup invalid object metadataItemViewsEntity

        this.metadataItemViewsEntity.setId(null);
        metadataItemViewsPersistence.save(this.metadataItemViewsEntity);

        fail("expected an exception");
    }

    /**
     * Test search successful.
     */
    @org.junit.Test
    public void testAbstractMetadataItemViewsEntitySearchSuccessful() {
        final MetadataItemViewsPersistence metadataItemViewsPersistence = PersistenceServiceProvider.getService(MetadataItemViewsPersistence.class);

        // Setup valid object metadataItemViewsEntity

        metadataItemViewsPersistence.save(this.metadataItemViewsEntity);
        Integer id = this.metadataItemViewsEntity.getId();
        if (id != null) {
            final MetadataItemViewsEntity savedMetadataItemViewsEntity = metadataItemViewsPersistence.load(id);
            assertNotNull(savedMetadataItemViewsEntity);

            assertEquals(this.metadataItemViewsEntity.getId(), savedMetadataItemViewsEntity.getId());
            assertEquals(this.metadataItemViewsEntity.getGuid(), savedMetadataItemViewsEntity.getGuid());
            assertEquals(this.metadataItemViewsEntity.getMetadataType(), savedMetadataItemViewsEntity.getMetadataType());
            assertEquals(this.metadataItemViewsEntity.getGrandparentTitle(), savedMetadataItemViewsEntity.getGrandparentTitle());
            assertEquals(this.metadataItemViewsEntity.getParentIndex(), savedMetadataItemViewsEntity.getParentIndex());
            assertEquals(this.metadataItemViewsEntity.getParentTitle(), savedMetadataItemViewsEntity.getParentTitle());
            assertEquals(this.metadataItemViewsEntity.getTitle(), savedMetadataItemViewsEntity.getTitle());
            assertEquals(this.metadataItemViewsEntity.getThumbUrl(), savedMetadataItemViewsEntity.getThumbUrl());
            assertEquals(this.metadataItemViewsEntity.getViewedAt(), savedMetadataItemViewsEntity.getViewedAt());
            assertEquals(this.metadataItemViewsEntity.getGrandparentGuid(), savedMetadataItemViewsEntity.getGrandparentGuid());
            assertEquals(this.metadataItemViewsEntity.getOriginallyAvailableAt(), savedMetadataItemViewsEntity.getOriginallyAvailableAt());
        }
    }

    /**
     * Test search failed.
     */
    @org.junit.Test
    public void testAbstractMetadataItemViewsEntitySearchFailed() {
        final MetadataItemViewsPersistence metadataItemViewsPersistence = PersistenceServiceProvider.getService(MetadataItemViewsPersistence.class);

        // Setup invalid object metadataItemViewsEntity

        metadataItemViewsPersistence.save(this.metadataItemViewsEntity);
        final Integer id = Integer.valueOf(-1);
        final MetadataItemViewsEntity savedMetadataItemViewsEntity = metadataItemViewsPersistence.load(id);

        assertNull(savedMetadataItemViewsEntity);
    }

    /**
     * Test delete.
     */
    @org.junit.Test
    public void testAbstractMetadataItemViewsEntityDelete() {
        final MetadataItemViewsPersistence metadataItemViewsPersistence = PersistenceServiceProvider.getService(MetadataItemViewsPersistence.class);

        // Setup valid object metadataItemViewsEntity

        final Integer id = this.metadataItemViewsEntity.getId();
        if (id != null) {
            metadataItemViewsPersistence.save(this.metadataItemViewsEntity);
            final MetadataItemViewsEntity savedMetadataItemViewsEntity = metadataItemViewsPersistence.load(id);
            assertNotNull(savedMetadataItemViewsEntity);

            metadataItemViewsPersistence.delete(this.metadataItemViewsEntity);
            final MetadataItemViewsEntity deletedMetadataItemViewsEntity = metadataItemViewsPersistence.load(id);
            assertNull(deletedMetadataItemViewsEntity);
        }
    }
}
