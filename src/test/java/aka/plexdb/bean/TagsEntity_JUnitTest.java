package aka.plexdb.bean;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import aka.plexdb.bean.abstracts.AbstractTagsEntity_JUnitTest;
import aka.plexdb.persistence.PersistenceServiceProvider;
import aka.plexdb.persistence.services.MetadataItemsPersistence;
import aka.plexdb.persistence.services.TagsPersistence;

/**
 * JUnitTest for TagsEntity.
 */
public class TagsEntity_JUnitTest extends AbstractTagsEntity_JUnitTest {

    /**
     * Test save successful.
     */
    @org.junit.Test
    public void testAbstractTagsEntitySaveSuccessful() {
        final TagsPersistence tagsPersistence = PersistenceServiceProvider.getService(TagsPersistence.class);

        // Setup valid object mediaPartsEntity
        final MetadataItemsEntity metadataItemsEntity = new MetadataItemsEntity();
        metadataItemsEntity.setId(Integer.valueOf(1));
        final MetadataItemsPersistence metadataItemsPersistence = PersistenceServiceProvider.getService(MetadataItemsPersistence.class);
        metadataItemsPersistence.save(metadataItemsEntity);
        this.tagsEntity.setMetadataItem(metadataItemsEntity);

        tagsPersistence.save(this.tagsEntity);
        final Integer id = this.tagsEntity.getId();
        if (id != null) {
            final TagsEntity savedTagsEntity = tagsPersistence.load(id);
            assertNotNull(savedTagsEntity);
        }
    }

    /**
     * Test save failed.
     */
    @org.junit.Test(expected = javax.persistence.PersistenceException.class)
    public void testAbstractTagsEntitySaveFailed() {
        final TagsPersistence tagsPersistence = PersistenceServiceProvider.getService(TagsPersistence.class);

        // Setup invalid object tagsEntity

        this.tagsEntity.setId(null);
        tagsPersistence.save(this.tagsEntity);

        fail("expected an exception");
    }

    /**
     * Test search successful.
     */
    @org.junit.Test
    public void testAbstractTagsEntitySearchSuccessful() {
        final TagsPersistence tagsPersistence = PersistenceServiceProvider.getService(TagsPersistence.class);

        // Setup valid object mediaPartsEntity
        final MetadataItemsEntity metadataItemsEntity = new MetadataItemsEntity();
        metadataItemsEntity.setId(Integer.valueOf(1));
        final MetadataItemsPersistence metadataItemsPersistence = PersistenceServiceProvider.getService(MetadataItemsPersistence.class);
        metadataItemsPersistence.save(metadataItemsEntity);
        this.tagsEntity.setMetadataItem(metadataItemsEntity);

        tagsPersistence.save(this.tagsEntity);
        final Integer id = this.tagsEntity.getId();
        if (id != null) {
            final TagsEntity savedTagsEntity = tagsPersistence.load(id);
            assertNotNull(savedTagsEntity);

            assertEquals(this.tagsEntity.getId(), savedTagsEntity.getId());
            assertEquals(this.tagsEntity.getTag(), savedTagsEntity.getTag());
            assertEquals(this.tagsEntity.getTagType(), savedTagsEntity.getTagType());
            assertEquals(this.tagsEntity.getUserThumbUrl(), savedTagsEntity.getUserThumbUrl());
            assertEquals(this.tagsEntity.getUserArtUrl(), savedTagsEntity.getUserArtUrl());
            assertEquals(this.tagsEntity.getUserMusicUrl(), savedTagsEntity.getUserMusicUrl());
            assertEquals(this.tagsEntity.getCreatedAt(), savedTagsEntity.getCreatedAt());
            assertEquals(this.tagsEntity.getUpdatedAt(), savedTagsEntity.getUpdatedAt());
            assertEquals(this.tagsEntity.getTagValue(), savedTagsEntity.getTagValue());
            assertEquals(this.tagsEntity.getExtraData(), savedTagsEntity.getExtraData());
        }
    }

    /**
     * Test search failed.
     */
    @org.junit.Test
    public void testAbstractTagsEntitySearchFailed() {
        final TagsPersistence tagsPersistence = PersistenceServiceProvider.getService(TagsPersistence.class);

        // Setup valid object mediaPartsEntity
        final MetadataItemsEntity metadataItemsEntity = new MetadataItemsEntity();
        metadataItemsEntity.setId(Integer.valueOf(1));
        final MetadataItemsPersistence metadataItemsPersistence = PersistenceServiceProvider.getService(MetadataItemsPersistence.class);
        metadataItemsPersistence.save(metadataItemsEntity);
        this.tagsEntity.setMetadataItem(metadataItemsEntity);

        tagsPersistence.save(this.tagsEntity);
        final Integer id = Integer.valueOf(-1);
        final TagsEntity savedTagsEntity = tagsPersistence.load(id);

        assertNull(savedTagsEntity);
    }

    /**
     * Test delete.
     */
    @org.junit.Test
    public void testAbstractTagsEntityDelete() {
        final TagsPersistence tagsPersistence = PersistenceServiceProvider.getService(TagsPersistence.class);

        // Setup valid object mediaPartsEntity
        final MetadataItemsEntity metadataItemsEntity = new MetadataItemsEntity();
        metadataItemsEntity.setId(Integer.valueOf(1));
        final MetadataItemsPersistence metadataItemsPersistence = PersistenceServiceProvider.getService(MetadataItemsPersistence.class);
        metadataItemsPersistence.save(metadataItemsEntity);
        this.tagsEntity.setMetadataItem(metadataItemsEntity);

        final Integer id = this.tagsEntity.getId();
        if (id != null) {
            tagsPersistence.save(this.tagsEntity);
            final TagsEntity savedTagsEntity = tagsPersistence.load(id);
            assertNotNull(savedTagsEntity);

            tagsPersistence.delete(this.tagsEntity);
            final TagsEntity deletedTagsEntity = tagsPersistence.load(id);
            assertNull(deletedTagsEntity);
        }
    }
}
