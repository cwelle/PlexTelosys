package aka.plexdb.bean;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import aka.plexdb.bean.abstracts.AbstractMediaItemsEntity_JUnitTest;
import aka.plexdb.persistence.PersistenceServiceProvider;
import aka.plexdb.persistence.services.MediaItemsPersistence;

/**
 * JUnitTest for MediaItemsEntity.
 */
public class MediaItemsEntity_JUnitTest extends AbstractMediaItemsEntity_JUnitTest {

    /**
     * Test save successful.
     */
    @org.junit.Test
    public void testAbstractMediaItemsEntitySaveSuccessful() {
        final MediaItemsPersistence mediaItemsPersistence = PersistenceServiceProvider.getService(MediaItemsPersistence.class);

        // Setup valid object mediaItemsEntity

        mediaItemsPersistence.save(this.mediaItemsEntity);
        Integer id = this.mediaItemsEntity.getId();
        if (id != null) {
            final MediaItemsEntity savedMediaItemsEntity = mediaItemsPersistence.load(id);
            assertNotNull(savedMediaItemsEntity);
        }
    }

    /**
     * Test save failed.
     */
    @org.junit.Test(expected = javax.persistence.PersistenceException.class)
    public void testAbstractMediaItemsEntitySaveFailed() {
        final MediaItemsPersistence mediaItemsPersistence = PersistenceServiceProvider.getService(MediaItemsPersistence.class);

        // Setup invalid object mediaItemsEntity

        this.mediaItemsEntity.setId(null);
        mediaItemsPersistence.save(this.mediaItemsEntity);

        fail("expected an exception");
    }

    /**
     * Test search successful.
     */
    @org.junit.Test
    public void testAbstractMediaItemsEntitySearchSuccessful() {
        final MediaItemsPersistence mediaItemsPersistence = PersistenceServiceProvider.getService(MediaItemsPersistence.class);

        // Setup valid object mediaItemsEntity

        mediaItemsPersistence.save(this.mediaItemsEntity);
        Integer id = this.mediaItemsEntity.getId();
        if (id != null) {
            final MediaItemsEntity savedMediaItemsEntity = mediaItemsPersistence.load(id);
            assertNotNull(savedMediaItemsEntity);

            assertEquals(this.mediaItemsEntity.getId(), savedMediaItemsEntity.getId());
            assertEquals(this.mediaItemsEntity.getTypeId(), savedMediaItemsEntity.getTypeId());
            assertEquals(this.mediaItemsEntity.getWidth(), savedMediaItemsEntity.getWidth());
            assertEquals(this.mediaItemsEntity.getHeight(), savedMediaItemsEntity.getHeight());
            assertEquals(this.mediaItemsEntity.getSize(), savedMediaItemsEntity.getSize());
            assertEquals(this.mediaItemsEntity.getDuration(), savedMediaItemsEntity.getDuration());
            assertEquals(this.mediaItemsEntity.getBitrate(), savedMediaItemsEntity.getBitrate());
            assertEquals(this.mediaItemsEntity.getContainer(), savedMediaItemsEntity.getContainer());
            assertEquals(this.mediaItemsEntity.getVideoCodec(), savedMediaItemsEntity.getVideoCodec());
            assertEquals(this.mediaItemsEntity.getAudioCodec(), savedMediaItemsEntity.getAudioCodec());
            assertEquals(this.mediaItemsEntity.getDisplayAspectRatio(), savedMediaItemsEntity.getDisplayAspectRatio());
            assertEquals(this.mediaItemsEntity.getFramesPerSecond(), savedMediaItemsEntity.getFramesPerSecond());
            assertEquals(this.mediaItemsEntity.getAudioChannels(), savedMediaItemsEntity.getAudioChannels());
            assertEquals(this.mediaItemsEntity.getInterlaced(), savedMediaItemsEntity.getInterlaced());
            assertEquals(this.mediaItemsEntity.getSource(), savedMediaItemsEntity.getSource());
            assertEquals(this.mediaItemsEntity.getHints(), savedMediaItemsEntity.getHints());
            assertEquals(this.mediaItemsEntity.getDisplayOffset(), savedMediaItemsEntity.getDisplayOffset());
            assertEquals(this.mediaItemsEntity.getSettings(), savedMediaItemsEntity.getSettings());
            assertEquals(this.mediaItemsEntity.getCreatedAt(), savedMediaItemsEntity.getCreatedAt());
            assertEquals(this.mediaItemsEntity.getUpdatedAt(), savedMediaItemsEntity.getUpdatedAt());
            assertEquals(this.mediaItemsEntity.getOptimizedForStreaming(), savedMediaItemsEntity.getOptimizedForStreaming());
            assertEquals(this.mediaItemsEntity.getDeletedAt(), savedMediaItemsEntity.getDeletedAt());
            assertEquals(this.mediaItemsEntity.getMediaAnalysisVersion(), savedMediaItemsEntity.getMediaAnalysisVersion());
            assertEquals(this.mediaItemsEntity.getSampleAspectRatio(), savedMediaItemsEntity.getSampleAspectRatio());
            assertEquals(this.mediaItemsEntity.getExtraData(), savedMediaItemsEntity.getExtraData());
            assertEquals(this.mediaItemsEntity.getProxyType(), savedMediaItemsEntity.getProxyType());
        }
    }

    /**
     * Test search failed.
     */
    @org.junit.Test
    public void testAbstractMediaItemsEntitySearchFailed() {
        final MediaItemsPersistence mediaItemsPersistence = PersistenceServiceProvider.getService(MediaItemsPersistence.class);

        // Setup invalid object mediaItemsEntity

        mediaItemsPersistence.save(this.mediaItemsEntity);
        final Integer id = Integer.valueOf(-1);
        final MediaItemsEntity savedMediaItemsEntity = mediaItemsPersistence.load(id);

        assertNull(savedMediaItemsEntity);
    }

    /**
     * Test delete.
     */
    @org.junit.Test
    public void testAbstractMediaItemsEntityDelete() {
        final MediaItemsPersistence mediaItemsPersistence = PersistenceServiceProvider.getService(MediaItemsPersistence.class);

        // Setup valid object mediaItemsEntity

        final Integer id = this.mediaItemsEntity.getId();
        if (id != null) {
            mediaItemsPersistence.save(this.mediaItemsEntity);
            final MediaItemsEntity savedMediaItemsEntity = mediaItemsPersistence.load(id);
            assertNotNull(savedMediaItemsEntity);

            mediaItemsPersistence.delete(this.mediaItemsEntity);
            final MediaItemsEntity deletedMediaItemsEntity = mediaItemsPersistence.load(id);
            assertNull(deletedMediaItemsEntity);
        }
    }
}
