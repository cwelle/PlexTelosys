package aka.plexdb.bean;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import aka.plexdb.bean.abstracts.AbstractStreamTypesEntity_JUnitTest;
import aka.plexdb.persistence.PersistenceServiceProvider;
import aka.plexdb.persistence.services.StreamTypesPersistence;

/**
 * JUnitTest for StreamTypesEntity.
 */
public class StreamTypesEntity_JUnitTest extends AbstractStreamTypesEntity_JUnitTest {

    /**
     * Test save successful.
     */
    @org.junit.Test
    public void testAbstractStreamTypesEntitySaveSuccessful() {
        final StreamTypesPersistence streamTypesPersistence = PersistenceServiceProvider.getService(StreamTypesPersistence.class);

        // Setup valid object streamTypesEntity

        streamTypesPersistence.save(this.streamTypesEntity);
        Integer id = this.streamTypesEntity.getId();
        if (id != null) {
            final StreamTypesEntity savedStreamTypesEntity = streamTypesPersistence.load(id);
            assertNotNull(savedStreamTypesEntity);
        }
    }

    /**
     * Test save failed.
     */
    @org.junit.Test(expected = javax.persistence.PersistenceException.class)
    public void testAbstractStreamTypesEntitySaveFailed() {
        final StreamTypesPersistence streamTypesPersistence = PersistenceServiceProvider.getService(StreamTypesPersistence.class);

        // Setup invalid object streamTypesEntity

        this.streamTypesEntity.setId(null);
        streamTypesPersistence.save(this.streamTypesEntity);

        fail("expected an exception");
    }

    /**
     * Test search successful.
     */
    @org.junit.Test
    public void testAbstractStreamTypesEntitySearchSuccessful() {
        final StreamTypesPersistence streamTypesPersistence = PersistenceServiceProvider.getService(StreamTypesPersistence.class);

        // Setup valid object streamTypesEntity

        streamTypesPersistence.save(this.streamTypesEntity);
        Integer id = this.streamTypesEntity.getId();
        if (id != null) {
            final StreamTypesEntity savedStreamTypesEntity = streamTypesPersistence.load(id);
            assertNotNull(savedStreamTypesEntity);

            assertEquals(this.streamTypesEntity.getId(), savedStreamTypesEntity.getId());
            assertEquals(this.streamTypesEntity.getName(), savedStreamTypesEntity.getName());
        }
    }

    /**
     * Test search failed.
     */
    @org.junit.Test
    public void testAbstractStreamTypesEntitySearchFailed() {
        final StreamTypesPersistence streamTypesPersistence = PersistenceServiceProvider.getService(StreamTypesPersistence.class);

        // Setup invalid object streamTypesEntity

        streamTypesPersistence.save(this.streamTypesEntity);
        final Integer id = Integer.valueOf(-1);
        final StreamTypesEntity savedStreamTypesEntity = streamTypesPersistence.load(id);

        assertNull(savedStreamTypesEntity);
    }

    /**
     * Test delete.
     */
    @org.junit.Test
    public void testAbstractStreamTypesEntityDelete() {
        final StreamTypesPersistence streamTypesPersistence = PersistenceServiceProvider.getService(StreamTypesPersistence.class);

        // Setup valid object streamTypesEntity

        final Integer id = this.streamTypesEntity.getId();
        if (id != null) {
            streamTypesPersistence.save(this.streamTypesEntity);
            final StreamTypesEntity savedStreamTypesEntity = streamTypesPersistence.load(id);
            assertNotNull(savedStreamTypesEntity);

            streamTypesPersistence.delete(this.streamTypesEntity);
            final StreamTypesEntity deletedStreamTypesEntity = streamTypesPersistence.load(id);
            assertNull(deletedStreamTypesEntity);
        }
    }
}
