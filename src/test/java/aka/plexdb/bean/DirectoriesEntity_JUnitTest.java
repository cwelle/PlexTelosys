package aka.plexdb.bean;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import aka.plexdb.bean.abstracts.AbstractDirectoriesEntity_JUnitTest;
import aka.plexdb.persistence.PersistenceServiceProvider;
import aka.plexdb.persistence.services.DirectoriesPersistence;

/**
 * JUnitTest for DirectoriesEntity.
 */
public class DirectoriesEntity_JUnitTest extends AbstractDirectoriesEntity_JUnitTest {

    /**
     * Test save successful.
     */
    @org.junit.Test
    public void testAbstractDirectoriesEntitySaveSuccessful() {
        final DirectoriesPersistence directoriesPersistence = PersistenceServiceProvider.getService(DirectoriesPersistence.class);

        // Setup valid object directoriesEntity

        directoriesPersistence.save(this.directoriesEntity);
        Integer id = this.directoriesEntity.getId();
        if (id != null) {
            final DirectoriesEntity savedDirectoriesEntity = directoriesPersistence.load(id);
            assertNotNull(savedDirectoriesEntity);
        }
    }

    /**
     * Test save failed.
     */
    @org.junit.Test(expected = javax.persistence.PersistenceException.class)
    public void testAbstractDirectoriesEntitySaveFailed() {
        final DirectoriesPersistence directoriesPersistence = PersistenceServiceProvider.getService(DirectoriesPersistence.class);

        // Setup invalid object directoriesEntity

        this.directoriesEntity.setId(null);
        directoriesPersistence.save(this.directoriesEntity);

        fail("expected an exception");
    }

    /**
     * Test search successful.
     */
    @org.junit.Test
    public void testAbstractDirectoriesEntitySearchSuccessful() {
        final DirectoriesPersistence directoriesPersistence = PersistenceServiceProvider.getService(DirectoriesPersistence.class);

        // Setup valid object directoriesEntity

        directoriesPersistence.save(this.directoriesEntity);
        Integer id = this.directoriesEntity.getId();
        if (id != null) {
            final DirectoriesEntity savedDirectoriesEntity = directoriesPersistence.load(id);
            assertNotNull(savedDirectoriesEntity);

            assertEquals(this.directoriesEntity.getId(), savedDirectoriesEntity.getId());
            assertEquals(this.directoriesEntity.getPath(), savedDirectoriesEntity.getPath());
            assertEquals(this.directoriesEntity.getCreatedAt(), savedDirectoriesEntity.getCreatedAt());
            assertEquals(this.directoriesEntity.getUpdatedAt(), savedDirectoriesEntity.getUpdatedAt());
            assertEquals(this.directoriesEntity.getDeletedAt(), savedDirectoriesEntity.getDeletedAt());
        }
    }

    /**
     * Test search failed.
     */
    @org.junit.Test
    public void testAbstractDirectoriesEntitySearchFailed() {
        final DirectoriesPersistence directoriesPersistence = PersistenceServiceProvider.getService(DirectoriesPersistence.class);

        // Setup invalid object directoriesEntity

        directoriesPersistence.save(this.directoriesEntity);
        final Integer id = Integer.valueOf(-1);
        final DirectoriesEntity savedDirectoriesEntity = directoriesPersistence.load(id);

        assertNull(savedDirectoriesEntity);
    }

    /**
     * Test delete.
     */
    @org.junit.Test
    public void testAbstractDirectoriesEntityDelete() {
        final DirectoriesPersistence directoriesPersistence = PersistenceServiceProvider.getService(DirectoriesPersistence.class);

        // Setup valid object directoriesEntity

        final Integer id = this.directoriesEntity.getId();
        if (id != null) {
            directoriesPersistence.save(this.directoriesEntity);
            final DirectoriesEntity savedDirectoriesEntity = directoriesPersistence.load(id);
            assertNotNull(savedDirectoriesEntity);

            directoriesPersistence.delete(this.directoriesEntity);
            final DirectoriesEntity deletedDirectoriesEntity = directoriesPersistence.load(id);
            assertNull(deletedDirectoriesEntity);
        }
    }
}
