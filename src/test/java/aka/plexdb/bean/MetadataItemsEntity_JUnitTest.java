package aka.plexdb.bean;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import aka.plexdb.bean.abstracts.AbstractMetadataItemsEntity_JUnitTest;
import aka.plexdb.persistence.PersistenceServiceProvider;
import aka.plexdb.persistence.services.MetadataItemsPersistence;

/**
 * JUnitTest for MetadataItemsEntity.
 */
public class MetadataItemsEntity_JUnitTest extends AbstractMetadataItemsEntity_JUnitTest {

    /**
     * Test save successful.
     */
    @org.junit.Test
    public void testAbstractMetadataItemsEntitySaveSuccessful() {
        final MetadataItemsPersistence metadataItemsPersistence = PersistenceServiceProvider.getService(MetadataItemsPersistence.class);

        // Setup valid object metadataItemsEntity

        metadataItemsPersistence.save(this.metadataItemsEntity);
        Integer id = this.metadataItemsEntity.getId();
        if (id != null) {
            final MetadataItemsEntity savedMetadataItemsEntity = metadataItemsPersistence.load(id);
            assertNotNull(savedMetadataItemsEntity);
        }
    }

    /**
     * Test save failed.
     */
    @org.junit.Test(expected = javax.persistence.PersistenceException.class)
    public void testAbstractMetadataItemsEntitySaveFailed() {
        final MetadataItemsPersistence metadataItemsPersistence = PersistenceServiceProvider.getService(MetadataItemsPersistence.class);

        // Setup invalid object metadataItemsEntity

        this.metadataItemsEntity.setId(null);
        metadataItemsPersistence.save(this.metadataItemsEntity);

        fail("expected an exception");
    }

    /**
     * Test search successful.
     */
    @org.junit.Test
    public void testAbstractMetadataItemsEntitySearchSuccessful() {
        final MetadataItemsPersistence metadataItemsPersistence = PersistenceServiceProvider.getService(MetadataItemsPersistence.class);

        // Setup valid object metadataItemsEntity

        metadataItemsPersistence.save(this.metadataItemsEntity);
        Integer id = this.metadataItemsEntity.getId();
        if (id != null) {
            final MetadataItemsEntity savedMetadataItemsEntity = metadataItemsPersistence.load(id);
            assertNotNull(savedMetadataItemsEntity);

            assertEquals(this.metadataItemsEntity.getId(), savedMetadataItemsEntity.getId());
            assertEquals(this.metadataItemsEntity.getMetadataType(), savedMetadataItemsEntity.getMetadataType());
            assertEquals(this.metadataItemsEntity.getGuid(), savedMetadataItemsEntity.getGuid());
            assertEquals(this.metadataItemsEntity.getMediaItemCount(), savedMetadataItemsEntity.getMediaItemCount());
            assertEquals(this.metadataItemsEntity.getTitle(), savedMetadataItemsEntity.getTitle());
            assertEquals(this.metadataItemsEntity.getTitleSort(), savedMetadataItemsEntity.getTitleSort());
            assertEquals(this.metadataItemsEntity.getOriginalTitle(), savedMetadataItemsEntity.getOriginalTitle());
            assertEquals(this.metadataItemsEntity.getStudio(), savedMetadataItemsEntity.getStudio());
            assertEquals(this.metadataItemsEntity.getRating(), savedMetadataItemsEntity.getRating());
            assertEquals(this.metadataItemsEntity.getRatingCount(), savedMetadataItemsEntity.getRatingCount());
            assertEquals(this.metadataItemsEntity.getTagline(), savedMetadataItemsEntity.getTagline());
            assertEquals(this.metadataItemsEntity.getSummary(), savedMetadataItemsEntity.getSummary());
            assertEquals(this.metadataItemsEntity.getTrivia(), savedMetadataItemsEntity.getTrivia());
            assertEquals(this.metadataItemsEntity.getQuotes(), savedMetadataItemsEntity.getQuotes());
            assertEquals(this.metadataItemsEntity.getContentRating(), savedMetadataItemsEntity.getContentRating());
            assertEquals(this.metadataItemsEntity.getContentRatingAge(), savedMetadataItemsEntity.getContentRatingAge());
            assertEquals(this.metadataItemsEntity.getAbsoluteIndex(), savedMetadataItemsEntity.getAbsoluteIndex());
            assertEquals(this.metadataItemsEntity.getDuration(), savedMetadataItemsEntity.getDuration());
            assertEquals(this.metadataItemsEntity.getUserThumbUrl(), savedMetadataItemsEntity.getUserThumbUrl());
            assertEquals(this.metadataItemsEntity.getUserArtUrl(), savedMetadataItemsEntity.getUserArtUrl());
            assertEquals(this.metadataItemsEntity.getUserBannerUrl(), savedMetadataItemsEntity.getUserBannerUrl());
            assertEquals(this.metadataItemsEntity.getUserMusicUrl(), savedMetadataItemsEntity.getUserMusicUrl());
            assertEquals(this.metadataItemsEntity.getUserFields(), savedMetadataItemsEntity.getUserFields());
            assertEquals(this.metadataItemsEntity.getTagsGenre(), savedMetadataItemsEntity.getTagsGenre());
            assertEquals(this.metadataItemsEntity.getTagsCollection(), savedMetadataItemsEntity.getTagsCollection());
            assertEquals(this.metadataItemsEntity.getTagsDirector(), savedMetadataItemsEntity.getTagsDirector());
            assertEquals(this.metadataItemsEntity.getTagsWriter(), savedMetadataItemsEntity.getTagsWriter());
            assertEquals(this.metadataItemsEntity.getTagsStar(), savedMetadataItemsEntity.getTagsStar());
            assertEquals(this.metadataItemsEntity.getOriginallyAvailableAt(), savedMetadataItemsEntity.getOriginallyAvailableAt());
            assertEquals(this.metadataItemsEntity.getAvailableAt(), savedMetadataItemsEntity.getAvailableAt());
            assertEquals(this.metadataItemsEntity.getExpiresAt(), savedMetadataItemsEntity.getExpiresAt());
            assertEquals(this.metadataItemsEntity.getRefreshedAt(), savedMetadataItemsEntity.getRefreshedAt());
            assertEquals(this.metadataItemsEntity.getYear(), savedMetadataItemsEntity.getYear());
            assertEquals(this.metadataItemsEntity.getAddedAt(), savedMetadataItemsEntity.getAddedAt());
            assertEquals(this.metadataItemsEntity.getCreatedAt(), savedMetadataItemsEntity.getCreatedAt());
            assertEquals(this.metadataItemsEntity.getUpdatedAt(), savedMetadataItemsEntity.getUpdatedAt());
            assertEquals(this.metadataItemsEntity.getDeletedAt(), savedMetadataItemsEntity.getDeletedAt());
            assertEquals(this.metadataItemsEntity.getTagsCountry(), savedMetadataItemsEntity.getTagsCountry());
            assertEquals(this.metadataItemsEntity.getExtraData(), savedMetadataItemsEntity.getExtraData());
            assertEquals(this.metadataItemsEntity.getHash(), savedMetadataItemsEntity.getHash());
            assertEquals(this.metadataItemsEntity.getAudienceRating(), savedMetadataItemsEntity.getAudienceRating());
            assertEquals(this.metadataItemsEntity.getChangedAt(), savedMetadataItemsEntity.getChangedAt());
            assertEquals(this.metadataItemsEntity.getResourcesChangedAt(), savedMetadataItemsEntity.getResourcesChangedAt());
        }
    }

    /**
     * Test search failed.
     */
    @org.junit.Test
    public void testAbstractMetadataItemsEntitySearchFailed() {
        final MetadataItemsPersistence metadataItemsPersistence = PersistenceServiceProvider.getService(MetadataItemsPersistence.class);

        // Setup invalid object metadataItemsEntity

        metadataItemsPersistence.save(this.metadataItemsEntity);
        final Integer id = Integer.valueOf(-1);
        final MetadataItemsEntity savedMetadataItemsEntity = metadataItemsPersistence.load(id);

        assertNull(savedMetadataItemsEntity);
    }

    /**
     * Test delete.
     */
    @org.junit.Test
    public void testAbstractMetadataItemsEntityDelete() {
        final MetadataItemsPersistence metadataItemsPersistence = PersistenceServiceProvider.getService(MetadataItemsPersistence.class);

        // Setup valid object metadataItemsEntity

        final Integer id = this.metadataItemsEntity.getId();
        if (id != null) {
            metadataItemsPersistence.save(this.metadataItemsEntity);
            final MetadataItemsEntity savedMetadataItemsEntity = metadataItemsPersistence.load(id);
            assertNotNull(savedMetadataItemsEntity);

            metadataItemsPersistence.delete(this.metadataItemsEntity);
            final MetadataItemsEntity deletedMetadataItemsEntity = metadataItemsPersistence.load(id);
            assertNull(deletedMetadataItemsEntity);
        }
    }
}
