package aka.plexdb.bean.abstracts;

import java.util.Date;

import java.time.Instant;

import org.eclipse.jdt.annotation.NonNull;

import aka.plexdb.bean.MetadataItemSettingsEntity;

/**
 * JUnitTest for AbstractMetadataItemSettingsEntity.
 */
public class AbstractMetadataItemSettingsEntity_JUnitTest extends AbstractBean_JUnitTest {

    /**
     * MetadataItemSettingsEntity.
     */
    @NonNull
    public final MetadataItemSettingsEntity metadataItemSettingsEntity = new MetadataItemSettingsEntity();

    /**
     * Test save.
     */
    @org.junit.Before
    public void before() {
        // Primary key
        final Integer id = Integer.valueOf(1);
        // Fields
        final String guid = "guid";
        final Float rating = 1000.5F;
        final Integer viewOffset = Integer.valueOf(1);
        final Integer viewCount = Integer.valueOf(1);
        final Date lastViewedAt = Date.from(Instant.now());
        final Date createdAt = Date.from(Instant.now());
        final Date updatedAt = Date.from(Instant.now());
        final Integer skipCount = new Integer("0");
        final Date lastSkippedAt = null;
        final Integer changedAt = new Integer("0");

        this.metadataItemSettingsEntity.setId(id);
        this.metadataItemSettingsEntity.setGuid(guid);
        this.metadataItemSettingsEntity.setRating(rating);
        this.metadataItemSettingsEntity.setViewOffset(viewOffset);
        this.metadataItemSettingsEntity.setViewCount(viewCount);
        this.metadataItemSettingsEntity.setLastViewedAt(lastViewedAt);
        this.metadataItemSettingsEntity.setCreatedAt(createdAt);
        this.metadataItemSettingsEntity.setUpdatedAt(updatedAt);
        this.metadataItemSettingsEntity.setSkipCount(skipCount);
        this.metadataItemSettingsEntity.setLastSkippedAt(lastSkippedAt);
        this.metadataItemSettingsEntity.setChangedAt(changedAt);
    }
}
