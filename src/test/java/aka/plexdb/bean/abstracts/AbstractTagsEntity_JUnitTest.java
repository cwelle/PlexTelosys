package aka.plexdb.bean.abstracts;

import java.util.Date;

import java.time.Instant;

import org.eclipse.jdt.annotation.NonNull;

import aka.plexdb.bean.TagsEntity;

/**
 * JUnitTest for AbstractTagsEntity.
 */
public class AbstractTagsEntity_JUnitTest extends AbstractBean_JUnitTest {

    /**
     * TagsEntity.
     */
    @NonNull
    public final TagsEntity tagsEntity = new TagsEntity();

    /**
     * Test save.
     */
    @org.junit.Before
    public void before() {
        // Primary key
        final Integer id = Integer.valueOf(1);
        // Fields
        final String tag = "tag";
        final Integer tagType = Integer.valueOf(1);
        final String userThumbUrl = "userThumbUrl";
        final String userArtUrl = "userArtUrl";
        final String userMusicUrl = "userMusicUrl";
        final Date createdAt = Date.from(Instant.now());
        final Date updatedAt = Date.from(Instant.now());
        final Integer tagValue = Integer.valueOf(1);
        final String extraData = "extraData";

        this.tagsEntity.setId(id);
        this.tagsEntity.setTag(tag);
        this.tagsEntity.setTagType(tagType);
        this.tagsEntity.setUserThumbUrl(userThumbUrl);
        this.tagsEntity.setUserArtUrl(userArtUrl);
        this.tagsEntity.setUserMusicUrl(userMusicUrl);
        this.tagsEntity.setCreatedAt(createdAt);
        this.tagsEntity.setUpdatedAt(updatedAt);
        this.tagsEntity.setTagValue(tagValue);
        this.tagsEntity.setExtraData(extraData);
    }
}
