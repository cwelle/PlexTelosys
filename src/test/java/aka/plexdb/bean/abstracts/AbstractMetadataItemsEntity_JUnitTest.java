package aka.plexdb.bean.abstracts;

import java.util.Date;

import java.time.Instant;

import org.eclipse.jdt.annotation.NonNull;

import aka.plexdb.bean.MetadataItemsEntity;

/**
 * JUnitTest for AbstractMetadataItemsEntity.
 */
public class AbstractMetadataItemsEntity_JUnitTest extends AbstractBean_JUnitTest {

    /**
     * MetadataItemsEntity.
     */
    @NonNull
    public final MetadataItemsEntity metadataItemsEntity = new MetadataItemsEntity();

    /**
     * Test save.
     */
    @org.junit.Before
    public void before() {
        // Primary key
        final Integer id = Integer.valueOf(1);
        // Fields
        final Integer metadataType = Integer.valueOf(1);
        final String guid = "guid";
        final Integer mediaItemCount = Integer.valueOf(1);
        final String title = "title";
        final String titleSort = "titleSort";
        final String originalTitle = "originalTitle";
        final String studio = "studio";
        final Float rating = 1000.5F;
        final Integer ratingCount = Integer.valueOf(1);
        final String tagline = "tagline";
        final String summary = "summary";
        final String trivia = "trivia";
        final String quotes = "quotes";
        final String contentRating = "contentRating";
        final Integer contentRatingAge = Integer.valueOf(1);
        final Integer absoluteIndex = Integer.valueOf(1);
        final Integer duration = Integer.valueOf(1);
        final String userThumbUrl = "userThumbUrl";
        final String userArtUrl = "userArtUrl";
        final String userBannerUrl = "userBannerUrl";
        final String userMusicUrl = "userMusicUrl";
        final String userFields = "userFields";
        final String tagsGenre = "tagsGenre";
        final String tagsCollection = "tagsCollection";
        final String tagsDirector = "tagsDirector";
        final String tagsWriter = "tagsWriter";
        final String tagsStar = "tagsStar";
        final Date originallyAvailableAt = Date.from(Instant.now());
        final Date availableAt = Date.from(Instant.now());
        final Date expiresAt = Date.from(Instant.now());
        final Date refreshedAt = Date.from(Instant.now());
        final Integer year = Integer.valueOf(1);
        final Date addedAt = Date.from(Instant.now());
        final Date createdAt = Date.from(Instant.now());
        final Date updatedAt = Date.from(Instant.now());
        final Date deletedAt = Date.from(Instant.now());
        final String tagsCountry = "tagsCountry";
        final String extraData = "extraData";
        final String hash = "hash";
        final Float audienceRating = 1000.5F;
        final Integer changedAt = new Integer("0");
        final Integer resourcesChangedAt = new Integer("0");

        this.metadataItemsEntity.setId(id);
        this.metadataItemsEntity.setMetadataType(metadataType);
        this.metadataItemsEntity.setGuid(guid);
        this.metadataItemsEntity.setMediaItemCount(mediaItemCount);
        this.metadataItemsEntity.setTitle(title);
        this.metadataItemsEntity.setTitleSort(titleSort);
        this.metadataItemsEntity.setOriginalTitle(originalTitle);
        this.metadataItemsEntity.setStudio(studio);
        this.metadataItemsEntity.setRating(rating);
        this.metadataItemsEntity.setRatingCount(ratingCount);
        this.metadataItemsEntity.setTagline(tagline);
        this.metadataItemsEntity.setSummary(summary);
        this.metadataItemsEntity.setTrivia(trivia);
        this.metadataItemsEntity.setQuotes(quotes);
        this.metadataItemsEntity.setContentRating(contentRating);
        this.metadataItemsEntity.setContentRatingAge(contentRatingAge);
        this.metadataItemsEntity.setAbsoluteIndex(absoluteIndex);
        this.metadataItemsEntity.setDuration(duration);
        this.metadataItemsEntity.setUserThumbUrl(userThumbUrl);
        this.metadataItemsEntity.setUserArtUrl(userArtUrl);
        this.metadataItemsEntity.setUserBannerUrl(userBannerUrl);
        this.metadataItemsEntity.setUserMusicUrl(userMusicUrl);
        this.metadataItemsEntity.setUserFields(userFields);
        this.metadataItemsEntity.setTagsGenre(tagsGenre);
        this.metadataItemsEntity.setTagsCollection(tagsCollection);
        this.metadataItemsEntity.setTagsDirector(tagsDirector);
        this.metadataItemsEntity.setTagsWriter(tagsWriter);
        this.metadataItemsEntity.setTagsStar(tagsStar);
        this.metadataItemsEntity.setOriginallyAvailableAt(originallyAvailableAt);
        this.metadataItemsEntity.setAvailableAt(availableAt);
        this.metadataItemsEntity.setExpiresAt(expiresAt);
        this.metadataItemsEntity.setRefreshedAt(refreshedAt);
        this.metadataItemsEntity.setYear(year);
        this.metadataItemsEntity.setAddedAt(addedAt);
        this.metadataItemsEntity.setCreatedAt(createdAt);
        this.metadataItemsEntity.setUpdatedAt(updatedAt);
        this.metadataItemsEntity.setDeletedAt(deletedAt);
        this.metadataItemsEntity.setTagsCountry(tagsCountry);
        this.metadataItemsEntity.setExtraData(extraData);
        this.metadataItemsEntity.setHash(hash);
        this.metadataItemsEntity.setAudienceRating(audienceRating);
        this.metadataItemsEntity.setChangedAt(changedAt);
        this.metadataItemsEntity.setResourcesChangedAt(resourcesChangedAt);
    }
}
