package aka.plexdb.bean.abstracts;

import java.util.Date;

import java.time.Instant;

import org.eclipse.jdt.annotation.NonNull;

import aka.plexdb.bean.DirectoriesEntity;

/**
 * JUnitTest for AbstractDirectoriesEntity.
 */
public class AbstractDirectoriesEntity_JUnitTest extends AbstractBean_JUnitTest {

    /**
     * DirectoriesEntity.
     */
    @NonNull
    public final DirectoriesEntity directoriesEntity = new DirectoriesEntity();

    /**
     * Test save.
     */
    @org.junit.Before
    public void before() {
        // Primary key
        final Integer id = Integer.valueOf(1);
        // Fields
        final String path = "path";
        final Date createdAt = Date.from(Instant.now());
        final Date updatedAt = Date.from(Instant.now());
        final Date deletedAt = Date.from(Instant.now());

        this.directoriesEntity.setId(id);
        this.directoriesEntity.setPath(path);
        this.directoriesEntity.setCreatedAt(createdAt);
        this.directoriesEntity.setUpdatedAt(updatedAt);
        this.directoriesEntity.setDeletedAt(deletedAt);
    }
}
