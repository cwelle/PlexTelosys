package aka.plexdb.bean.abstracts;

import java.util.Date;

import java.time.Instant;

import org.eclipse.jdt.annotation.NonNull;

import aka.plexdb.bean.LibrarySectionsEntity;

/**
 * JUnitTest for AbstractLibrarySectionsEntity.
 */
public class AbstractLibrarySectionsEntity_JUnitTest extends AbstractBean_JUnitTest {

    /**
     * LibrarySectionsEntity.
     */
    @NonNull
    public final LibrarySectionsEntity librarySectionsEntity = new LibrarySectionsEntity();

    /**
     * Test save.
     */
    @org.junit.Before
    public void before() {
        // Primary key
        final Integer id = Integer.valueOf(1);
        // Fields
        final String name = "name";
        final String nameSort = "nameSort";
        final Integer sectionType = Integer.valueOf(1);
        final String language = "language";
        final String agent = "agent";
        final String scanner = "scanner";
        final String userThumbUrl = "userThumbUrl";
        final String userArtUrl = "userArtUrl";
        final String userThemeMusicUrl = "userThemeMusicUrl";
        final Boolean isPublic = Boolean.TRUE;
        final Date createdAt = Date.from(Instant.now());
        final Date updatedAt = Date.from(Instant.now());
        final Date scannedAt = Date.from(Instant.now());
        final Boolean displaySecondaryLevel = Boolean.TRUE;
        final String userFields = "userFields";
        final String queryXml = "queryXml";
        final Integer queryType = Integer.valueOf(1);
        final String uuid = "uuid";
        final Integer changedAt = new Integer("0");

        this.librarySectionsEntity.setId(id);
        this.librarySectionsEntity.setName(name);
        this.librarySectionsEntity.setNameSort(nameSort);
        this.librarySectionsEntity.setSectionType(sectionType);
        this.librarySectionsEntity.setLanguage(language);
        this.librarySectionsEntity.setAgent(agent);
        this.librarySectionsEntity.setScanner(scanner);
        this.librarySectionsEntity.setUserThumbUrl(userThumbUrl);
        this.librarySectionsEntity.setUserArtUrl(userArtUrl);
        this.librarySectionsEntity.setUserThemeMusicUrl(userThemeMusicUrl);
        this.librarySectionsEntity.setIsPublic(isPublic);
        this.librarySectionsEntity.setCreatedAt(createdAt);
        this.librarySectionsEntity.setUpdatedAt(updatedAt);
        this.librarySectionsEntity.setScannedAt(scannedAt);
        this.librarySectionsEntity.setDisplaySecondaryLevel(displaySecondaryLevel);
        this.librarySectionsEntity.setUserFields(userFields);
        this.librarySectionsEntity.setQueryXml(queryXml);
        this.librarySectionsEntity.setQueryType(queryType);
        this.librarySectionsEntity.setUuid(uuid);
        this.librarySectionsEntity.setChangedAt(changedAt);
    }
}
