package aka.plexdb.bean.abstracts;

import java.util.Date;

import java.time.Instant;

import org.eclipse.jdt.annotation.NonNull;

import aka.plexdb.bean.MediaItemsEntity;

/**
 * JUnitTest for AbstractMediaItemsEntity.
 */
public class AbstractMediaItemsEntity_JUnitTest extends AbstractBean_JUnitTest {

    /**
     * MediaItemsEntity.
     */
    @NonNull
    public final MediaItemsEntity mediaItemsEntity = new MediaItemsEntity();

    /**
     * Test save.
     */
    @org.junit.Before
    public void before() {
        // Primary key
        final Integer id = Integer.valueOf(1);
        // Fields
        final Integer typeId = Integer.valueOf(1);
        final Integer width = Integer.valueOf(1);
        final Integer height = Integer.valueOf(1);
        final Integer size = Integer.valueOf(1);
        final Integer duration = Integer.valueOf(1);
        final Integer bitrate = Integer.valueOf(1);
        final String container = "container";
        final String videoCodec = "videoCodec";
        final String audioCodec = "audioCodec";
        final Float displayAspectRatio = 1000.5F;
        final Float framesPerSecond = 1000.5F;
        final Integer audioChannels = Integer.valueOf(1);
        final Boolean interlaced = Boolean.TRUE;
        final String source = "source";
        final String hints = "hints";
        final Integer displayOffset = Integer.valueOf(1);
        final String settings = "settings";
        final Date createdAt = Date.from(Instant.now());
        final Date updatedAt = Date.from(Instant.now());
        final Boolean optimizedForStreaming = Boolean.TRUE;
        final Date deletedAt = Date.from(Instant.now());
        final Integer mediaAnalysisVersion = new Integer("0");
        final Float sampleAspectRatio = 1000.5F;
        final String extraData = "extraData";
        final Integer proxyType = Integer.valueOf(1);

        this.mediaItemsEntity.setId(id);
        this.mediaItemsEntity.setTypeId(typeId);
        this.mediaItemsEntity.setWidth(width);
        this.mediaItemsEntity.setHeight(height);
        this.mediaItemsEntity.setSize(size);
        this.mediaItemsEntity.setDuration(duration);
        this.mediaItemsEntity.setBitrate(bitrate);
        this.mediaItemsEntity.setContainer(container);
        this.mediaItemsEntity.setVideoCodec(videoCodec);
        this.mediaItemsEntity.setAudioCodec(audioCodec);
        this.mediaItemsEntity.setDisplayAspectRatio(displayAspectRatio);
        this.mediaItemsEntity.setFramesPerSecond(framesPerSecond);
        this.mediaItemsEntity.setAudioChannels(audioChannels);
        this.mediaItemsEntity.setInterlaced(interlaced);
        this.mediaItemsEntity.setSource(source);
        this.mediaItemsEntity.setHints(hints);
        this.mediaItemsEntity.setDisplayOffset(displayOffset);
        this.mediaItemsEntity.setSettings(settings);
        this.mediaItemsEntity.setCreatedAt(createdAt);
        this.mediaItemsEntity.setUpdatedAt(updatedAt);
        this.mediaItemsEntity.setOptimizedForStreaming(optimizedForStreaming);
        this.mediaItemsEntity.setDeletedAt(deletedAt);
        this.mediaItemsEntity.setMediaAnalysisVersion(mediaAnalysisVersion);
        this.mediaItemsEntity.setSampleAspectRatio(sampleAspectRatio);
        this.mediaItemsEntity.setExtraData(extraData);
        this.mediaItemsEntity.setProxyType(proxyType);
    }
}
