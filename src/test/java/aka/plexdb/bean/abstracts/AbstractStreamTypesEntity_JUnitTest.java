package aka.plexdb.bean.abstracts;


import org.eclipse.jdt.annotation.NonNull;

import aka.plexdb.bean.StreamTypesEntity;

/**
 * JUnitTest for AbstractStreamTypesEntity.
 */
public class AbstractStreamTypesEntity_JUnitTest extends AbstractBean_JUnitTest {

    /**
     * StreamTypesEntity.
     */
    @NonNull
    public final StreamTypesEntity streamTypesEntity = new StreamTypesEntity();

    /**
     * Test save.
     */
    @org.junit.Before
    public void before() {
        // Primary key
        final Integer id = Integer.valueOf(1);
        // Fields
        final String name = "name";

        this.streamTypesEntity.setId(id);
        this.streamTypesEntity.setName(name);
    }
}
