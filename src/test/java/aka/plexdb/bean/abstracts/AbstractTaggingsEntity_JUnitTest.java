package aka.plexdb.bean.abstracts;

import java.util.Date;

import java.time.Instant;

import org.eclipse.jdt.annotation.NonNull;

import aka.plexdb.bean.TaggingsEntity;

/**
 * JUnitTest for AbstractTaggingsEntity.
 */
public class AbstractTaggingsEntity_JUnitTest extends AbstractBean_JUnitTest {

    /**
     * TaggingsEntity.
     */
    @NonNull
    public final TaggingsEntity taggingsEntity = new TaggingsEntity();

    /**
     * Test save.
     */
    @org.junit.Before
    public void before() {
        // Primary key
        final Integer id = Integer.valueOf(1);
        // Fields
        final String text = "text";
        final Integer timeOffset = Integer.valueOf(1);
        final Integer endTimeOffset = Integer.valueOf(1);
        final String thumbUrl = "thumbUrl";
        final Date createdAt = Date.from(Instant.now());
        final String extraData = "extraData";

        this.taggingsEntity.setId(id);
        this.taggingsEntity.setText(text);
        this.taggingsEntity.setTimeOffset(timeOffset);
        this.taggingsEntity.setEndTimeOffset(endTimeOffset);
        this.taggingsEntity.setThumbUrl(thumbUrl);
        this.taggingsEntity.setCreatedAt(createdAt);
        this.taggingsEntity.setExtraData(extraData);
    }
}
