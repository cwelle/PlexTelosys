package aka.plexdb.bean;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import aka.plexdb.bean.abstracts.AbstractMediaStreamsEntity_JUnitTest;
import aka.plexdb.persistence.PersistenceServiceProvider;
import aka.plexdb.persistence.services.MediaStreamsPersistence;

/**
 * JUnitTest for MediaStreamsEntity.
 */
public class MediaStreamsEntity_JUnitTest extends AbstractMediaStreamsEntity_JUnitTest {

    /**
     * Test save successful.
     */
    @org.junit.Test
    public void testAbstractMediaStreamsEntitySaveSuccessful() {
        final MediaStreamsPersistence mediaStreamsPersistence = PersistenceServiceProvider.getService(MediaStreamsPersistence.class);

        // Setup valid object mediaStreamsEntity

        mediaStreamsPersistence.save(this.mediaStreamsEntity);
        Integer id = this.mediaStreamsEntity.getId();
        if (id != null) {
            final MediaStreamsEntity savedMediaStreamsEntity = mediaStreamsPersistence.load(id);
            assertNotNull(savedMediaStreamsEntity);
        }
    }

    /**
     * Test save failed.
     */
    @org.junit.Test(expected = javax.persistence.PersistenceException.class)
    public void testAbstractMediaStreamsEntitySaveFailed() {
        final MediaStreamsPersistence mediaStreamsPersistence = PersistenceServiceProvider.getService(MediaStreamsPersistence.class);

        // Setup invalid object mediaStreamsEntity

        this.mediaStreamsEntity.setId(null);
        mediaStreamsPersistence.save(this.mediaStreamsEntity);

        fail("expected an exception");
    }

    /**
     * Test search successful.
     */
    @org.junit.Test
    public void testAbstractMediaStreamsEntitySearchSuccessful() {
        final MediaStreamsPersistence mediaStreamsPersistence = PersistenceServiceProvider.getService(MediaStreamsPersistence.class);

        // Setup valid object mediaStreamsEntity

        mediaStreamsPersistence.save(this.mediaStreamsEntity);
        Integer id = this.mediaStreamsEntity.getId();
        if (id != null) {
            final MediaStreamsEntity savedMediaStreamsEntity = mediaStreamsPersistence.load(id);
            assertNotNull(savedMediaStreamsEntity);

            assertEquals(this.mediaStreamsEntity.getId(), savedMediaStreamsEntity.getId());
            assertEquals(this.mediaStreamsEntity.getUrl(), savedMediaStreamsEntity.getUrl());
            assertEquals(this.mediaStreamsEntity.getCodec(), savedMediaStreamsEntity.getCodec());
            assertEquals(this.mediaStreamsEntity.getLanguage(), savedMediaStreamsEntity.getLanguage());
            assertEquals(this.mediaStreamsEntity.getCreatedAt(), savedMediaStreamsEntity.getCreatedAt());
            assertEquals(this.mediaStreamsEntity.getUpdatedAt(), savedMediaStreamsEntity.getUpdatedAt());
            assertEquals(this.mediaStreamsEntity.getChannels(), savedMediaStreamsEntity.getChannels());
            assertEquals(this.mediaStreamsEntity.getBitrate(), savedMediaStreamsEntity.getBitrate());
            assertEquals(this.mediaStreamsEntity.getUrlIndex(), savedMediaStreamsEntity.getUrlIndex());
            assertEquals(this.mediaStreamsEntity.getIsDefault(), savedMediaStreamsEntity.getIsDefault());
            assertEquals(this.mediaStreamsEntity.getIsForced(), savedMediaStreamsEntity.getIsForced());
            assertEquals(this.mediaStreamsEntity.getExtraData(), savedMediaStreamsEntity.getExtraData());
        }
    }

    /**
     * Test search failed.
     */
    @org.junit.Test
    public void testAbstractMediaStreamsEntitySearchFailed() {
        final MediaStreamsPersistence mediaStreamsPersistence = PersistenceServiceProvider.getService(MediaStreamsPersistence.class);

        // Setup invalid object mediaStreamsEntity

        mediaStreamsPersistence.save(this.mediaStreamsEntity);
        final Integer id = Integer.valueOf(-1);
        final MediaStreamsEntity savedMediaStreamsEntity = mediaStreamsPersistence.load(id);

        assertNull(savedMediaStreamsEntity);
    }

    /**
     * Test delete.
     */
    @org.junit.Test
    public void testAbstractMediaStreamsEntityDelete() {
        final MediaStreamsPersistence mediaStreamsPersistence = PersistenceServiceProvider.getService(MediaStreamsPersistence.class);

        // Setup valid object mediaStreamsEntity

        final Integer id = this.mediaStreamsEntity.getId();
        if (id != null) {
            mediaStreamsPersistence.save(this.mediaStreamsEntity);
            final MediaStreamsEntity savedMediaStreamsEntity = mediaStreamsPersistence.load(id);
            assertNotNull(savedMediaStreamsEntity);

            mediaStreamsPersistence.delete(this.mediaStreamsEntity);
            final MediaStreamsEntity deletedMediaStreamsEntity = mediaStreamsPersistence.load(id);
            assertNull(deletedMediaStreamsEntity);
        }
    }
}
