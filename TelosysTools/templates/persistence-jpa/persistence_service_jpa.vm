## ----------------------------------------------------------------------
#if ( $entity.hasCompositePrimaryKey() )
#set( $primaryKeyType = "${entity.name}EntityKey" )
#else 
#set( $primaryKeyType = $entity.keyAttributes[0].wrapperType )
#end
## ----------------------------------------------------------------------
package ${target.javaPackageFromFolder($SRC)};

#foreach( $import in $java.imports($entity.keyAttributes) )
import $import;
#end
import javax.persistence.Query;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;

import ${ENTITY_PKG}.${entity.name}Entity;
#if ( $entity.hasCompositePrimaryKey() )
import ${ENTITY_PKG}.${entity.name}EntityKey;
#end
import ${ROOT_PKG}.persistence.commons.GenericJPAService;
import ${ROOT_PKG}.persistence.commons.JPAOperation;
import ${ROOT_PKG}.persistence.finders.$fn.toLowerCase(${entity.name}).${entity.name}CountAll;
import ${ROOT_PKG}.persistence.services.${entity.name}Persistence;

/**
 * JPA implementation for basic persistence operations ( entity "${entity.name}" ).
 */
public final class ${entity.name}PersistenceImpl extends GenericJPAService<@NonNull ${entity.name}Entity, @NonNull $primaryKeyType> implements ${entity.name}Persistence {

	/**
	 * Constructor.
	 */
	public ${entity.name}PersistenceImpl() {
		super(${entity.name}Entity.class);
	}

	@Override
    @Nullable 
	public ${entity.name}Entity load(final @NonNull $fn.argumentsListWithType( $entity.keyAttributes )) {
#if ( $entity.hasCompositePrimaryKey() )
		// Build the composite key
		$primaryKeyType key = new $primaryKeyType($fn.argumentsList( $entity.keyAttributes ));
		return super.load(key);
#else
		return super.load($fn.argumentsList( $entity.keyAttributes ));
#end	
	}

	@Override
	public boolean delete(final @NonNull $fn.argumentsListWithType( $entity.keyAttributes )) {
#if ( $entity.hasCompositePrimaryKey() )
		// Build the composite key
		$primaryKeyType key = new $primaryKeyType($fn.argumentsList( $entity.keyAttributes ));
		return super.delete(key);
#else
		return super.delete($fn.argumentsList( $entity.keyAttributes ));
#end	
	}

	@Override
	public boolean delete(final @NonNull ${entity.name}Entity entity) {
#if ( $entity.hasCompositePrimaryKey() )
        // Build the composite key
        $primaryKeyType key = new $primaryKeyType($fn.argumentsListWithGetter( "entity", $entity.keyAttributes ));
        boolean result = super.delete(key);
#else
        boolean result = false;
        $primaryKeyType key = $fn.argumentsListWithGetter( "entity", $entity.keyAttributes );
        if (key != null) {
            result = super.delete(key);
        }
#end	
		return result;
	}

    @Override
    public long countAll() {
        final JPAOperation operation = em -> {
            final ${entity.name}CountAll finder = new ${entity.name}CountAll();
            final Query query = finder.getBuildedQuery(em);
            return query.getSingleResult();
        };

        final Long value = (Long) execute(operation);
        long result = 0;
        if (value != null) {
            result = value.longValue();
        }

        return result;
    }
}
